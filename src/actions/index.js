export const addresultwelldata = (payload) => {
  return {
    type: 'RESULTWELLDATA',
    payload
  }
}
export const addwellselect = (payload) => {
  return {
    type: "SELECTWELLDATA",
    payload
  }
}

export const reflasheviewchart = (payload) => {
  return {
    type: "REFLASEVIEWCHART",
    payload
  }
} 

export const settempwellid = (payload) => {
  return {
    type: "SETTEMPWELLID",
    payload
  }
}

export const setpointchart = (payload) => {
  return {
    type : "SETPOINTCHART",
    payload
  }
}

export const setpointselect = (payload) => {
  return {
    type : "SETPOINTCHARTSELECT",
    payload
  }
}

export const setcurrentdevice = (payload) => {
  return {
    type: 'SETCURRENTDEVICE',
    payload 
  }
}

export const setprediction = (payload) => {
  return {
    type: "SETPREDICTION",
    payload
  }
}

export const setpredictioncurrent = (payload) => {
  return {
    type: "SETPREDICTIONCURRENT",
    payload
  }
}

export const accesstoken = (payload) => {
  return {
    type: "ACCESSTOKEN",
    payload
  }
}

export const email = (payload) => {
  return {
    type: "EMAIL",
    payload
  }
}

export const type = (payload) => {
  return {
    type: "TYPE",
    payload
  }
}