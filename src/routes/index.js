import React from 'react'
import { Route, HashRouter, Switch } from 'react-router-dom'

import BeamPumpMainPage from './../components/BeamPumpMainPage'
import Current from './../components/Current'
import ScrollToTop from './../components/ScrollTop'
import Visualisation from './../components/Visualisation'
import Admin from './../components/Admin'
import ManageDevices from './../components/ManageDevices'
import Login from './../components/login'
import PumpRunStatus from './../components/PumpRunStatus'
import PumpCalculate from './../components/PumpCalculate'
import WellParameter from './../components/WellParameter'
import Test from './../components/fortest'

export default props => (
    <HashRouter
      // hashType='slash'
      // basename="/app"
    >
      <ScrollToTop>

        <Switch>
          <Route exact path='/' component={ Visualisation } />
          <Route exact path="/Home" component={ Visualisation } />
          <Route exact path='/Realtime' component={ Current } />
          <Route exact path='/History' component={ BeamPumpMainPage } />
          <Route exact path="/Admin" component={Admin} />
          <Route exact path="/ManageDevices" component={ManageDevices} />
          <Route exact path="/Login" component={Login} />
          <Route exact path="/PumpRunStatus" component={PumpRunStatus} />
          <Route exact path="/PumpCalculate" component={PumpCalculate} />
          <Route exact path="/WellParameter" component={WellParameter} />
          <Route exact path="/test" component={Test} />
        </Switch>

      </ScrollToTop>
    </HashRouter>
  )

/* <ScrollToTop>
  <Switch>
  <Route exact path='/' component={ Current } />
  <Route exact path='/Realtime' component={ Current } />
  <Route exact path='/History' component={ BeamPumpMainPage } />
  </Switch>
</ScrollToTop> */