import React, { lazy, Suspense } from "react";
import ReactDOM from 'react-dom';
import './index.css';
import './components/cards/Chart/PointChart.css'
import { store } from './store'

// import App from './App';
const App = lazy(() => import("./App"))

function render () {
  ReactDOM.render(
    <div id="container">
      <Suspense fallback={<h1>Still Loading…</h1>}>
        <App store={store} />
      </Suspense>
    </div>
    ,
    document.getElementById('root')
  );
}

store.subscribe(render);

render();
