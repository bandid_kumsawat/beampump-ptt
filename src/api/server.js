import axios from 'axios'

export default (token) => {
  var api = axios.create({
    baseURL: "http://35.247.155.68/api"
    // baseURL: "http://127.0.0.1:4000/api"
    // baseURL: "http://192.168.1.143:4000/api"
    // baseURL: 'http://127.0.0.1:11111'
    // baseURL: 'http://192.168.43.194:4000/api'
  })
  api.defaults.headers.common['Authorization'] = 'Bearer ' + token
  return api
}