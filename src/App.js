import React, { Component } from 'react';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles'
import './App.css';
import Routes from './routes'
import { blue, indigo } from '@material-ui/core/colors'
import { makeStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import InputIcon from '@material-ui/icons/Input';

import { store } from './store'
import {email, type, accesstoken} from './actions'

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: blue[900]
    },
    primary: {
      main: indigo[700]
    }
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '"Lato"',
      'sans-serif'
    ].join(',')
  }
});

class App extends Component {
  constructor(props) {
    super(props);
    this.store = this.props.store;
  }
  componentDidMount(){
    if (localStorage.getItem('token') !== '' || store.getState().AccessToken){
      store.dispatch(email(localStorage.getItem('email')))
      store.dispatch(type(localStorage.getItem('utype')))
      store.dispatch(accesstoken(localStorage.getItem('token')))
    }
  }
  render() {
    return (
      <div>
        <ThemeProvider theme={theme}>
          <Routes />
        </ThemeProvider>
      </div>
    );
  }
}

// export default App
const useStyles = makeStyles((theme) => ({
  root: {
    zIndex: -10,
    height: '0',
  },
  speedDial: {
    position: 'fixed',
    bottom: "5%",
    left: '93%',
    zIndex: theme.zIndex.drawer + 2,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

// const actions = [{ 
//     icon: <InputIcon />,
//     name: <div style ={{
//       width: "300px"
//     }}> <span style={{color: '#aaaaaa'}}> Logout </span> <span style={{color: '#0000ff88'}}>{store.getState().email}({store.getState().type})</span></div>, fn: () => {
//       localStorage.setItem('email', '');
//       localStorage.setItem('utype', '');
//       localStorage.setItem('token', '');
//       store.dispatch(email(localStorage.getItem('email')))
//       store.dispatch(type(localStorage.getItem('utype')))
//       store.dispatch(accesstoken(localStorage.getItem('token')))
//       window.location.replace(store.getState().FullPath+'/Login');
//     },
// }]

// export default App;
export default function SpeedDialTooltipOpen() {
  // console.log(store.getState().AccessToken)
  
  // setInterval(() => {
  // if (localStorage.getItem('token') !== null && store.getState().AccessToken === ''){
  //   store.dispatch(email(localStorage.getItem('email')))
  //   store.dispatch(type(localStorage.getItem('type')))
  //   store.dispatch(accesstoken(localStorage.getItem('token')))
  // }
  if ((store.getState().AccessToken === '' && localStorage.getItem('token') === null) || localStorage.getItem('token') === ''){
    localStorage.setItem('email', '');
    localStorage.setItem('utype', '');
    localStorage.setItem('token', '');
    window.location.replace(store.getState().FullPath+'/Login');
  }
  // }, 100);

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const setcolor = () => {
    document.getElementById("container").style.backgroundColor = "#ff00ff !important"
  }

  const handleClose = () => {
    setOpen(false);
    if (open)
      setcolor()
    else {
      setcolor()
    }
  };
  return (
    <div className={classes.root}>
      <Backdrop hidden={(store.getState().AccessToken === '' && localStorage.getItem('token') === '') } className={classes.backdrop} open={open}></Backdrop>
      <SpeedDial
        hidden={(store.getState().AccessToken === '' && localStorage.getItem('token') === '') }
        ariaLabel="SpeedDial tooltip example"
        className={classes.speedDial}
        icon={<PersonOutlineIcon />}
        onClose={handleClose}
        onOpen={handleOpen}
        open={open}
      >

        <SpeedDialAction
            key={<div style ={{
              width: "300px"
            }}> <span style={{color: '#aaaaaa'}}> Logout </span> <span style={{color: '#0000ff88'}}>{store.getState().email}({store.getState().type})</span></div>}
            icon={<InputIcon />}
            tooltipTitle={<div style ={{
              width: "300px"
            }}> <span style={{color: '#aaaaaa'}}> Logout </span> <span style={{color: '#0000ff88'}}>{store.getState().email}({store.getState().type})</span></div>}
            tooltipOpen
            onClick={() => {
              localStorage.setItem('email', '');
              localStorage.setItem('utype', '');
              localStorage.setItem('token', '');
              store.dispatch(email(localStorage.getItem('email')))
              store.dispatch(type(localStorage.getItem('utype')))
              store.dispatch(accesstoken(localStorage.getItem('token')))
              window.location.replace(store.getState().FullPath+'/Login');
            }}
          />
        {/* {actions.map((action) => (
          <SpeedDialAction
            key={action.name}
            icon={action.icon}
            tooltipTitle={action.name}
            tooltipOpen
            onClick={action.fn}
          />
        ))} */}
      </SpeedDial>
      <App />
    </div>
  );
}
