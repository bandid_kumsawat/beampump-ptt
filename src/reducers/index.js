import initialState from './state'

export default (state = initialState, action) => {
  switch (action.type) {
    case 'RESULTWELLDATA':
      return Object.assign({}, state, {
        wellResult: action.payload
      })
    case "SELECTWELLDATA": 
      return Object.assign({}, state, {
        selectWell: action.payload
      })
    case "REFLASEVIEWCHART":
        return Object.assign({}, state, {
          refrash_view_chart: action.payload
        })
    case "SETTEMPWELLID":
        return Object.assign({}, state, {
          TempWellID: action.payload
        })
    case "SETPOINTCHART":
        return Object.assign({}, state, {
          PointChart: [action.payload]
        })
    case "SETPOINTCHARTSELECT":
      return Object.assign({}, state, {
        PointSelect: action.payload
      })
    case "SETCURRENTDEVICE":
      return Object.assign({}, state, {
        currentWell: action.payload
      })
    case "SETPREDICTION":
      return Object.assign({}, state, {
        currentWellPrediction: action.payload
      })
    case "SETPREDICTIONCURRENT":
      return Object.assign({}, state, {
        Prediction: action.payload
      })
    case "ACCESSTOKEN": 
      return Object.assign({}, state, {
        AccessToken: action.payload
      })
    case "TYPE": 
      return Object.assign({}, state, {
        type: action.payload
      })
    case "EMAIL": 
      return Object.assign({}, state, {
        email: action.payload
      })
    default:
      return state
  }
}
