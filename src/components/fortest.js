import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import DynaChartResponsive from './cards/Chart/DynaChartResponsive_Visualisation'
import DynaChart from './cards/Chart/DynaChart'
// store
import { store } from './../store'
import data from './lp.json'

const styles = theme => ({

});

class test extends Component {
  constructor (){
    super()
    this.state = {
      username: '',
      password: ''
    }
  }
  componentDidMount() {

  }
  render() {
    const { classes } = this.props;

    return (
      <div>
        <DynaChartResponsive 
          cid={"test"}
          store={store}
          key={'test'} 
          index={12}
          Loads={data.Loads} 
          Positions={data.Positions}
          color='#29abe2'
          fill='#E8F7FF'
          width='1000'
          height='700'
          subTitle={data.DateTime}
          hover={false}
          border={false}
          onclick={false}
          detailline={true}
          PumpCardLoads={data.PumpCardPositions}
          PumpCardPositions={data.PumpCardLoads}
          autoscale={false}
        />
        {/* <DynaChart
          cid={"test"}
          store={store}
          key={'test'} 
          index={12}
          Loads={data.Loads} 
          Positions={data.Positions}
          color='#29abe2'
          fill='#E8F7FF'
          width='1000'
          height='700'
          subTitle={data.DateTime}
          hover={false}
          border={false}
          onclick={false}
          detailline={true}
          PumpCardLoads={data.PumpCardLoads}
          PumpCardPositions={data.PumpCardPositions}
          autoscale={false}
        /> */}
      </div>
    );
  }
}

export default withStyles(styles)(test);


// --db=PTT_BEAM_PUMP
// --collection
// logstatuses
// raws
// sensors
// speeds
// users


// mongoexport --collection=logstatuses --db=PTT_BEAM_PUMP --jsonArray --out=logstatuses.json
// mongoexport --collection=raws --db=PTT_BEAM_PUMP --jsonArray --out=raws.json
// mongoexport --collection=sensors --db=PTT_BEAM_PUMP --jsonArray --out=sensors.json
// mongoexport --collection=speeds --db=PTT_BEAM_PUMP --jsonArray --out=speeds.json
// mongoexport --collection=users --db=PTT_BEAM_PUMP --jsonArray --out=users.json