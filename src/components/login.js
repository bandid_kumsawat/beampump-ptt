import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import backgroundImage from './../../src/images/img-01-new.jpg'
import logoPTT from './../../src/images/avatar-01.png'

// store
import { store } from './../store'

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import API from './../api/server'

import {email, type, accesstoken} from './../actions'

const backgroundShape = require("../images/shape.svg");

const BootstrapButton = withStyles({
  root: {
    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 16,
    padding: '6px 12px',
    border: '1px solid',
    lineHeight: 1.5,
    backgroundColor: '#0a88be',
    borderColor: '#0a88be',
    width: '100%',
    // fontFamily: [
    //   '-apple-system',
    //   'BlinkMacSystemFont',
    //   '"Segoe UI"',
    //   'Roboto',
    //   '"Helvetica Neue"',
    //   'Arial',
    //   'sans-serif',
    //   '"Apple Color Emoji"',
    //   '"Segoe UI Emoji"',
    //   '"Segoe UI Symbol"',
    // ].join(','),
    '&:hover': {
      backgroundColor: '#0a88be',
      borderColor: '#0a88be',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#0a88be',
      borderColor: '#0a88be',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
  },
})(Button);

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: '#000000',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#000000',
      color: '#000000'
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#000000',
        color: '#000000'
      },
      '&:hover fieldset': {
        borderColor: '#000000',
        color: '#000000'
      },
      '&.Mui-focused fieldset': {
        borderColor: '#000000',
        color: '#000000'
      },
    },
  },
})(TextField);

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.grey["A500"],
    overflow: "hidden",
    background: `url(${backgroundShape}) no-repeat`,
    backgroundSize: "cover",
    backgroundPosition: "0 400px",
    marginTop: 0,
    paddingRight: 20,
    paddingLeft: 20
  },
  grid: {
    width: 1000
  },
  hieghtlightchart: {
    borderTop: "3px solid #0a88be"
  },
  bgimage: {
    backgroundImage: `url(${backgroundImage})`,
    width: "100%",
    height: "100vh",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
    background: "webkit-linear-gradient(bottom, #b2cdf8, #00c6fb);"
  },
  loginFrom: {
    position: "fixed",
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    width: '30%',
    height: "50%",
    borderRadius: '4px',
    backgroundColor: '#ffffff9C',
    // boxShadow: "2px 4px #ffffffff"
  },
  logoLogin:{
    width: "100%",
    marginTop: '10px',
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  logoImage: {
    width: "30%",
    height: "30%",
    marginLeft: '50%',
    transform: 'translate(-50%, 0%)',
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
    alignItems: "center"
  },
  textLogin: {
    color: '#0b78a7ff',
    margin: 'auto',
    marginTop: '10px',
    fontSize: '20px',
    textAlign: 'center',
    fontWeight: '700',
  },
  textPassword: {
    width: "90%",
    margin: "auto",
    marginTop: '10px'
  },
  textUsername: {
    width: "90%",
    margin: "auto",
    marginTop: '10px'
  },
  textComfirm: {
    width: "90%",
    margin: "auto",
    marginTop: '10px',
  }
});

class Admin extends Component {
  constructor (){
    super()
    this.state = {
      username: '',
      password: ''
    }
    this.handlePassword = this.handlePassword.bind(this)
    this.handleUsername = this.handleUsername.bind(this)
    this.handleonclick_login = this.handleonclick_login.bind(this)
  }
  componentDidMount() {
    if (localStorage.getItem('token') !== ''){
      window.location.replace(store.getState().FullPath+'/Home');
      store.dispatch(email(''))
      store.dispatch(type(''))
      store.dispatch(accesstoken(''))

      localStorage.setItem('email', '');
      localStorage.setItem('utype', '');
      localStorage.setItem('token', '');
    }
  }
  handlePassword(e){
    this.setState({
      password: e.target.value
    })
  }
  handleUsername(e){
    this.setState({
      username: e.target.value
    })
  }
  async handleonclick_login() {
    if (this.state.username === '' && this.state.username === ''){
      alert('กรุณากรอก Username และ Password')
    }else {
      try {
        const res = await API().post('/users/login', {
          "user": {
            "username": this.state.username,
            "password": this.state.password,
          }
        })
        if (res.data.user.auth.type !== 'device'){
          store.dispatch(email(res.data.user.auth.email))
          store.dispatch(type(res.data.user.auth.type))
          store.dispatch(accesstoken(res.data.user.auth.token))
    
          localStorage.setItem('email', res.data.user.auth.email);
          localStorage.setItem('utype', res.data.user.auth.type);
          localStorage.setItem('token', res.data.user.auth.token);
    
          window.location.replace(store.getState().FullPath+'/Home');
        } else {
          alert("ไม่สามารถล๊อกอิน")
        }
      }catch(e){
        alert("ไม่สามารถล๊อกอิน")
      }
    }
  }
  render() {
    const { classes } = this.props;
    // const currentPath = this.props.location.pathname;

    return (
      <>
        <div className={classes.bgimage}></div>
        <div className={classes.loginFrom}>
          <div className={classes.logoLogin}>
            {/* <img src={logoPTT} alt="logo-PTT-S1" className={classes.logoImage}></img> */}
          </div>
          <div className={classes.textLogin}>LOGIN</div>
          <div className={classes.textUsername}>
            <CssTextField
              style={{color: '#000000', width: "100%"}}
              className={classes.margin}
              label="Username"
              variant="outlined"
              id="custom-css-outlined-input-username"
              onChange={this.handleUsername}
            />
          </div>
          <div className={classes.textPassword}>
            <CssTextField
              type="password"
              style={{color: '#000000', width: "100%"}}
              className={classes.margin}
              label="Password"
              variant="outlined"
              id="custom-css-outlined-input-password"
              onChange={this.handlePassword}
            />
          </div>
          <div className={classes.textComfirm}>
            <BootstrapButton 
              variant="contained" 
              color="primary" 
              disableRipple 
              className={classes.margin}
              onClick={this.handleonclick_login}
            >
              Login
            </BootstrapButton>
          </div>
        </div>
      </>
      // <>
      //   {/* <img src={backgroundImage} width="100%"></img> */}
      //   {/* <React.Fragment>
      //     <CssBaseline />
      //     <div className={classes.root}>
      //       <Grid container justify="center">
      //         <Grid item md={12}>
      //           <img src={backgroundImage}></img>
      //         </Grid>
      //       </Grid>
      //     </div>
      //   </React.Fragment> */}
      // </>
    );
  }
}

export default withStyles(styles)(Admin);
