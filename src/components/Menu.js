// import { store } from './../store'
// var privilege = store.getState().type
// console.log(privilege)
// var privilege = localStorage.getItem('type')
// console.log(privilege)
var Menu = {}
Menu['admin'] = [
  {
    label: 'Home',
    pathname:"/Home"
  },
  {
    label: "Chart",
    pathname: "/Realtime"
  },
  {
    label: "History",
    pathname: "/History"
  },
  {
    label: 'PumpRunStatus',
    pathname: "/PumpRunStatus"
  },
  {
    label: "PumpCalculate",
    pathname: "/PumpCalculate"
  },
  {
    label: "WellParameter",
    pathname: "/WellParameter"
  },
  {
    label: "Admin",
    pathname: "/Admin"
  }, {
    label: "Manage Devices",
    pathname: "/ManageDevices"
  }
]
Menu['user'] = [
  {
    label: 'Home',
    pathname:"/Home"
  },
  {
    label: "Chart",
    pathname: "/Realtime"
  },
  {
    label: "History",
    pathname: "/History"
  },{
    label: 'PumpRunStatus',
    pathname: "/PumpRunStatus"
  },
  {
    label: "PumpCalculate",
    pathname: "/PumpCalculate"
  },
  {
    lable: "WellParameter",
    pathname: "/WellParameter"
  },
]
// var Menu = []
// if (privilege === 'admin'){
//   Menu = [
//     {
//       label: 'Visualisation',
//       pathname:"/Visualisation"
//     },
//     {
//       label: "RealTime",
//       pathname: "/Realtime"
//     },
//     {
//       label: "History",
//       pathname: "/History"
//     },
//     {
//       label: "Admin",
//       pathname: "/Admin"
//     }, {
//       label: "Manage Devices",
//       pathname: "/ManageDevices"
//     }
//   ];
// }else {
//   Menu = [
//     {
//       label: 'Visualisation',
//       pathname:"/Visualisation"
//     },
//     {
//       label: "RealTime",
//       pathname: "/Realtime"
//     },
//     {
//       label: "History",
//       pathname: "/History"
//     }
//   ];
// }

export default Menu;