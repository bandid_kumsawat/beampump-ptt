import React, { Component } from 'react';
import * as d3 from 'd3';
import withStyles from '@material-ui/styles/withStyles';
import './DynaChart.css'

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  }
});

class UpdateChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
      Loads: this.props.Loads,
      Positions: this.props.Positions,
      width: (this.props.width === undefined ? 600 : this.props.width),
      height: (this.props.height === undefined ? 400 : this.props.height),
      title: (this.props.subTitle === undefined ? "" : this.props.subTitle),
      subTitle: (this.props.title === undefined ? '' : this.props.title),
      PropsClassName: this.props.className
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.data = Array
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
    this.data1 = [
      {ser1: 0.3, ser2: 4},
      {ser1: 2, ser2: 16},
      {ser1: 3, ser2: 8}
    ];
  
    this.data2 = [
      {ser1: 1, ser2: 7},
      {ser1: 4, ser2: 1},
      {ser1: 6, ser2: 8}
    ];

    this.margin = {top: 50, right: 30, bottom: 30, left: 50}
    this.width = this.props.width - this.margin.left - this.margin.right
    this.height = this.props.height - this.margin.top - this.margin.bottom
    this.svg = Object
    this.x = Object
    this.y = Object
    this.xAxis = Object
    this.yAxis = Object
  }
  // componentWillReceiveProps () {
      
  // }
  componentDidUpdate() {

  }
  componentDidMount(){
    this.LineChartFunction();
    this.data1 = []
    this.props.Loads.forEach((item ,index) => {
      this.data1.push({
        ser1: this.props.Positions[index], ser2: item
      })
    })
    this.update(this.data1);
    // this.update(this.data2);
  }

  LineChartFunction() {
    this.svg = d3.select(this.myRef.current)
      .append("svg")
      .attr("width", this.width + this.margin.left + this.margin.right)
      .attr("height", this.height + this.margin.top + this.margin.bottom)
      .attr("viewBox", "0 0 "+(this.width + this.margin.left + this.margin.right + 40)+" " + (this.height + this.margin.top + this.margin.bottom + 40))
      .append("g")
      .attr("transform","translate(" + this.margin.left + "," + this.margin.top + ")");

    // sub title line chart
    this.svg.append("text")
      .attr('y', -15)
      .attr("class", "sub-title")
      .attr('x', (this.width / 2) + 0)
      .attr('fill', "#000000")
      .attr("text-anchor","middle")
      .text('LKU-A05T') 
    // Yaxis title
    this.svg.append("text")
      .attr("text-anchor", "end")
      .attr("transform", "rotate(-90)")
      .attr("y", -(this.margin.left)+20)
      .attr("x", -(this.height / 2) + 20)
      .text("Loads")
    // Xaxis title
    this.svg.append("text")
      .attr("text-anchor", "end")
      .attr("x", this.width / 2)
      .attr("y", this.height + this.margin.top + 10)
      .text("Positions");
    this.x = d3.scaleLinear().range([0, this.width]);
    this.xAxis = d3.axisBottom().scale(this.x);
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .attr("class","myXaxis")

    this.y = d3.scaleLinear().range([this.height, 0]);
    this.yAxis = d3.axisLeft().scale(this.y);
    this.svg.append("g")
      .attr("class","myYaxis") 
  }


  update(data){
    this.x.domain([0, d3.max(data, function(d) { return d.ser1 }) ]);
    this.svg.selectAll(".myXaxis").transition()
      .duration(300)
      .call(this.xAxis);

    this.y.domain([0, d3.max(data, function(d) { return d.ser2  }) ]);
    this.svg.selectAll(".myYaxis")
      .transition()
      .duration(300)
      .call(this.yAxis);

    var u = this.svg.selectAll(".lineTest")
      .data([data], function(d){ return d.ser1 });

    var self = this
    u
      .enter()
      .append("path")
      .attr("class","lineTest")
      .merge(u)
      .transition()
      .duration(300)
      .attr("d", d3.line()
        .curve(d3.curveCatmullRom.alpha(1))
        .x(function(d) { return self.x(d.ser1); })
        .y(function(d) { return self.y(d.ser2); }))
        .attr("fill", "none")
        .attr("stroke", "steelblue")
        .attr("stroke-width", 2.5)
  }

  render() {
    const { classes } = this.props;
    return (
        <div 
          className={((this.props.border) ? classes.root : classes.rootNoBorder) + " " + this.state.PropsClassName + " " + ((this.props.hover) ? "isHover" : "") + this.classselected}  
          ref={this.myRef}
        >
        </div>
    )
  }
}

export default withStyles(styles)(UpdateChart)