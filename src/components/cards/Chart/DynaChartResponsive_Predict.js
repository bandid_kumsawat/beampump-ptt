import React, { Component } from 'react';
import * as d3 from 'd3';
import moment from 'moment'
import withStyles from '@material-ui/styles/withStyles';
import './DynaChartResponsive_Predict.css'
import { addwellselect } from '../../../actions';
// import pg from './datapg.json'

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
      Loads: this.props.Loads,
      Positions: this.props.Positions,
      PumpCardLoads: this.props.PumpCardLoads,
      PumpCardPositions: this.props.PumpCardPositions,
      width: (this.props.width === undefined ? 600 : this.props.width),
      height: (this.props.height === undefined ? 400 : this.props.height),
      title: (this.props.subTitle === undefined ? "" : this.props.subTitle),
      subTitle: (this.props.title === undefined ? '' : this.props.title),
      PropsClassName: this.props.className
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.data = Array
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
  }
  // componentWillReceiveProps () {
      
  // }
  componentDidUpdate() {
    // this.LineChartFunction();
  }
  componentDidMount(){
    this.LineChartFunction();
    // this.classselected = this.check_select()
  }
  LineChartFunction(){
    var surfaces = []
    var cards = []
    
    // this.state.PumpCardLoads.forEach((item, index,) => {
    //   cards.push({
    //     Positions: item,
    //     Loads: this.state.PumpCardPositions[index],
    //   })
    // })

    this.state.Positions.forEach((item, index,) => {
      surfaces.push({
        Positions: item,
        Loads: this.state.Loads[index],
      })
    })
    
    var all = cards.concat(surfaces)
    
    var margin = {top: 10, right: 20, bottom: 20, left: 50},
        width, 
        height = 300 - margin.top - margin.bottom;
    
    var yScale = d3.scaleLinear().range([height, 0]);
    var yAxis = d3.axisLeft().scale(yScale);
    
    var lineSurfaces = d3.line().curve(d3.curveCatmullRom.alpha(1));
    var lineCards = d3.line().curve(d3.curveCatmullRom.alpha(1));
    
    var svg = d3.select(this.myRef.current)
        .append("svg")
        .attr("height", height + margin.top + margin.bottom);
    
    var artboard = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    var xScale = d3.scaleLinear();
    var xScaleCard = d3.scaleLinear();
    var xAxis = d3.axisBottom().scale(xScale);
    xScale.domain(
      d3.extent(surfaces, function(d) { return d.Positions; })
    );
    // xScale.domain(
    //   d3.min(all, function(d) { return Math.floor(d.Positions); }), 
    //   d3.max(all, function(d) { return Math.floor(d.Positions); })
    // )
    xScaleCard.domain(d3.extent(surfaces, function(d) { return d.Positions; }));

    yScale.domain([
      d3.min(surfaces, function(d) { return Math.floor(d.Loads); }), 
      d3.max(surfaces, function(d) { return Math.floor(d.Loads); })
    ]);
    
    var path = artboard.append("path").data([surfaces])
        .style('fill', '#FF46A420')
        .style('stroke', '#FF46A4')
        .style('stroke-width', '2px');
    
    var pathcard = artboard.append("path").data([cards])
        .style('fill', '#27AE6020')
        .style('stroke', '#27AE60')
        .style('stroke-width', '2px');

    var xAxisEl = artboard.append("g")
        .attr("transform", "translate(0," + height + ")");
        
    var yAxisEl = artboard.append("g")
        .call(yAxis);
    
    var self = this

    xAxisEl
      .attr('class', "grid")
      .attr("transform", "translate(0," + height + ")")
      .call(
        xAxis.ticks(10)
        .tickSize(-height)
        .tickFormat((d) => {
            return d3.format("")(d)
        })
      )

    var gridy = yAxisEl
      .attr("class", "grid")

    function drawChart() {
      console.log(self)
      try{
        width = parseInt(d3.select(self.myRef.current).style('width'), 10) - margin.left - margin.right;
        console.log(width)
      }catch(e){
        console.log(e)
      }

      svg.attr("width", width + margin.left + margin.right);
      
      xScale.range([0, width]);
      
      xAxis.scale(xScale);

      xScaleCard.range([0, width]);
      
      xAxis.scale(xScaleCard);

      xAxisEl.call(xAxis);
      
      gridy.call(yAxis.ticks(10)
        .tickSize(-width)
        .tickFormat(function (d) {
            return d3.format("")(d)
        }))

      lineCards.x(function(d) { return xScale(d.Positions); })
        .y(function(d) { return yScale(d.Loads); });

      lineSurfaces.x(function(d) { return xScale(d.Positions); })
        .y(function(d) { return yScale(d.Loads); });
      
      path.attr('d', lineSurfaces);
      pathcard.attr('d', lineCards)
    }
    
    drawChart();
    
    window.addEventListener('resize', drawChart);
  }

  check_select () {
    this.store.getState().TempWellID.forEach((item ,index) => {
      try{
        if (this.store.getState().selectWell[0].id === item){
          document.getElementById(item).style.borderTop = "4px solid #0a88beaf";
          document.getElementById(item).style.borderRight = "4px solid #0a88beaf";
          document.getElementById(item).style.borderBottom = "10px solid #0a88beaf";
          document.getElementById(item).style.borderLeft = "4px solid #0a88beaf";
        } else {
          document.getElementById(item).style.borderTop = "4px solid #8c8b8b44";
          document.getElementById(item).style.borderRight = "4px solid #8c8b8b44";
          document.getElementById(item).style.borderBottom = "10px solid #8c8b8b44";
          document.getElementById(item).style.borderLeft = "4px solid #8c8b8b44";
        }
      } catch (e){
        console.log(e)
      }
    })
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={"chart-main"}>
        <div className={"chart-header-title"}>{"PREDICTION"}</div>
        <div className={"chart-header"}>{moment(this.state.title).format('DD-MMMM-YYYY HH:mm:ss')}</div>
        <div className={'chart-body'}>
          <div className={'chart-yAis-name'} style={
            {
              // transform: 'rotate(-90deg)',
              // transformOrigin: "0 0"
            }
          }>
            {/* <div style={{transform: 'rotate(-90deg)'}}>
              Loads (lbs.)
            </div> */}
            Loads&nbsp;(lbs.)
          </div>
          <div 
            // className={((this.props.border) ? classes.root : classes.rootNoBorder) + " " + this.state.PropsClassName + " " + ((this.props.hover) ? "isHover" : "") + this.classselected}  
            className={"chart-chart"}
            ref={this.myRef}
            id={this.props.cid}
            onClick={() => {
                if (this.props.onclick){
                  this.store.dispatch(addwellselect([undefined]))
                  const callFn  = () => {
                    this.store.dispatch(addwellselect([this.store.getState().wellResult.data[this.props.index]]))
                    localStorage.setItem('select_Dynachart', JSON.stringify(this.store.getState().wellResult.data[this.props.index]));
                    this.check_select()
                  }
                  setTimeout(() => { callFn() }, 1);
                }
            }}
          >
          </div>
        </div>
        <div className={"chart-xAxis-name"}>Postions (Inch.)</div>
        {/* <div className={"chart-footer"}>
          <div className={"chart-lebel"}>
            <div className={"chart-box"} style={
              {
                backgroundColor: "#27AE6020",
                border: "solid 2px #29abe2",
              }
            }></div>
            <div className={"chart-title"}>Surface Card</div>
          </div>
          <div className={"chart-lebel"}>
            <div className={"chart-box"} style={
              {
                backgroundColor: "#27AE6020",
                border: "solid 2px #27AE60",
              }
            }></div>
            <div className={"chart-title"}>Pump Card</div></div>
        </div> */}
      </div>
    )
  }
}

export default withStyles(styles)(DynaChart)