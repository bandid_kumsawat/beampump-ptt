var data = [
	{year: 2000, population: 1192},
	{year: 2001, population: 1234},
	{year: 2002, population: 1463},
	{year: 2003, population: 1537},
	{year: 2004, population: 1334},
	{year: 2005, population: 1134},
	{year: 2006, population: 1234},
	{year: 2007, population: 1484},
	{year: 2008, population: 1562},
	{year: 2009, population: 1427},
	{year: 2010, population: 1325},
	{year: 2011, population: 1484},
	{year: 2012, population: 1661},
	{year: 2013, population: 1537},
	{year: 2014, population: 1334},
	{year: 2015, population: 1134},
	{year: 2016, population: 1427}
];

var parseDate = d3.timeParse("%Y");

function type(dataArray) {
	dataArray.forEach(function(d) {
		d.year = parseDate(d.year);
		d.retention = +d.population;
	});
	return dataArray;
}
data = type(data);

var margin = {top: 30, right: 20, bottom: 30, left: 50},
		width, 
    height = 250 - margin.top - margin.bottom;

var xScale = d3.scaleTime();
var yScale = d3.scaleLinear().range([height, 0]);

var xAxis = d3.axisBottom().scale(xScale);
var yAxis = d3.axisLeft().scale(yScale);

var line = d3.line();

var svg = d3.select("body")
    .append("svg")
		.attr("height", height + margin.top + margin.bottom);

var artboard = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

xScale.domain(d3.extent(data, function(d) { return d.year; }));
yScale.domain([
		d3.min(data, function(d) { return Math.floor(d.population - 200); }), 
		d3.max(data, function(d) { return Math.floor(d.population + 200); })
	]);

var path = artboard.append("path").data([data])
		.style('fill', 'none')
		.style('stroke', 'steelblue')
		.style('stroke-width', '2px');

var xAxisEl = artboard.append("g")
    .attr("transform", "translate(0," + height + ")");
    
var yAxisEl = artboard.append("g")
		.call(yAxis);

function drawChart() {

	width = parseInt(d3.select('body').style('width'), 10) - margin.left - margin.right;
	
	svg.attr("width", width + margin.left + margin.right);
	
	xScale.range([0, width]);
	
	xAxis.scale(xScale);
	
	xAxisEl.call(xAxis);
	
	line.x(function(d) { return xScale(d.year); })
		.y(function(d) { return yScale(d.population); });
	
	path.attr('d', line);
}

drawChart();

window.addEventListener('resize', drawChart);