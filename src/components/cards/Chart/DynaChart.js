import React, { Component } from 'react';
import * as d3 from 'd3';
import moment from 'moment'
import withStyles from '@material-ui/styles/withStyles';
import './DynaChart.css'
import { addwellselect } from './../../../actions';
// import pg from './datapg.json'

const styles = theme => ({
  root: {
    color: "#000000",
    fontSize: '1em',
    border: "2px solid #8c8b8b44",
    borderTop: "4px solid #8c8b8b44",
    borderBottom: "10px solid #8c8b8b44",
    borderRight: "4px solid #8c8b8b44",
    borderLeft: "4px solid #8c8b8b44",
    borderRadius: "3px"
  },
  rootNoBorder: {
    color: "#000000",
    fontSize: '1em',
  }
});

class DynaChart extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.myRef = React.createRef();
    this.state = {
      Loads: this.props.Loads,
      Positions: this.props.Positions,
      PumpCardLoads: this.props.PumpCardLoads,
      PumpCardPositions: this.props.PumpCardPositions,
      width: (this.props.width === undefined ? 600 : this.props.width),
      height: (this.props.height === undefined ? 400 : this.props.height),
      title: (this.props.subTitle === undefined ? "" : this.props.subTitle),
      subTitle: (this.props.title === undefined ? '' : this.props.title),
      PropsClassName: this.props.className
    }
    this.width = this.props.width
    this.height = this.props.height
    this.x = Object
    this.y = Object
    this.data = Array
    this.svg = Object
    this.valueline = Object
    this.classselected = ''
  }
  // componentWillReceiveProps () {
      
  // }
  componentDidUpdate() {
    // this.LineChartFunction();
  }
  componentDidMount(){
    this.LineChartFunction();
    // this.classselected = this.check_select()
  }
  LineChartFunction(){
    var that = this

    var data = []
    , datapc = []
    if (this.props.PumpCardLoads !== undefined && this.props.PumpCardPositions !== undefined){
      this.state.PumpCardLoads.forEach((item, index, arr) => {
        var tttt = this.state.PumpCardPositions[index]
        datapc.push({
            "x": Number(tttt),
            "y": Number(item),
        })
      })
    }

    this.state.Loads.forEach((item, index, arr) => {
      var tttt = this.state.Positions[index]
      data.push({
          "x": Number(tttt),
          "y": Number(item),
      })
    })
    this.state.Loads.forEach((item, index, arr) => {
      var tttt = this.state.Positions[index]
      data.push({
          "x": Number(tttt),
          "y": Number(item),
      })
    })

    var max = () => { 
      var temp = Math.max.apply(Math, data.map(function(o) { 
          return o.y;  
      })); 
      return temp
    }
    var min = () => { 
      var temp = Math.min.apply(Math, data.map(function(o) { 
          return o.y;  
      })); 
      return temp
    } 

    var margin = {
      top: 10,
      right: 20,
      bottom: 50,
      left: 70
    }

    var padding_top = 30
    
    var default_width = this.width - margin.left - margin.right;
    
    var default_height = this.height - margin.top - margin.bottom;
    
    var default_ratio = default_width / default_height;

    const set_size = () => {
        var w, h
        var current_width = window.innerWidth;
        
        var current_height = window.innerHeight;

        var current_ratio = current_width / current_height;

        // desktop
        if (current_ratio > default_ratio) {
            h = default_height;
            w = default_width;

        // mobile
        } else {
            margin.left = 40
            w = current_width - 40;
            h = w / default_ratio;
        }
        // Set new width and height based on graph dimensions
        this.width = w - 50 - margin.right;
        this.height = h - margin.top - margin.bottom;
    }

    set_size();
    //end responsive graph cod

    // set the ranges
    this.x = d3.scaleTime().range([0, this.width]);
    this.y = d3.scaleLinear().range([this.height, 0])

    if (this.props.autoscale){
      // auto scale
      this.x.domain([d3.min(data, function(d){
        return d.x;
      }), d3.max(data, function (d) {
          return d.x;
      })]);
      this.y.domain([d3.min(data, function(d){
        return d.y;
      }), d3.max(data, function (d) {
          return d.y;
      })]);
    }else {
      // console.log('this.state.PumpCardLoads')
      // console.log(this.state.PumpCardLoads)
      // console.log('this.state.PumpCardLoads')
      var alldata = []
      this.state.PumpCardLoads.forEach((item, index, arr) => {
        var tttt = this.state.PumpCardPositions[index]
        alldata.push({
            "x": Number(tttt),
            "y": Number(item),
        })
      })
      this.state.Loads.forEach((item, index, arr) => {
        var tttt = this.state.Positions[index]
        alldata.push({
            "x": Number(tttt),
            "y": Number(item),
        })
      })
      // console.log(alldata)
      this.x.domain([d3.min(alldata, function (d) {
        return d.x;
      }), d3.max(alldata, function (d) {
        return d.x;
      })]);
      this.y.domain([d3.min(alldata, function (d) {
        return d.y;
      }), d3.max(alldata, function (d) {
          return d.y;
      })]);
      // this.x.domain([0, d3.max(alldata, function (d) {
      //   return d.x;
      // })]);
      // this.y.domain([0, d3.max(alldata, function (d) {
      //     return d.y;
      // })]);
    }

    // define the line
    this.valueline = d3.line()
        .curve(d3.curveCatmullRom.alpha(1))
        .x(function (d) {
            return that.x(d.x);
        })
        .y(function (d) {
            return that.y(d.y);
        });

    // append the svg object to the body of the page
    
    // var svg = d3.select(this.myRef.current).remove()
    this.svg = d3.select(this.myRef.current).append("svg")
        // .attr("style", "padding-top: "+padding_top+";")
        .attr("width", this.width + margin.left + margin.right)
        .attr("height", this.height + margin.top + margin.bottom + padding_top)
        .attr("viewBox", "0 0 "+(this.width + margin.left + margin.right + 40)+" " + (this.height + margin.top + margin.bottom + padding_top + ((this.props.detailline) ? 20 : 0)))
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + (margin.top + 40) + ")")
      
    // title line chart
    // var subtitle = moment(this.state.title).format('DD-MMMM-YYYY HH:MM:SS');
    // var subtitle = ((this.state.title !== "") ? moment(this.state.title).format('DD-MMMM-YYYY HH:mm:ss') : "");

    var subtitle = ((this.props.fill === '#FFF7FB') ? moment(this.state.title).format('DD-MMMM-YYYY') : moment(this.state.title).format('DD-MMMM-YYYY HH:mm:ss'))

    this.svg.append("text")
        // .attr('y', -40)
        .attr('y', ((this.props.PumpCardLoads !== undefined && this.props.PumpCardPositions !== undefined) ? -20 : -40))
        .attr("class", "title-line-chart")
        .attr('x', (this.width / 2) + 0)
        .attr('fill', "#000000")
        .attr("style", "font-size: 1.4em;font-style: '\"Lato\",sans-serif';")
        .attr("text-anchor","middle")
        .text(subtitle)

    // sub title line chart
    this.svg.append("text")
        .attr('y', -15)
        .attr("class", "sub-title")
        .attr('x', (this.width / 2) + 0)
        .attr('fill', "#000000")
        .attr("text-anchor","middle")
        .text(this.state.subTitle)

    // Add the trendline
    this.svg.append("path")
        .data([data])
        .attr("class", "line")
        .attr("d", this.valueline)
        .attr("stroke", this.props.color)
        .attr("stroke-width", 2)
        .attr("fill", this.props.fill)
      
    // plot Pump Card
    if (this.props.PumpCardLoads !== undefined && this.props.PumpCardPositions !== undefined){
      // define the line
      this.svg.append("path")
          .data([datapc])
          .attr("class", "line")
          .attr("d", this.valueline)
          .attr("stroke", "#27AE6080")
          .attr("stroke-width", 2)
          .attr("fill", "#27AE6020")
    }

    this.svg.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + this.height + ")")
        .call(d3.axisBottom(this.x)
          .ticks(10)
          .tickSize(-this.height)
          .tickFormat((d) => {
              return d3.format("")(d)
          }));

    var scale
    
    if (this.props.PumpCardLoads !== undefined && this.props.PumpCardPositions !== undefined){
      scale = d3.scaleLinear()
          .domain([0 , max()])
          .range([this.height, 0]);
    }else{
      scale = d3.scaleLinear()
          .domain([min() , max()])
          .range([this.height, 0]);
    }
    this.svg.append("text")
        .attr("text-anchor", "end")
        .attr("x", this.width / 2)
        .attr("y", this.height + margin.top + 20)
        .text("Positions (Inch.)");

    // Y
    this.svg.append("g")
        .attr("class", "grid")
        .call(d3.axisLeft(this.y).scale(scale)
          .ticks(10)
          .tickSize(-this.width)
          .tickFormat(function (d) {
              return d3.format("")(d)
          }));

    this.svg.append("text")
        .attr("text-anchor", "end")
        .attr("transform", "rotate(-90)")
        .attr("y", -(margin.left)+20)
        .attr("x", -(this.height / 2) + 20)
        .text("Loads (lbs.)")

    // this.svg.append("div").attr('id', this.props.cid)

    // var divnode = d3.select("#" + this.props.cid);
    // divnode.append('rect')
    //   .attr('y', (this.height) + 40)
    //   .attr('x', 0)
    //   .attr('width', 15)
    //   .attr('height', 15)
    //   .attr('fill', '#E8F7FF')
    //   .attr("stroke", "#29abe2")
    //   .attr("stroke-width", 2)

    if (this.props.detailline){
      // detail line color dynachart
      this.svg.append("rect")
          .attr('y', (this.height) + 40)
          .attr('x', 0)
          .attr('width', 15)
          .attr('height', 15)
          // ((this.props.color === '#FF46A4') ? "#FFF7FB" : "#E8F7FF")
          .attr('fill', ((this.props.fill === '#FFF7FB') ? "#FFF7FB" : "#E8F7FF"))
          .attr("stroke", ((this.props.color === '#FF46A4') ? "#FF46A4" : "#29abe2"))
          .attr("stroke-width", 2)
          this.svg.append("text")
          .attr("y", (this.height) + 52)
          .attr("x", 20)
          .text("Surface Card")    
      // detail line color pump card
      this.svg.append("rect")
          .attr('y', (this.height) + 40)
          .attr('x', 200)
          .attr('width', 15)
          .attr('height', 15)
          .attr('fill', '#27AE6020')
          .attr("stroke", "#27AE6080")
          .attr("stroke-width", 2)
      this.svg.append("text")
          .attr("y", (this.height) + 52)
          .attr("x", 220)
          .text("Pump Card")  
    }
  }

  check_select () {
    this.store.getState().TempWellID.forEach((item ,index) => {
      try{
        if (this.store.getState().selectWell[0].id === item){
          document.getElementById(item).style.borderTop = "4px solid #0a88beaf";
          document.getElementById(item).style.borderRight = "4px solid #0a88beaf";
          document.getElementById(item).style.borderBottom = "10px solid #0a88beaf";
          document.getElementById(item).style.borderLeft = "4px solid #0a88beaf";
        } else {
          document.getElementById(item).style.borderTop = "4px solid #8c8b8b44";
          document.getElementById(item).style.borderRight = "4px solid #8c8b8b44";
          document.getElementById(item).style.borderBottom = "10px solid #8c8b8b44";
          document.getElementById(item).style.borderLeft = "4px solid #8c8b8b44";
        }
      } catch (e){
        console.log(e)
      }
    })
  }

  render() {
    const { classes } = this.props;
    return (
        <div 
          className={((this.props.border) ? classes.root : classes.rootNoBorder) + " " + this.state.PropsClassName + " " + ((this.props.hover) ? "isHover" : "") + this.classselected}  
          ref={this.myRef}
          id={this.props.cid}
          onClick={() => {
              if (this.props.onclick){
                this.store.dispatch(addwellselect([undefined]))
                const callFn  = () => {
                  this.store.dispatch(addwellselect([this.store.getState().wellResult.data[this.props.index]]))
                  localStorage.setItem('select_Dynachart', JSON.stringify(this.store.getState().wellResult.data[this.props.index]));
                  this.check_select()
                }
                setTimeout(() => { callFn() }, 1);
              }
          }}
        >
        </div>
    )
  }
}

export default withStyles(styles)(DynaChart)