import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { Grid } from '@material-ui/core';
import './css/Beam_MDevice.css'
import Button from '@material-ui/core/Button';
import API from './../../api/server'

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const ColorButton = withStyles((theme) => ({
  root: {
    width: "100%",
    color: '#ffffff',
    backgroundColor: '#0a88be',
    '&:hover': {
      backgroundColor: '#0a88be',
    },
  },
}))(Button);

// const ColorButtonRed = withStyles((theme) => ({
//   root: {
//     width: "100%",
//     color: '#ffffff',
//     backgroundColor: '#cd1e0cb5',
//     '&:hover': {
//       backgroundColor: '#cd1e0cb5',
//     },
//   },
// }))(Button);

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
    paddingTop:'20px'
  },
  hieghtlightchart: {
    borderTop: "3px solid #ffffffff"
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'left'
  },
  containerCenter:{

  },
  title: {
    width: "100%",
    textAlign: "center",
    margin: "20px",
    padding: "8px",
    borderBottom: '1px solid #0000007a',
    fontSize: '20px'
  },
  title2: {
    width: "100%",
    textAlign: "center",
    margin: "0px",
    padding: "8px",
    fontSize: '20px'
  },
  spacingPadding: {
    paddingRight: "2%",
    paddingLeft: "2%",
  },
  btncustom:{
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "40%",
    backgroundColor: '#0a88beaa',
    border: "0px "
  }
});

class BeamManageDevices extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      devicename: '',
      sensors: [],
      open: false,
      Parameter: [],
      pname: "",
      pvalue: '',
      Parameter_main: {},
      station_id: ''
    }
    this.handleDevice = this.handleDevice.bind(this)
    this.handleConfirmDeviceName = this.handleConfirmDeviceName.bind(this)
    this.handleDeleteDevicebyid = this.handleDeleteDevicebyid.bind(this)
    this.handleClickOpen = this.handleClickOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleParameterValue = this.handleParameterValue.bind(this)
    this.handlepnameChange = this.handlepnameChange.bind(this)
    this.handlepvalueChange = this.handlepvalueChange.bind(this)
    this.handleDeleteParameterbyid = this.handleDeleteParameterbyid.bind(this)
  }
  async componentDidMount() {
    const res = await API().get("/sensors")
    if (res.data.msg === undefined){
      this.setState({sensors: res.data})
    }
  }
  handleDevice (e) {
    this.setState({devicename: e.target.value})
  }
  async handleConfirmDeviceName () {
    const res = await API().put("/sensors", {
      "station_id": this.state.devicename
    })
    if (res.status === 200) {
      alert('เพิ่มอุปกรณ์แล้ว')
      this.state.sensors.push(res.data.result)
      this.setState({sensors: this.state.sensors})
      this.setState({devicename: ''})
    }
  }
  async handleDeleteDevicebyid(e){
    var id = e.target.value
    const res = await API().delete("/sensors?id=" +id, {
      "id": id
    },{
      'Content-Type': 'application/json'
    })
    if (res.data.msg === 1){
      alert('ลบอุปกรณ์แล้ว')
      window.location.reload()
      // const res = await API().get("/sensors")
      // console.log(res.data)
      // if (res.data.msg === undefined){
      //   this.setState({sensors: res.data})
      // }
    }else {
      alert('ลบอุปกรณ์ไม่สำเร็จ')
    }
  }
  handleClickOpen(e){
    // console.log(JSON.parse(e.target.value))
    var Parameter = JSON.parse(e.target.value)[0]
    var station_id = JSON.parse(e.target.value)[1]
    if (Array.isArray(Parameter)){
      Parameter = {}
    }
    this.setState({Parameter_main: Parameter})
    var newParameter = []
    for (var name in Parameter){
      newParameter.push({
        name: name, 
        value: Parameter[name]
      })
    }
    this.setState({Parameter: newParameter})
    this.setState({station_id: station_id})
    this.setState({open: true})
  };
  handleClose(e){
    this.setState({Parameter: []})
    this.setState({open: false})
  };
  handleParameterValue(e){
    var Parameter = this.state.Parameter
    var temp = this.state.Parameter_main
    for (var i = 0; i< Parameter.length;i++){
      temp[this.state.pname] = Parameter[i].value
    }
    temp[this.state.pname] = this.state.pvalue
    this.setState({Parameter_main: temp})
    var newParameter = []
    for (var name in temp){
      newParameter.push({
        name: name, 
        value: temp[name]
      })
    }
    this.setState({Parameter: newParameter})
    this.update()
  }
  handlepnameChange(e) {
    this.setState({pname: e.target.value})
  }
  handlepvalueChange(e) {
    this.setState({pvalue: e.target.value})
  }
  handleDeleteParameterbyid(e){
    var temp = this.state.Parameter_main
    delete temp[e.target.value];  
    var newParameter = []
    for (var name in temp){
      newParameter.push({
        name: name, 
        value: temp[name]
      })
    }
    this.setState({Parameter: newParameter})
    this.setState({Parameter_main: temp})
    this.update()
  }
  async update(){
    console.log({
      station_id: this.state.station_id,
      Parameter: this.state.Parameter_main
    })
    const res = await API(this.store.getState().AccessToken).post('/sensors',{
      station_id: this.state.station_id,
      Parameter: this.state.Parameter_main
    },)
    console.log(res)
    window.location.reload()
  }
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
          <Grid container spacing={3}>
              <Grid item sm={4}>
                เพิ่มอุปกรณ์
              </Grid>
            </Grid>
            <Grid container spacing={3}>
              <Grid item sm={3}></Grid>
              <Grid item sm={3}>
                <FormControl fullWidth className={classes.margin}>
                  <InputLabel htmlFor="standard-adornment-amount">ชื่ออุปกรณ์</InputLabel>
                  <Input
                    id="standard-adornment-amount"
                    value={this.state.devicename}
                    onChange={this.handleDevice}
                  />
                </FormControl>
              </Grid>
              <Grid item sm={3}>
                <ColorButton 
                  className={classes.buttonMargin}
                  variant="outlined" 
                  color="default" 
                  disabled={( this.state.devicename === "")}
                  onClick={this.handleConfirmDeviceName}
                >
                  เพิ่มอุปกรณ์
                </ColorButton>
              </Grid>
              <Grid item sm={3}></Grid>
            </Grid>
            <Grid container spacing={3}>
              <Grid item sm={3}></Grid>
              <Grid item sm={6}>
                <table id="show-device">
                  <tbody>
                    <tr>
                      <th>ชื่ออุปกรณ์</th>
                      <th>จัดการอุปกรณ์</th>
                      <th>จัดการพารามิเตอร์</th>
                    </tr>
                    {
                      ((this.state.sensors.length !== 0) ? this.state.sensors.map((item, index) => {
                        return <tr key={index}>
                          <td>{item.station_id}</td>
                          <td>
                            <button 
                              className={classes.btncustom}
                              value={item._id}
                              onClick={this.handleDeleteDevicebyid}
                            >
                              ลบ
                            </button>
                          </td>
                          <td>
                            <button 
                              className={classes.btncustom}
                              value={JSON.stringify([item.Parameter, item.station_id])}
                              onClick={this.handleClickOpen}
                            >
                              พารามิเตอร์
                            </button>
                          </td>
                        </tr>
                      }) : <tr key={0}><td></td><td></td></tr>)
                    }
                  </tbody>
                </table>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"จัดการพารามิเตอร์"}</DialogTitle>
          <DialogContent>
            <table id="show-parameter">
              <tbody>
                <tr>
                  <th>Parameter</th>
                  <th>Value</th>
                  <th>จัดการอุปกรณ์</th>
                </tr>
                {
                  ((this.state.Parameter[0] !== undefined) ? this.state.Parameter.map((item ,index) => {
                    return <tr key={index}>
                      <td>{item.name}</td>
                      <td>{item.value}</td>
                      <td>
                        <button 
                          className={classes.btncustom}
                          value={item.name}
                          onClick={this.handleDeleteParameterbyid}
                        >
                          ลบ
                        </button>
                      </td>
                    </tr>
                  }) : <tr></tr>)                
                }
                <tr>
                  <td><input type = 'text' placeholder={'เพิ่มพารามิเตอร์'} onChange={this.handlepnameChange} value={this.state.pname}></input></td>
                  <td><input type = 'text' placeholder={'เพิ่มค่า'} onChange={this.handlepvalueChange} value={this.state.pvalue}></input><button type = 'button' value='' onClick={this.handleParameterValue}>เพิ่ม</button></td>
                </tr>
              </tbody>
            </table>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              ยกเลิก
            </Button>
            {/* <Button onClick={this.handleClose} color="primary" autoFocus>
              ตกลง
            </Button> */}
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default withStyles(styles)(BeamManageDevices);