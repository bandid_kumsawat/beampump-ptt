import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';




const columns = [
    { id: 'index', label: 'taperNumber', minWidth: 100 },
    { id: 'rod_desc', label: 'Rod Description ', minWidth: 120 },
    { id: 'rod_count', label: 'number of rod', minWidth: 100 },
    { id: 'rod_length', label: 'rod Length', minWidth: 100 },
    { id: 'up_damp', label: 'Up Stroke Damping', minWidth: 70 },
    { id: 'down_damp', label: 'Down Stroke Damping', minWidth: 70 },
    { id: 'btn_edit' , label:"EDIT"},
    { id: 'btn_del' , label:"DEL"}
  ];
  
  const useStyles = makeStyles({
    root: {
      width: '98%',
    },
    container: {
      maxHeight: 440,
    },
  });
  
  export default function RodTable(props) {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    
    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };
  
    return (
      <Paper className={classes.root} style = {{margin: "0px 20px 20px 20px"}}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {props.PumpStatus.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,index_) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={index_}>
                    {columns.map((column,index ) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={index} align={column.align}>
                          {column.format && typeof value === 'number' ? column.format(value) : value}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        {/* <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={props.PumpStatus.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        /> */}
      </Paper>
    );
  }