import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import moment from 'moment'

const columns = [
  { id: 'Loads', label: 'Loads', minWidth: 170 },
  { id: 'Positions', label: 'Positions', minWidth: 100 }
];

var data = []
var temp = []

if (localStorage.getItem('select_Dynachart') === null){
  data = []
}else {
  data = JSON.parse(localStorage.getItem('select_Dynachart'))
  data.Loads.forEach((item, index) => {
    temp.push(
      { Loads: item, Positions: data.Positions[index] }
    )
  });
}

var rows = temp;
temp = []
const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  WellName: {
    fontSize: "18px",
    fontWeight: "500",
    marginLeft: "20px",
    paddingBotton: "20px"
  },
  WellInformation: {
    fontSize: "20px",
    fontWeight: "500",
    marginBotton: "20px",
    marginRight: '30%',
    marginLeft: '30%',
    textAlign: 'center',
    marginBottom: "20px",
    borderBottom: "2px solid #8c8b8b66"
  },
  WellClusterChart: {
    padding: "12px 30% 12px 30%",
    textAlign: "center",
    fontSize: "20px",
    fontWeight: "500",
  },
  TableInformation: {
    fontSize: "16px",
    width: "100%",
    paddingRight: '35%',
    paddingLeft: '35%',
    paddingBottom: "20px"
  }
});

export default function StickyHeadTable(props) {
  temp = []
  const store = props.store;
  if (store.getState().selectWell[0] === undefined) {
    rows = []
  }else {
    store.getState().selectWell[0].Loads.forEach((item ,index) => {
      temp.push(
        { Loads: item, Positions: store.getState().selectWell[0].Positions[index] }
      )
    })
  }
  rows = temp
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  // var localS = localStorage.getItem('select_Dynachart')
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <Card style={{borderTop: "3px solid #ffffff"}}>
        <CardContent>
          <div className={classes.WellInformation}>Loads and Positions Data</div>
          <table  className={classes.TableInformation}>
            <tbody>
              <tr>
                <td style={{textAlign: "left"}}>Well Name</td>
                <td style={{textAlign: "right", fontWeight: "350"}}>
                  {
                    ((store.getState().selectWell[0] === undefined) ? "" : store.getState().selectWell[0].station_id)
                  }
                </td>
              </tr>
              <tr>
                <td style={{textAlign: "left"}}>Card Report Date</td>
                <td style={{textAlign: "right", fontWeight: "350"}}>
                  {
                    ((store.getState().selectWell[0] === undefined) ? "" : moment(store.getState().selectWell[0].DateTime).format("DD-MMMM-YYYY HH:mm:ss"))
                  }
                </td>
              </tr>
              {/* <tr>
                <td style={{textAlign: "left"}}>Card Report Type</td>
                <td style={{textAlign: "right", fontWeight: "350"}}>
                  {
                    ((store.getState().selectWell[0] === undefined) ? "" : store.getState().selectWell[0].CardType)
                  }
                </td>
              </tr> */}
            </tbody>
          </table>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === 'number' ? column.format(value) : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </CardContent>
    </Card>
  );
}