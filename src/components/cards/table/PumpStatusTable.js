import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

const columns = [
  { id: 'index', label: '#', minWidth: 100 },
  { id: 'station_id', label: 'Station', minWidth: 100 },
  { id: 'start_time', label: 'Start Time', minWidth: 100 },
  { id: 'stop_time', label: 'Stop Time', minWidth: 100 },
  { id: 'period_ms', label: 'Period (m.)', minWidth: 170 },
  { id: 'type', label: 'Type', minWidth: 100 },
];

const useStyles = makeStyles({
  root: {
    width: '98%',
  },
  container: {
    maxHeight: 440,
  },
});

export default function BeamPumpStatusTable(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root} style = {{margin: "0px 20px 20px 20px"}}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {props.PumpStatus.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,index_) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={index_}>
                  {columns.map((column,index ) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={index} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={props.PumpStatus.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
}