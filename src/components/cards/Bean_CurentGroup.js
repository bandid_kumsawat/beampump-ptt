import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import { red } from '@material-ui/core/colors';

import DynaChart from './Chart/DynaChart'
import { store } from './../../store'

import "./css/Beam_DataGroup.css"

const styles = theme => ({
  container: {
    display: 'flex',  
    flexWrap: 'wrap',
  },
  root: {
    maxWidth: '100%',
    flexGrow: 1,
    margin: "10px 0px 10px 0px"
  },
  hieghtlightchart: {
    borderTop: "3px solid #0a88be"
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
    // paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  boxShow: {
    margin: "0px auto",
    float: "left!important",
    flexDirection: "row !important",
    display: "flex !important",
    position: "relative",
    minWidth: 0,
  },
  boxMain: {
    overflowX: "scroll !important",
  },
  DynaChart: {
    margin: "5px 10px 5px 0px",
    cursor: "grab",
  }
});

class BeamPreDictionGrouped extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      welldata: []
    }
  }

  componentDidMount () {
    if (this.store.getState().Prediction.data !== undefined){
      this.setState({welldata: this.store.getState().Prediction.data});
    }
  }

  genChart(CardDate , index, data, classes){
    // var length = this.store.getState().Prediction.data.length
    if (index === 0){
      return (
        <DynaChart
          cid={data[index].id}
          className={classes.DynaChart}
          store={store}
          key={index} 
          color='#FF46A4'
          fill='#FFF7FB'
          index={index}
          Loads={data[index].Loads} 
          Positions={data[index].Positions}
          width='500'
          height='300'
          // title={data[index].station_id}
          subTitle={data[index].DateTime}
          hover={true}
          border={true}
          onclick={false}

          detailline={true}
          PumpCardLoads={data[index].PumpCardLoads}
          PumpCardPositions={data[index].PumpCardPositions}
          autoscale={false}

          // detailline={false}
          // autoscale={true}
        />
      )
    }else {
      return (
        <DynaChart
          cid={data[index].id}
          className={classes.DynaChart}
          store={store}
          key={index} 
          color='#29abe2'
          fill='#E8F7FF'
          index={index}
          Loads={data[index].Loads} 
          Positions={data[index].Positions}
          width='500'
          height='300'
          // title={data[index].station_id}
          subTitle={data[index].DateTime}
          hover={true}
          border={true}
          onclick={false}

          detailline={true}
          PumpCardLoads={data[index].PumpCardLoads}
          PumpCardPositions={data[index].PumpCardPositions}
          autoscale={false}

          // detailline={false}
          // autoscale={true}
        />
      )
    }
  }

  handleDragResultQuery (classDrag){
    const slider = document.querySelector('.' + classDrag);

    if (slider !== null){
      let isDown = false;
      let startX;
      let scrollLeft;
      
      slider.addEventListener('mousedown', (e) => {
        isDown = true;
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.addEventListener('mouseleave', () => {
        isDown = false;
      });
      slider.addEventListener('mouseup', () => {
        isDown = false;
      });
      slider.addEventListener('mousemove', (e) => {
        if(!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
      });
    }
  }

  render() {
    const { classes } = this.props;
    this.handleDragResultQuery(classes.boxMain)
    return (
      <div className={classes.root}>
        <div className={classes.boxMain}>
          <div className={classes.boxShow + " "}>
            {
              ( 
                this.store.getState().Prediction.data !== undefined ? 
                  this.store.getState().Prediction.data.map((item, index) => {
                    return this.genChart(item.CardDate, index, this.store.getState().Prediction.data, classes)
                  })
                : "ไม่พบข้อมูล" 
              ) 
            }
          </div>
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(BeamPreDictionGrouped);