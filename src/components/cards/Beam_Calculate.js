import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import { 
  Grid,
  Button
} from '@material-ui/core';
import "./css/Beam_DataGroup.css"
import Wellinformation from './Beam_WellInformation'

const ColorButton = withStyles((theme) => ({
  root: {
    width: "100%",
    color: theme.palette.getContrastText('#0a88be'),
    backgroundColor: '#0a88be',
    '&:hover': {
      backgroundColor: '#0a88be',
    },
  },
}))(Button);

const styles = theme => ({
  container: {
    display: 'flex',  
    flexWrap: 'wrap',
  },
  root: {
    maxWidth: '100%',
    flexGrow: 1,
    margin: "10px 0px 10px 0px"
  },
});

class BeamCalculate extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      welldata: []
    }
  }

  componentDidMount () {

  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item sm={12} style={{paddingRight: "-10px"}}>
            <Grid container spacing={3}> 
              <Grid item sm={6} style={{paddingRight: "-10px"}}>
                <fieldset className={classes.FIELDSET}>
                  <legend className={classes.LEGEND}>Depths</legend>
                  <div style={{display: "flex"}}>
                    <div style={{paddingLeft: "20px"}}>Top Preforation</div>
                    <div style={{paddingLeft: "68px"}}>feet</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={666.3}></input></div>
                  </div>

                  <div style={{display: "flex",marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}>Bottom Preforation</div>
                    <div style={{paddingLeft: "44px"}}>feet</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={6496.1}></input></div>
                  </div>


                  <div style={{display: "flex",marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}>Anchor Depth</div>
                    <div style={{paddingLeft: "77px"}}>feet</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={6013.8}></input></div>
                  </div>


                  <div style={{display: "flex",marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}>Plug-Back TD</div>
                    <div style={{paddingLeft: "80px"}}>feet</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={9478.3}></input></div>
                  </div>

                  <div style={{display: "flex",marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}>Pump Measured</div>
                    <div style={{paddingLeft: "64px"}}>feet</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={5820.2}></input></div>
                  </div>


                  <div style={{display: "flex",marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}>Pump Vertical</div>
                    <div style={{paddingLeft: "78px"}}>feet</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={4514.4}></input></div>
                  </div>

                </fieldset>
              </Grid>
            </Grid>
            <Grid container spacing={3}> 
              <Grid item sm={6} style={{paddingRight: "-10px"}}>
                <fieldset className={classes.FIELDSET}>
                  <legend className={classes.LEGEND}>Casing/Tubing</legend>
                  <div style={{display: "flex"}}>
                    <div style={{paddingLeft: "20px"}}>Pump ID</div>
                    <div style={{paddingLeft: "90px"}}>inch</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={1.5}></input></div>
                  </div>
                  <div style={{display: "flex", marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}><input type={'checkbox'}></input></div>
                    <div style={{paddingLeft: "20px"}}>Casing Valve Open?</div>
                  </div>
                  <div style={{display: "flex", marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}>Casing OD</div>
                    <div style={{paddingLeft: "76px"}}>inch</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={7}></input></div>
                  </div>
                  <div style={{display: "flex", marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}>Casing Weight</div>
                    <div style={{paddingLeft: "52px"}}>lb/ft</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={23}></input></div>
                  </div>
                  <div style={{display: "flex", marginTop: "10px"}}>
                    <div style={{paddingLeft: "20px"}}>Tubing OD</div>
                    <div style={{paddingLeft: "76px"}}>inch</div>
                    <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={2.875}></input></div>
                  </div>
                </fieldset>
              </Grid>
            </Grid>
            <Grid container spacing={3}> 
              <Grid item sm={12}></Grid>
              <Grid item sm={12}>
                <Wellinformation title={true} rod_config={this.props.rod_config} index={0}/>
              </Grid>
            <Grid container spacing={3}>
              <Grid item sm={12}></Grid>
              <Grid item sm={3}>
                <ColorButton variant="outlined" 
                  color="default"  style={{marginTop: "20px"}}>
                  Recal Pumpcard
                </ColorButton>
              </Grid>
              <Grid item sm={6}>
              </Grid>
              <Grid item sm={3}>
                <ColorButton variant="outlined" 
                  color="default"  style={{marginLeft: "0px", marginTop: "20px"}}>
                  Update new Parameter
                </ColorButton>
              </Grid>
            </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withStyles(styles)(BeamCalculate);