import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid } from '@material-ui/core';
import './Beam_Current.css'
import Alert from '@material-ui/lab/Alert';
import DynaChartResponsive_Visualisation from './Chart/DynaChartResponsive_Visualisation'
import moment from 'moment';

import API from './../../api/server'
import { store } from './../../store'

import './css/Beam_Visualisation.css'

import Button from '@material-ui/core/Button';

import Wellinformation from './Beam_WellInformation'

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
    paddingTop:'20px'
  },
  hieghtlightchart: {
    borderTop: "3px solid #ffffffff",
    // 0a88be
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'left'
  },
  title: {
    width: "100%",
    textAlign: "center",
    margin: "20px",
    padding: "8px",
    borderBottom: '1px solid #0000007a',
    fontSize: '20px'
  },
  title2: {
    width: "100%",
    textAlign: "center",
    margin: "0px",
    padding: "8px",
    fontSize: '20px'
  },
  spacingPadding: {
    paddingRight: "2%",
    paddingLeft: "2%",
  },
  pumpstatusoffline: {
    backgroundColor: '#6a6a6a',
    marginTop: '2px',
    borderRadius: '2px',
    width: '100px',
    height: '20px',
    float: "right",
    border: "solid 1px rgba(0,0,0,0.2)",
    paddingLeft: "15px",
    color: "#d9d9d9"
  },
  pumpstatusonline: {
    backgroundColor: '#1b9704',
    marginTop: '2px',
    borderRadius: '2px',
    width: '100px',
    height: '20px',
    float: "right",
    border: "solid 1px rgba(0,0,0,0.2)",
    paddingLeft: "10px",
  },
  statusoffline: {
    backgroundColor: '#6a6a6a',
    marginTop: '2px',
    borderRadius: '20px',
    width: '20px',
    height: '20px',
    float: "right",
    border: "solid 1px rgba(0,0,0,0.2)",
  },
  statusonline: {
    backgroundColor: '#1b9704',
    marginTop: '2px',
    borderRadius: '20px',
    width: '20px',
    height: '20px',
    float: "right",
    border: "solid 1px rgba(0,0,0,0.2)",
  },
  header: {
    fontSize: '16px',
    fontWeight: '600',
  },
  paramValue: {
    color: "#087fb2",
    //offline :color: 8c8b8b
    textAlign:'center',
    fontWeight: 600
  },
  paramValueheader: {
    textAlign:'center',
    fontWeight: 600
  },
  param:{
    textAlign:'center'
  },
  headerStatus: {
    float: "right"
  }
});

const ColorButton = withStyles((theme) => ({
  root: {
    width: "70%",
    color: theme.palette.getContrastText('#0a88be'),
    backgroundColor: '#0a88be',
    '&:hover': {
      backgroundColor: '#0a88be',
    },
  },
}))(Button);

class BeamVisualisation extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      offlineTime: 8,
      hookJSX: [],
      paramTable: [{
        label: "Max Load (lbs)",
        tag: "Loads_max"
      },{
        label: "Min Load (lbs)",
        tag: "Loads_min"
      },{
        label: "Max Positions (inch.)",
        tag: "Positions_max"
      },{
        label: "Min Positions (inch.)",
        tag: "Positions_min"
      },
      {
        label: "Temperature Transmitter (C°)",
        tag: "TT"
      },
      {
        label: "Pressure Transmitter (psi.)",
        tag: "PT"
      },
      {
        label: 'Speed (rpm.)',
        tag: 'speed'
      },{
        label: "View Prediction",
        tag: "view_device"
      }],
      JSXParam: Object,
      data: Object
    }
    this.interval = Object
    this.intervalTime = 5000
    this.handleClickView = this.handleClickView.bind(this)
  }
  handleClickView (ev){
    localStorage.setItem('device', ev.currentTarget.value);
    localStorage.setItem('device', ev.currentTarget.value);
    window.location.replace(store.getState().FullPath+'/Realtime');
  }
  async componentDidMount() {
    this.LoadDataLoadsAndPostions();
    this.intervalChart();
  }
  componentWillUnmount(){
    clearInterval(this.interval);
  }
  intervalChart() {
    this.interval = setInterval(async () => {
      this.LoadDataLoadsAndPostions()
    }, this.intervalTime);
  }
  async LoadDataLoadsAndPostions(){
    var self = this
    var res = await API().get('/sensors', {})
    var data = res.data
    var TempHookJSX

    self.setState({
      hookJSX: []
    })
    TempHookJSX = data.map((item ,index) => {
      return [<DynaChartResponsive_Visualisation
        store={store}
        key={index}
        color='#29abe2'
        fill='#E8F7FF'
        index={index}
        Loads={item.Loads}
        Positions={item.Positions}
        width='700'
        height='290'
        title={''}
        subTitle={((item.timestamp_g === null) ? undefined : item.timestamp_g)}
        hover={false}
        border={false}
        onclick={false}


        detailline={true}
        PumpCardPositions={item.PumpCardLoads}
        PumpCardLoads={item.PumpCardPositions}
        autoscale={false}

      />,
      item, ((!item.rtu_status) ? <Alert key={index} style={{marginTop: '10px',marginBottom: '10px'}} severity="error">Pump Controller Communication Error .</Alert> : "")]
    })
    self.setState({
      hookJSX: TempHookJSX
    })
  }
  render() {
    const { classes } = this.props;
    // const currentPath = this.props.currentPath;
    return (
      <div className={classes.root}>
        {
          this.state.hookJSX.map((item, index) => {
            return <Card key={index} style={((index > 0) ? {marginTop: '20px'} : {})} className={classes.hieghtlightchart}>
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item xs={10} className={classes.header}>
                    WELL - {((item[1] === null) ? "" : item[1].station_id)}
                  </Grid>
                  <Grid item xs={1}>
                    {
                      // <div hidden={item[1]["pump_status"]} className={classes['pumpstatus'+((item[1]["pump_status"]) ? "" : "offline")]}>Pump Stop</div>
                    }
                  </Grid>
                  <Grid item xs={1} className={classes.headerStatus}>
                    {
                      <div className={classes['status'+((moment(new Date()).diff(moment(item[1]['timestamp_g']),"minutes") > this.state.offlineTime) ? "offline" : "online")]}></div>
                    }
                  </Grid>
                </Grid>
                <Grid container spacing={3}>
                  <Grid item md={5} sm={12}>
                    {
                      item[0]
                    }
                  </Grid>
                  <Grid item md={7} sm={12}>
                    {/* <Alert severity="error">RTU Communication Error.</Alert><br></br> */}
                    {
                      item[2]
                    }
                    <table id="paramValue">
                      <thead></thead>
                      <tbody>
                        <tr>
                          <th>Parameter</th>
                          <th>Value</th>
                        </tr>
                      {
                        this.state.paramTable.map((itemT, index) => {
                          return <tr key={index}>
                            <td className={classes.param}>{itemT.label}</td>
                            <td className={classes.paramValue}>
                              {(
                                (item[1] === null) 
                                ? "" : 
                                  ((itemT.tag === 'timestamp_g') 
                                    ? ((item[1][itemT.tag] === null) ? "" :
                                      moment(item[1][itemT.tag]).add(0, 'hours').format("DD-MMMM-YYYY HH:mm:ss")) : 
                                        ((itemT.tag === 'view_device') ? 
                                          <ColorButton 
                                            variant="outlined" 
                                            className={classes.btnmargin}
                                            value={item[1].station_id}
                                            onClick={this.handleClickView}
                                            // href='#/Realtime'
                                          >
                                            View
                                          </ColorButton>
                                        : item[1][itemT.tag])
                                  )
                              )}
                            </td>
                          </tr>
                        })
                      }
                      </tbody>
                      <tfoot></tfoot>
                    </table>
                    <br></br>
                    {
                      ((item[1].rod_config !== undefined) ? (((item[1].rod_config.length !== 0)) ? <Wellinformation rod_config={((item[1].rod_config.length !== 0) ? item[1].rod_config: [])} index={0} style={{color: "#ff00ff"}}/>: <div></div>): "")
                    }
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          })
        }
      </div>
    )
  }
}

export default withStyles(styles)(BeamVisualisation);