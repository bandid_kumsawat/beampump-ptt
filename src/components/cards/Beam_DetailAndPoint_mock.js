import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import { Grid } from '@material-ui/core';
import DynaChartResponsive_History from './Chart/DynaChartResponsive_History'
import PointChart from './Chart/PointChart'
import moment from 'moment'
import "./Beam_DetailAndPoint.css"

import Table_mock from './table_mock'

import { store } from './../../store'

// import { addwellselect } from './../../actions';

import API from './../../api/server'


const ColorButton_mock = withStyles((theme) => ({
  root: {
    width: "100%",
    color: theme.palette.getContrastText('#0a88be'),
    backgroundColor: '#0a88be',
    '&:hover': {
      backgroundColor: '#0a88be',
    },
  },
}))(Button);

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  hieghtlightchart: {
    borderTop: "3px solid #ffffffff"
  },
  station_id: {
    fontSize: "18px",
    fontWeight: "500",
    marginLeft: "20px",
    paddingBotton: "20px"
  },
  WellInformation: {
    fontSize: "20px",
    fontWeight: "500",
    marginLeft: "20px",
    paddingBotton: "20px"
  },
  WellClusterChart: {
    padding: "0px 30% 12px 30%",
    textAlign: "center",
    fontSize: "20px",
    fontWeight: "500",
  },
  TableInformation: {
    fontSize: "16px",
    width: "100%",
    paddingRight: '20%',
    paddingLeft: '20%'
  },
  TableRow:{
    // marginBottom: "6px !important",
    padding: "5px 0px 1px 10px"
  },
  ChartPointInformation: {
    padding: "0 30% 0 10%"
  },
  ChartInformation: {
    padding: "2% 00% 0 00%"
  },
  TableDBcolor: {
    backgroundColor: "#B2ACAF",
    borderColor: '#ff00ff'
  },
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
});

class BeamViewSelect extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      localS: null,
      pointdata: []
    }
  }
  async componentDidMount() {
    const res = await API().post('/PointChart', {
      "station_id": ((localStorage.getItem('device') !== null) ? localStorage.getItem('device') : ""),
    })
    this.setState({pointdata: res.data})
    this.setState({localS: JSON.parse(localStorage.getItem('select_Dynachart'))})
    // console.log(this.state.pointdata)
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <Grid container spacing={3}>
            <Grid item sm={6}>
              <Grid item sm={12}>

                <Grid container spacing={3}>
                  <Grid item sm={12}>
                    <div className={classes.WellInformation}><center>Well Information</center></div>
                  </Grid>
                </Grid>


                <Grid container spacing={3}>
                  <Grid item sm={12} className={classes.TableInformation}>
                    
                    <table id="table-show-detail-well">
                      <tbody>
                        <tr>
                          <td style={{textAlign: "left"}}>Well Name</td>
                            <td className='param-show-well'>
                            {
                              ((this.store.getState().selectWell[0] === undefined) ? "" : this.store.getState().selectWell[0].station_id)
                              // ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).station_id : "null")
                            }
                          </td>
                        </tr>
                        <tr>
                          <td style={{textAlign: "left"}}>Card Report Date</td>
                          <td className='param-show-well'>
                            {
                              ((this.store.getState().selectWell[0] === undefined) ? "" : moment(this.store.getState().selectWell[0].DateTime).format("DD-MMMM-YYYY HH:mm:ss"))
                              // ((this.state.localS !== null) ? moment(JSON.parse(localStorage.getItem('select_Dynachart')).DateTime).format("DD-MMMM-YYYY HH:mm:ss") : "null")
                            }
                          </td>
                        </tr>
                        {/* <tr>
                          <td style={{textAlign: "left"}}>Card Report Type</td>
                          <td className='param-show-well'>
                            {
                              ((this.store.getState().selectWell[0] === undefined) ? "" : this.store.getState().selectWell[0].CardType)
                              // ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).CardType : "null")
                            }
                          </td>
                        </tr> */}
                      </tbody>
                    </table>
  {/*         
                  <table className={classes.TableInformation}>
                    <tbody>
                      <tr className={classes.TableDBcolor}>
                        <td style={{textAlign: "left"}}>
                          <div className={classes.TableRow}>
                            Well Name
                          </div>
                        </td>
                        <td style={{textAlign: "right"}}>
                          <div className={classes.TableRow}>
                            {
                              ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).station_id : "")
                            }
                          </div>
                        </td>
                      </tr>
                      <tr className={classes.TableDBcolor}>
                        <td style={{textAlign: "left"}} bordercolor='red'>
                          <div className={classes.TableRow}>
                            Card Report Date
                          </div>
                        </td>
                        <td style={{textAlign: "right"}}>
                          <div className={classes.TableRow}>
                            {
                              ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).DateTime : "")
                            }
                          </div>
                        </td>
                      </tr>
                      <tr className={classes.TableDBcolor}>
                        <td style={{textAlign: "left"}}>
                          <div className={classes.TableRow}>
                            Card Report Type
                          </div>
                        </td>
                        <td style={{textAlign: "right"}}>
                          <div className={classes.TableRow}>
                            {
                              ((this.state.localS !== null) ? JSON.parse(localStorage.getItem('select_Dynachart')).CardType : "")
                            }
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table> */}
    
                  </Grid>
                </Grid>
                
              </Grid>
                <hr></hr>
                <div className={classes.ChartInformation}>
                  {
                    ((this.store.getState().selectWell[0] === undefined) ? "" : this.store.getState().selectWell.map((item ,index) => {
                      return <DynaChartResponsive_History 
                        key={index}
                        className=""
                        color='#29abe2'
                        fill='#E8F7FF'
                        store={store}
                        Loads={item.Loads} 
                        Positions={item.Positions} 
                        width='800'
                        height='470'
                        // title={item.station_id} 
                        subTitle={item.DateTime} 
                        hover={false}
                        border={false}
                        onclick={false}

                        detailline={true}
                        PumpCardLoads={item.PumpCardLoads}
                        PumpCardPositions={item.PumpCardPositions}
                        autoscale={false}

                        // detailline={false}
                        // autoscale={true}
                      />
                    }))
                  }
                </div>
              </Grid>
              <Grid item sm={6} style={{paddingRight: "-10px"}}>
                <Grid container spacing={3}> 
                  <Grid item sm={6} style={{paddingRight: "-10px"}}>
                    <fieldset className={classes.FIELDSET}>
                      <legend className={classes.LEGEND}>Depths</legend>
                      <div style={{display: "flex"}}>
                        <div style={{paddingLeft: "20px"}}>Top Preforation</div>
                        <div style={{paddingLeft: "68px"}}>feet</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={666.3}></input></div>
                      </div>

                      <div style={{display: "flex",marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}>Bottom Preforation</div>
                        <div style={{paddingLeft: "44px"}}>feet</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={6496.1}></input></div>
                      </div>


                      <div style={{display: "flex",marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}>Anchor Depth</div>
                        <div style={{paddingLeft: "77px"}}>feet</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={6013.8}></input></div>
                      </div>


                      <div style={{display: "flex",marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}>Plug-Back TD</div>
                        <div style={{paddingLeft: "80px"}}>feet</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={9478.3}></input></div>
                      </div>

                      <div style={{display: "flex",marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}>Pump Measured</div>
                        <div style={{paddingLeft: "64px"}}>feet</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={5820.2}></input></div>
                      </div>


                      <div style={{display: "flex",marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}>Pump Vertical</div>
                        <div style={{paddingLeft: "78px"}}>feet</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={4514.4}></input></div>
                      </div>

                    </fieldset>
                  </Grid>
                  <Grid item sm={6} style={{paddingRight: "-10px"}}>
                    <fieldset className={classes.FIELDSET}>
                      <legend className={classes.LEGEND}>Casing/Tubing</legend>
                      <div style={{display: "flex"}}>
                        <div style={{paddingLeft: "20px"}}>Pump ID</div>
                        <div style={{paddingLeft: "90px"}}>inch</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={1.5}></input></div>
                      </div>
                      <div style={{display: "flex", marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}><input type={'checkbox'}></input></div>
                        <div style={{paddingLeft: "20px"}}>Casing Valve Open?</div>
                      </div>
                      <div style={{display: "flex", marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}>Casing OD</div>
                        <div style={{paddingLeft: "76px"}}>inch</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={7}></input></div>
                      </div>
                      <div style={{display: "flex", marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}>Casing Weight</div>
                        <div style={{paddingLeft: "52px"}}>lb/ft</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={23}></input></div>
                      </div>
                      <div style={{display: "flex", marginTop: "10px"}}>
                        <div style={{paddingLeft: "20px"}}>Tubing OD</div>
                        <div style={{paddingLeft: "76px"}}>inch</div>
                        <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={2.875}></input></div>
                      </div>
                    </fieldset>
                  </Grid>
                </Grid>
                <Grid container spacing={12}> 
                  <Grid container spacing={12}> 
                  <Grid item sm={12}></Grid>
                  <Grid item sm={12}>
                    <Table_mock />
                  </Grid>
                </Grid>
                <Grid container spacing={12}>
                  <Grid item sm={12}></Grid>
                  <Grid item sm={3}>
                    <ColorButton_mock variant="outlined" 
                      color="default"  style={{marginTop: "20px"}}>
                      Recal Pumpcard
                    </ColorButton_mock>
                  </Grid>
                  <Grid item sm={6}>
                  </Grid>
                  <Grid item sm={3}>
                    <ColorButton_mock variant="outlined" 
                      color="default"  style={{marginLeft: "0px", marginTop: "20px"}}>
                      Update new Parameter
                    </ColorButton_mock>
                  </Grid>
                </Grid>
                </Grid>
              </Grid>
              
              {/* <Grid container spacing={3}> 
                <Grid item sm={6}></Grid>
                <Grid item sm={6}>
                  <Table_mock />
                </Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid item sm={6}></Grid>
                <Grid item sm={2}>
                  <ColorButton_mock variant="outlined" 
                    color="default"  style={{marginTop: "20px"}}>
                    Recal Pumpcard
                  </ColorButton_mock>
                </Grid>
                <Grid item sm={2}>
                </Grid>
                <Grid item sm={2}>
                  <ColorButton_mock variant="outlined" 
                    color="default"  style={{marginLeft: "0px", marginTop: "20px"}}>
                    Update new Parameter
                  </ColorButton_mock>
                </Grid>
              </Grid> */}
            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamViewSelect);