import React, { Component } from 'react';
import moment from 'moment'
import _ from "underscore"
import withStyles from '@material-ui/styles/withStyles';
// import Card from '@material-ui/core/Card';
// import CardContent from '@material-ui/core/CardContent';
import { red } from '@material-ui/core/colors';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormHelperText from '@material-ui/core/FormHelperText';
import TextField from '@material-ui/core/TextField';
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';
import API from './../../api/server'
import { addresultwelldata, setpointchart, settempwellid, /*addwellselect*/} from './../../actions';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  root: {
    maxWidth: '100%',
    flexGrow: 1,
    // paddingLeft: '30%',
    // paddingRight: '30%',
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
    // paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'right'
  },
  containerCenter:{

  }
});

class BeamFrom extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      // selectDate: moment(new Date()).format("YYYY") + "-" + moment(new Date()).format("MM") + "-" + moment(new Date()).format("DD"),
      selectDate: ((localStorage.getItem('date') === null) ? "2020-02-17" : localStorage.getItem('date')),
      selectDevice: "",
      selectOffset: 0,
      listDeivce: [],
      listType: [],
      selectType: ''
    }
    this.handleDevice = this.handleDevice.bind(this);
    this.handleDatePicker = this.handleDatePicker.bind(this);
    this.handleOffset = this.handleOffset.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.handleType = this.handleType.bind(this);
  }
  async componentDidMount() {
    if (localStorage.getItem('device') === null 
      || localStorage.getItem('date') === null 
      || localStorage.getItem('offset') === null 
      || localStorage.getItem('select_Dynachart') === null
    ) {
      localStorage.setItem('device', 'LKU-R16T');
      localStorage.setItem('date', "2020-02-26");
      localStorage.setItem('offset', "10");
      localStorage.setItem('type', "FC")
      localStorage.setItem('select_Dynachart', '{"CardDate":"01-23-2020","CardDateTime":"Thu, 23 Jan 2020 00:04:00 GMT","CardTime":" 00:04:00","CardType":"CC","Loads":["-5792.0","-5440.0","-5376.0","-5472.0","-4832.0","-4416.0","-4128.0","-2976.0","-2080.0","-1664.0","-1440.0","-320.0","56.0","-512.0","-1736.0","-2624.0","-3296.0","-3680.0","-4416.0","-4896.0","-4480.0","-3616.0","-2784.0","-2304.0","-1888.0","-1632.0","-1280.0","-1760.0","-2592.0","-3424.0","-3904.0","-4480.0","-4800.0","-5056.0","-4736.0","-4192.0","-3488.0","-3136.0","-2656.0","-2432.0","-2368.0","-2720.0","-3392.0","-4096.0","-4512.0","-4928.0","-5312.0","-5408.0","-4896.0","-4160.0","-3584.0","-3712.0","-3968.0","-4544.0","-5248.0","-5600.0","-5664.0","-5792.0","-6080.0","-6720.0","-7168.0","-7456.0","-7872.0","-8000.0","-7544.0","-6944.0","-6272.0","-5856.0","-5504.0","-5472.0","-5536.0","-5920.0","-6688.0","-7168.0","-7648.0","-7896.0","-7808.0","-7608.0","-7264.0","-6816.0","-6400.0","-6400.0","-6624.0","-7328.0","-8440.0","-9624.0","-11128.0","-11736.0","-11544.0","-10968.0","-10168.0","-9208.0","-8312.0","-7064.0","-6592.0","-6560.0","-6976.0","-7608.0","-8152.0","-8664.0","-9464.0","-9528.0","-9496.0","-8960.0","-8248.0","-7680.0","-7232.0","-6272.0"],"PathImage":"LKU-R16T/LKU-R16T 01-23-2020  00-04-00.png","Positions":["11.0","19.0","38.0","55.0","72.0","94.0","118.0","137.0","168.0","198.0","234.0","267.0","303.0","339.0","372.0","413.0","465.0","509.0","550.0","592.0","639.0","669.0","710.0","757.0","799.0","845.0","903.0","947.0","991.0","1038.0","1082.0","1127.0","1170.0","1209.0","1250.0","1292.0","1341.0","1380.0","1413.0","1443.0","1473.0","1501.0","1525.0","1551.0","1572.0","1592.0","1609.0","1622.0","1633.0","1638.0","1644.0","1647.0","1650.0","1647.0","1644.0","1636.0","1631.0","1622.0","1609.0","1592.0","1575.0","1556.0","1534.0","1520.0","1496.0","1468.0","1438.0","1410.0","1380.0","1347.0","1305.0","1264.0","1228.0","1195.0","1156.0","1116.0","1074.0","1030.0","986.0","942.0","898.0","840.0","807.0","749.0","702.0","661.0","622.0","581.0","537.0","490.0","432.0","388.0","341.0","303.0","259.0","220.0","185.0","152.0","126.0","101.0","79.0","57.0","41.0","25.0","8.0","3.0","0.0","5.0"],"WellName":"LKU-R16T","id":"5f332a4841f5364e1144480c","x":0.389937371546909,"y":0.07588421977325277}');
    }

    const res = await API().get('/sensors/list')
    _.sortBy(res.data.device);
    res.data.device.sort()
    this.setState({listDeivce: [...new Set(res.data.device)]});
    _.sortBy(res.data.type);
    res.data.type.sort()
    this.setState({listType: [...new Set(res.data.type)]});
    if (localStorage.getItem('device') === null){
      this.setState({selectDevice: ""})
    } else {
      this.setState({selectDevice: localStorage.getItem('device')})
    }
    if (localStorage.getItem('date') === null){
      this.setState({selectDate: "2020-02-17"})
    }else{
      this.setState({selectDate: localStorage.getItem('date')})
    }
    if (localStorage.getItem('offset') === null){
      this.setState({selectOffset: 0})
    }else{
      this.setState({selectOffset: localStorage.getItem('offset')})
    }
    if (localStorage.getItem('type') === null){
      this.setState({selectType: "FC"})
    }else{
      this.setState({selectType: localStorage.getItem('type')})
    }
    var data = {
      "WellName": this.state.selectDevice,
      "year": Number(moment(this.state.selectDate).format("YYYY")),
      "month": Number(moment(this.state.selectDate).format("M")),
      "day": Number(moment(this.state.selectDate).format("D")),
      "offset": Number(this.state.selectOffset),
      "type": this.state.selectType
    }
    const resD = await API().post('/dynadata', data)
    if (resD.data.row){
      this.store.dispatch(addresultwelldata(resD.data));
      this.addID(resD.data.data)
      
    }

    // call point chart
    const resP = await API().post('/PointChart', {
      "WellName": ((localStorage.getItem('device') !== null) ? localStorage.getItem('device') : ""),
      "type": this.state.selectType
    })
    
    this.store.dispatch(setpointchart(resP.data))
    // this.store.dispatch(addwellselect([resP.data[0].data]))
    try{
      document.getElementById(resP.data.data[0].id).style.borderTop = "4px solid #0a88beaf";
      document.getElementById(resP.data.data[0].id).style.borderRight = "4px solid #0a88beaf";
      document.getElementById(resP.data.data[0].id).style.borderBottom = "10px solid #0a88beaf";
      document.getElementById(resP.data.data[0].id).style.borderLeft = "4px solid #0a88beaf";
    } catch (e){
      console.log(e)
    }
  }

  handleType(ev){
    this.setState({selectType: ev.target.value});
  }

  handleDevice (ev){
    this.setState({selectDevice: ev.target.value});
  }
  
  handleDatePicker (ev) {
    this.setState({selectDate: ev.target.value});
  }

  handleOffset (ev) {
    this.setState({selectOffset: ev.target.value});
  }

  async handleConfirm (ev) {
    localStorage.setItem('device', this.state.selectDevice);
    localStorage.setItem('date', this.state.selectDate);
    localStorage.setItem('offset', this.state.selectOffset);
    this.store.dispatch(addresultwelldata([]));
    var data = {
      "WellName": this.state.selectDevice,
      "year": Number(moment(this.state.selectDate).format("YYYY")),
      "month": Number(moment(this.state.selectDate).format("M")),
      "day": Number(moment(this.state.selectDate).format("D")),
      "offset": Number(this.state.selectOffset),
      "type": this.state.selectType
    }
    const res = await API().post('/dynadata', data)
    console.log(res.data)
    if (res.data.row){
      this.store.dispatch(addresultwelldata(res.data));
    }
    this.addID(res.data.data)
    localStorage.setItem('device', this.state.selectDevice);
    localStorage.setItem('date', this.state.selectDate);
    localStorage.setItem('offset', this.state.selectOffset);
    localStorage.setItem('type', this.state.selectType);
    if (res.data.row === 0){
      window.location.reload();
    }
    // call point chart
    this.store.dispatch(setpointchart(undefined))
    const reshandle = await API().post('/PointChart', {
      "WellName": ((localStorage.getItem('device') !== null) ? localStorage.getItem('device') : ""),
      "type": this.state.selectType
    })
    const callFn  = () => {
      this.store.dispatch(setpointchart(reshandle.data.data))
    }
    setTimeout(() => { callFn() }, 1);
    
    // window.location.reload();
  }

  addID(data) {
    var temp = []
    data.forEach((item , index) => {
      temp.push(item.id)
    });
    this.store.dispatch(settempwellid(temp))
  }

  render() {
    const { classes } = this.props;
    
    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item sm={2}>
            <FormControl className={classes.formControl}>
              <InputLabel shrink htmlFor="select-native-label-placeholder">
                เลือกอุปกรณ์
              </InputLabel>
              <NativeSelect
                value={this.state.selectDevice}
                onChange={this.handleDevice }
                inputProps={{
                  name: 'Select Device',
                  id: 'select-native-label-placeholder',
                }}
              >
                <option aria-label="None" value="" />
                {
                  this.state.listDeivce.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </NativeSelect>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Grid>
          {/* <Grid item sm={2}>
            <FormControl className={classes.formControl}>
              <InputLabel shrink htmlFor="select-native-label-placeholder-type">
                เลือกชนิดอุปกรณ์
              </InputLabel>
              <NativeSelect
                value={this.state.selectType}
                onChange={this.handleType }
                inputProps={{
                  name: 'Select Type',
                  id: 'select-native-label-placeholder-type',
                }}
              >
                <option aria-label="None" value="" />
                {
                  this.state.listType.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </NativeSelect>
              <FormHelperText></FormHelperText>
            </FormControl>
          </Grid> */}
          <Grid item sm={2}>
            <FormControl className={classes.formControl}>
              <TextField
                id="date"
                label="เลือกวันที่"
                type="date"
                defaultValue={this.state.selectDate}
                className={classes.textField}
                onChange={this.handleDatePicker}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </FormControl>
          </Grid>
          <Grid item sm={2}>        
            <FormControl className={classes.formControl}>
              <TextField
                id="standard-number"
                label="กำหนดช่วงเวลา (+,-)"
                type="number"
                value={this.state.selectOffset}
                onChange={this.handleOffset }
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </FormControl>
          </Grid>
          <Grid item sm={2}></Grid>
          <Grid item sm={2}>    
            <div className={classes.buttonFloat}>  
              <Button 
                className={classes.buttonMargin}
                variant="outlined" 
                color="default" 
                disabled={( this.state.selectDevice === "" || this.state.selectDate === "" || this.state.selectOffset === 0 || this.state.selectType === '')}
                onClick={this.handleConfirm}
              >
                เลือกดูข้อมูล
              </Button>
            </div>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withStyles(styles)(BeamFrom);
