import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, calories, fat, carbs, protein, col6, col7, col8) {
  return { name, calories, fat, carbs, protein , col6, col7, col8};
}

const rows = [
  createData(1, 'W-T66XD - 0.875', 56, 25.07, 0.7, 0.0219713, 0.0497688, 'Y'),
  createData(2, 'W-T66XD - 0.750', 171, 25.03, 0.7, 0.019518, 0.0393795, 'Y'),
  createData(3, 'SB (K) - 1.500', 56, 25.10, 0.7, 0.0489874, 0.1230028, 'N'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Toper Number</TableCell>
            <TableCell align="right">Rob Description Grade / Size</TableCell>
            <TableCell align="right">Number of Rods</TableCell>
            <TableCell align="right">Rob Length</TableCell>
            <TableCell align="right">Service Factor</TableCell>
            <TableCell align="right">Up Storke Damping</TableCell>
            <TableCell align="right">Down Strock Dampping</TableCell>
            <TableCell align="right">Guides / Used ? [Y/N] (#)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow style={{backgroundColor: "#cce2ec44"}}>
            <TableCell component="th" scope="row"></TableCell>
            <TableCell align="right"></TableCell>
            <TableCell align="right">#</TableCell>
            <TableCell align="right">feet</TableCell>
            <TableCell align="right"></TableCell>
            <TableCell align="right"></TableCell>
            <TableCell align="right"></TableCell>
            <TableCell align="right"></TableCell>
          </TableRow>
          {rows.map((row) => (
            <TableRow key={row.name} style={{backgroundColor: "#0a88be44"}}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.calories}</TableCell>
              <TableCell align="right">{row.fat}</TableCell>
              <TableCell align="right">{row.carbs}</TableCell>
              <TableCell align="right">{row.protein}</TableCell>
              <TableCell align="right">{row.col6}</TableCell>
              <TableCell align="right">{row.col7}</TableCell>
              <TableCell align="right">{row.col8}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
