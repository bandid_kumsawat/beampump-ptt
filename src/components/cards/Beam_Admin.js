import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid } from '@material-ui/core';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import API from './../../api/server'

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

import './Beam_Current.css'
import './css/Beam_MAdmin.css'

const ConfirmBtn = withStyles((theme) => ({
  root: {
    width: "100%",
    color: '#ffffff',
    backgroundColor: '#0a88be',
    '&:hover': {
      backgroundColor: '#0a88be',
    },
  },
}))(Button);

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
    paddingTop:'20px'
  },
  hieghtlightchart: {
    borderTop: "3px solid #ffffffff"
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'left'
  },
  containerCenter:{

  },
  title: {
    width: "100%",
    textAlign: "center",
    margin: "20px",
    padding: "8px",
    borderBottom: '1px solid #0000007a',
    fontSize: '20px'
  },
  title2: {
    width: "100%",
    textAlign: "center",
    margin: "0px",
    padding: "8px",
    fontSize: '20px'
  },
  spacingPadding: {
    paddingRight: "2%",
    paddingLeft: "2%",
  },
  btnmuser:{
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "40%",
    backgroundColor: '#0a88beaa',
    border: "0px"
  },
  btnmuserdelete: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "40%",
    backgroundColor: '#ff0000aa',
    border: "0px"
  }
});

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class BeamAdmin extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      username: '',
      password: '',
      email: '',
      type: 'user',
      users: [],

      username_edit: '',
      password_edit: '',
      email_edit: '',
      _id_edit: '',

      open: false
    }
    this.handleClickOpenDialog = this.handleClickOpenDialog.bind(this)
    this.handleBtnConfirm = this.handleBtnConfirm.bind(this)
    this.handleDeletUser = this.handleDeletUser.bind(this)

    this.handleUsername = this.handleUsername.bind(this)
    this.handleEmail = this.handleEmail.bind(this)
    this.handlePassword = this.handlePassword.bind(this)
    this.handleEditEmail = this.handleEditEmail.bind(this)
    this.handleEditPassword = this.handleEditPassword.bind(this)
    this.handleEditUsername = this.handleEditUsername.bind(this)
  }
  async componentDidMount() {
    const res = await API().post('/users/find')
    this.setState({
      users: res.data
    })
  }
  handleClickOpenDialog(e) {
    var data = JSON.parse(e.target.value)
    this.setState({
      _id_edit: data._id,
      username_edit: data.username,
      password_edit: '',
      email_edit: data.email
    })
    this.setState({
      open: true
    })
  }
  handleClose = () => {
    this.setState({
      open: false
    })
  };
  async updateTableUser() {
    const res = await API().post('/users/find')
    this.setState({
      users: res.data
    })
  }
  handleAddUser = async () => {
    const res = await API().post('/users', {
      "user": {
          "email": this.state.email,
          "password": this.state.password,
          "username": this.state.username,
          "type": "user"
      }
    })
    if (res.status === 200){
      this.updateTableUser()
      alert('ผู้ใช้ถูกเพิ่มแล้ว')
    }else {
      alert('ไม่สามารถเพิ่มผู้ใช้ได้')
    }
  }
  handleBtnConfirm = async () => {
    const res = await API().put('/user', {
      "primary_key": this.state._id_edit,
        "user": {
          "type": "user",
          "username": this.state.username_edit,
          "password": this.state.password_edit,
          "email": this.state.email_edit
        }
    })
    if (res.status === 200){
      this.updateTableUser()
      this.setState({
        open: false
      })
    }else{
      alert("แก้ไขผู้ใช้ไม่สำเร็จ")
    }
  };
  handleEmail(e){
    this.setState({
      email: e.target.value
    })
  }
  handleUsername(e){
    this.setState({
      username: e.target.value
    })
  }
  handlePassword(e){
    this.setState({
      password: e.target.value
    })
  }
  handleEditEmail(e){
    this.setState({
      email_edit: e.target.value
    })
  }
  handleEditPassword(e) {
    this.setState({
      password_edit: e.target.value
    })
  }
  handleEditUsername(e) {
    this.setState({
      username_edit: e.target.value
    })
  }
  async handleDeletUser(e){
    var data = JSON.parse(e.target.value)
    const res = await API().delete('/user?primary_key=' + data._id, {
      "primary_key": data._id
    }, {
      "Content-Type": 'application/json',
      "X-Requested-With": 'XMLHttpRequest',
      "Authorization": 'Token ' + this.store.getState().AccessToken,
    })
    if (res.status === 200){
      this.updateTableUser()
    }
  }
  render() {

    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <Grid container spacing={3}>
              <Grid item sm={4}>
                จัดการผู้ใช้งาน
              </Grid>
            </Grid>
            <Grid container spacing={3}>
              <Grid item sm={3}></Grid>
              <Grid item sm={3}>
                <FormControl fullWidth className={classes.margin}>
                    <InputLabel htmlFor="standard-adornment-amount1">Email</InputLabel>
                    <Input
                      value={this.state.email}
                      onChange={this.handleEmail}
                      id="standard-adornment-amount1"
                    />
                  </FormControl>
              </Grid>
              <Grid item sm={3}>
                <FormControl fullWidth className={classes.margin}>
                  <InputLabel htmlFor="standard-adornment-amount2">Username</InputLabel>
                  <Input
                    value={this.state.username}
                    onChange={this.handleUsername}
                    id="standard-adornment-amount2"
                  />
                </FormControl>
              </Grid>
              <Grid item sm={3}></Grid>
            </Grid>

            <Grid container spacing={3}>
              <Grid item sm={3}></Grid>
              <Grid item sm={3}>
                <FormControl fullWidth className={classes.margin}>
                  <InputLabel htmlFor="standard-adornment-amount3">Password</InputLabel>
                  <Input
                    value={this.state.password}
                    onChange={this.handlePassword}
                    type="password"
                    id="standard-adornment-amount3"
                  />
                </FormControl>
              </Grid>
              <Grid item sm={3}>
                <ConfirmBtn 
                  className={classes.buttonMargin}
                  variant="outlined" 
                  color="default" 
                  disabled={( this.state.username === "" || this.state.password === "" || this.state.email === "")}
                  onClick={this.handleAddUser}
                >
                  เพิ่มผู้ใช้
                </ConfirmBtn>
              </Grid>
              <Grid item sm={3}></Grid>
            </Grid>
            <hr></hr>

            <Grid container spacing={3}>
              <Grid item sm={3}></Grid>
              <Grid item sm={6}>
                <table id="show-device">
                  <thead></thead>
                  <tbody>
                    <tr>
                      <th>Email</th>
                      <th>Username</th>
                      <th colSpan='2'>จัดการผู้ใช้งาน</th>
                    </tr>
                    {
                      this.state.users.map((item, index) => {
                        return <tr key={index}>
                            <td>{item.email}</td>
                            <td>{item.username}</td>
                            <td>
                              <button 
                                className={classes.btnmuser}
                                type="button"
                                value={JSON.stringify(item)}
                                onClick={this.handleClickOpenDialog}
                              >
                                จัดการ
                              </button>
                            </td>
                            <td>
                              <button 
                                className={classes.btnmuserdelete}
                                type="button"
                                value={JSON.stringify(item)}
                                onClick={this.handleDeletUser}
                              >
                                ลบ
                              </button>
                            </td>
                          </tr>
                      })
                    }
                  </tbody>
                  <tfoot></tfoot>
                </table>
              </Grid>
            </Grid>

            <Dialog
              open={this.state.open}
              TransitionComponent={Transition}
              keepMounted
              onClose={this.handleClose}
              aria-labelledby="alert-dialog-slide-title"
              aria-describedby="alert-dialog-slide-description"
            >
              <DialogTitle id="alert-dialog-slide-title">{"แก้ไขข้อมูล "}</DialogTitle>
              <DialogContent>
                <FormControl fullWidth className={classes.margin}>
                  <InputLabel htmlFor="standard-adornment-amount14">Email</InputLabel>
                  <Input
                    value={this.state.email_edit}
                    onChange={this.handleEditEmail}
                    id="standard-adornment-amount14"
                  />
                </FormControl>
                <FormControl fullWidth className={classes.margin}>
                  <InputLabel htmlFor="standard-adornment-amount25">Username</InputLabel>
                  <Input
                    value={this.state.username_edit}
                    onChange={this.handleEditUsername}
                    id="standard-adornment-amount25"
                  />
                </FormControl>
                <FormControl fullWidth className={classes.margin}>
                  <InputLabel htmlFor="standard-adornment-amount36">Password</InputLabel>
                  <Input
                    value={this.state.password_edit}
                    type="password"
                    onChange={this.handleEditPassword}
                    id="standard-adornment-amount36"
                  />
                </FormControl>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleBtnConfirm} color="primary">
                  Confirm
                </Button>
                <Button onClick={this.handleClose} color="primary">
                  Close
                </Button>
              </DialogActions>
            </Dialog>

          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamAdmin);