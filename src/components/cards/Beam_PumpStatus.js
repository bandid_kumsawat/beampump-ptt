import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import InputLabel from '@material-ui/core/InputLabel';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button'
import { Grid } from '@material-ui/core';

import BeamPumpStatusTable from './table/PumpStatusTable'

import API from './../../api/server'
import _ from 'underscore'

import './Beam_Current.css'
import './css/Beam_Visualisation.css'

import { store } from './../../store'

import moment from 'moment';

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  hieghtlightchart: {
    borderTop: "3px solid #ffffffff",
  },
  header: {
    fontSize: '16px',
    fontWeight: '600',
  },
  formControl: {
    minWidth: "100%",
    margin: "20px 0px 20px 30px"
  },
  buttonMargin: {
    margin: "30px 0px 20px 30px"
  }
});

class BeamPumpStauts extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      listDeivce: [],
      selectDevice: "",
      PumpStatus: []
    }
    this.handleSelectDevice = this.handleSelectDevice.bind(this)
    this.handleClickSelectDevice = this.handleClickSelectDevice.bind(this)
  }
  async handleClickSelectDevice() {
    if (this.state.selectDevice !== ""){
      console.log(this.state.selectDevice)
      const res = await API().post('/statuspump/get', {
        "station_id": this.state.selectDevice
      })
      if (res.data.msg === undefined){
        res.data.map((item, index) => {
          item.index = index + 1
          item.period_ms = (item.period_ms / 1000 / 60).toFixed(2)
          item.type = ((item.type === 'START') ? <span style = {{width: "50px",textAlign: "center",display: "inline-block", color: "#ffffff", backgroundColor: "#1371b8", padding: "2px", borderRadius: "1px"}}>START</span> 
          
          : <span style = {{width: "50px",textAlign: "center",display: "inline-block", color: "#ffffff", backgroundColor: "#c43d3dde", padding: "2px", borderRadius: "1px"}}>STOP</span>)

          item.start_time = ((item.start_time === null) ? <span style={{color: "#ff0000", textDecoration: "underline"}}>ไม่พบเวลาเริ่ม</span>: moment(item.start_time).format('DD-MMMM-YYYY HH:mm:ss'))
          item.stop_time = ((item.stop_time === null) ? <span style={{color: "#ff0000", textDecoration: "underline"}}>ไม่พบเวลาหยุด</span>: moment(item.stop_time).format('DD-MMMM-YYYY HH:mm:ss'))
          return item
        })
      }
      console.log(res.data)
      if (res.data.msg === undefined){
        this.setState({PumpStatus: res.data})
      }else {
        this.setState({PumpStatus: []})
      }
    }
  }
  async componentDidMount() {
    var device = localStorage.getItem('device')
    if (device !== ''){
      this.setState({selectDevice: device})
      const res = await API().post('/statuspump/get', {
        "station_id": device
      })
      if (res.data.msg === undefined){
        res.data.map((item, index) => {
          item.index = index + 1
          item.period_ms = (item.period_ms / 1000 / 60).toFixed(2)
          item.type = ((item.type === 'START') ? <span style = {{width: "50px",textAlign: "center",display: "inline-block", color: "#ffffff", backgroundColor: "#1371b8", padding: "2px", borderRadius: "1px"}}>START</span> 
          
          : <span style = {{width: "50px",textAlign: "center",display: "inline-block", color: "#ffffff", backgroundColor: "#c43d3dde", padding: "2px", borderRadius: "1px"}}>STOP</span>)

          item.start_time = ((item.start_time === null) ? <span style={{color: "#ff0000", textDecoration: "underline"}}>ไม่พบเวลาเริ่ม</span>: moment(item.start_time).format('DD-MMMM-YYYY HH:mm:ss'))
          item.stop_time = ((item.stop_time === null) ? <span style={{color: "#ff0000", textDecoration: "underline"}}>ไม่พบเวลาหยุด</span>: moment(item.stop_time).format('DD-MMMM-YYYY HH:mm:ss'))
          return item
        })
      }
      console.log(res.data)
      if (res.data.msg === undefined){
        this.setState({PumpStatus: res.data})
      }else {
        this.setState({PumpStatus: []})
      }
    }
    const res = await API().get('/sensors/list')
    _.sortBy(res.data.device);
    res.data.device.sort()
    this.setState({listDeivce: [...new Set(res.data.device)]});
  }
  componentWillUnmount(){

  }
  
  handleSelectDevice(e) {
    this.setState({selectDevice: e.target.value})
  }
  render() {
    const { classes } = this.props;
    // const currentPath = this.props.currentPath;
    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <Grid container spacing={3}>
              <Grid item sm={10} className={classes.header}>
                Pump Status
              </Grid>
                <Grid container spacing={3}>
                  <Grid item md={2} >
                    <FormControl className={classes.formControl}>
                      <InputLabel shrink htmlFor="select-native-label-placeholder">
                        เลือกอุปกรณ์
                      </InputLabel>
                      <NativeSelect
                        inputProps={{
                          name: 'Select Device',
                          id: 'select-native-label-placeholder',
                        }}
                        value={this.state.selectDevice}
                        onChange={this.handleSelectDevice }
                      >
                        <option aria-label="None" value="" />
                        {
                          this.state.listDeivce.map((item, index) => {
                            return (<option key={index} value={item}>{item}</option>)
                          })
                        }
                      </NativeSelect>
                      <FormHelperText></FormHelperText>
                    </FormControl>
                  </Grid>
                  <Grid item sm={4}>
                    <div className={classes.buttonFloat}>  
                      <Button 
                        className={classes.buttonMargin}
                        variant="outlined" 
                        color="default" 
                        onClick={this.handleClickSelectDevice}
                      >
                        เลือกดูข้อมูล
                      </Button>
                    </div>
                  </Grid>
                </Grid>
                <Grid container spacing={3}>
                  <Grid item md={12}>
                    <BeamPumpStatusTable store={store} PumpStatus={this.state.PumpStatus}/>
                  </Grid>
                </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamPumpStauts);