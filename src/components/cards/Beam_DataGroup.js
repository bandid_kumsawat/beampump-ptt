import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { red } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button'

import DynaChart from './Chart/DynaChart'
import { store } from './../../store'
import "./css/Beam_DataGroup.css"
import BeamFrom from './Beam_From'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  hieghtlightchart: {
    borderTop: "3px solid #ffffffff"
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
    // paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  boxShow: {
    margin: "0px auto",
    float: "left!important",
    flexDirection: "row !important",
    display: "flex !important",
    position: "relative",
    minWidth: 0,
  },
  boxMain: {
    overflowX: "scroll !important",
  },
  DynaChart: {
    margin: "5px 10px 5px 0px",
    cursor: "grab",
  },
  exportdata: {
    marginTop: "10px",
    fontSize:"18px",
    textDecoration: "underline",
    color: "#0000ff",
    cursor: "pointer",
    display:"flex",
    justifyContent: "flex-end"
  }
});

class BeamDataGrouped extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      welldata: []
    }
  }

  componentDidMount () {
    if (this.store.getState().wellResult.data !== undefined){
      this.setState({welldata: this.store.getState().wellResult.data});
    }
  }

  genChart(CardDate , index, data, classes){
    // var length = this.store.getState().wellResult.data.length
    // if (index > (length - 3)){
    // }
    return (
      <DynaChart
        cid={data[index].id}
        className={classes.DynaChart}
        store={store}
        key={index} 
        color='#29abe2'
        fill='#E8F7FF'
        index={index}
        Loads={data[index].Loads} 
        Positions={data[index].Positions}
        // title={data[index].station_id}
        width='500'
        height='300'
        // title={data[index].station_id}
        subTitle={data[index].DateTime}
        hover={true}
        border={true}
        onclick={true}

        detailline={true}
        PumpCardLoads={data[index].PumpCardLoads}
        PumpCardPositions={data[index].PumpCardPositions}
        autoscale={false}
        // detailline={false}
        // autoscale={true}
      />
    )
  }

  handleDragResultQuery (classDrag){
    const slider = document.querySelector('.' + classDrag);

    if (slider !== null){
      let isDown = false;
      let startX;
      let scrollLeft;
      
      slider.addEventListener('mousedown', (e) => {
        isDown = true;
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });
      slider.addEventListener('mouseleave', () => {
        isDown = false;
      });
      slider.addEventListener('mouseup', () => {
        isDown = false;
      });
      slider.addEventListener('mousemove', (e) => {
        if(!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
      });
    }
  }

  render() {
    const { classes } = this.props;
    this.handleDragResultQuery(classes.boxMain)
    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <BeamFrom store={store} />
          </CardContent>
          <CardContent>
            <div className={classes.boxMain}>
              <div className={classes.boxShow + " "}>
                {
                  ( 
                    this.store.getState().wellResult.data !== undefined ? 
                      this.store.getState().wellResult.data.map((item, index) => {
                        return this.genChart(item.CardDate, index, this.store.getState().wellResult.data, classes)
                      })
                    : "ไม่พบข้อมูล" 
                  ) 
                }
              </div>
            </div>
            <div className={classes.exportdata}>
              <Button variant="contained" color="primary" onClick={() => {
                var csv = "STATION_ID,DATETIME,POSITIONS,LOADS\n"
                this.store.getState().wellResult.data.forEach((item, index) => {
                  var station_id = item.station_id
                  var date = item.DateTime
                  item.Positions.forEach((itemP, indexP) => {
                    csv += station_id + "," + date + "," + itemP + "," + item.Loads[indexP] + "\n"
                  })
                });
                const url = window.URL.createObjectURL(new Blob([csv]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'PWA_Stock_Logger_Export.csv'); //or any other extension
                document.body.appendChild(link);
                link.click();
              }}>
                Export .csv
              </Button>
            </div>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamDataGrouped);