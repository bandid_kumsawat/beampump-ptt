import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import InputLabel from '@material-ui/core/InputLabel';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
// import BeamPumpStatusTable from './table/PumpStatusTable'
import RodTable from './table/RobTable'

import API from './../../api/server'
import _ from 'underscore'

import './Beam_Current.css'
import './css/Beam_Visualisation.css'

// import bp_icon from './../../images/beampump.png'
import bp_icon from './../../images/sucker-rod.png'


import { store } from './../../store'

import moment from 'moment';

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  hieghtlightchart: {
    borderTop: "3px solid #ffffffff",
  },
  header: {
    fontSize: '16px',
    fontWeight: '600',
  },
  formControl: {
    minWidth: "100%",
    margin: "20px 0px 20px 30px"
  },
  buttonMargin: {
    margin: "30px 0px 20px 30px"
  },
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
    fontSize: '16px',
  },
  LEGEND: {
    padding: "2px"
  },
});

const ColorButton_mock = withStyles((theme) => ({
  root: {
    width: "100%",
    color: theme.palette.getContrastText('#0a88be'),
    backgroundColor: '#0a88be',
    '&:hover': {
      backgroundColor: '#0a88be',
    },
  },
}))(Button);

class BeamPumpStauts extends Component {
  constructor(props) {
    super(props);
    this.store = this.props.store;
    this.state = {
      listDeivce: [],
      selectDevice: "",
      PumpStatus: [
        {
          "index": "1",
          "rod_desc": "W-T66/XD - 0.875",
          "rod_count": "60",
          "rod_length": "25",
          "up_damp": "0.02232",
          "down_damp": "0.04432",
          "btn_edit": <Button variant="outlined" color="primary" >EDIT</Button>,
          "btn_del": <Button variant="outlined" color="secondary">DEL</Button>
        },
        {
          "index": "2",
          "rod_desc": "W-T66/XD - 0.875",
          "rod_count": "120",
          "rod_length": "25",
          "up_damp": "0.02123",
          "down_damp": "0.04656",
          "btn_edit": <Button variant="outlined" color="primary" >EDIT</Button>,
          "btn_del": <Button variant="outlined" color="secondary">DEL</Button>
        },
        {
          "index": "3",
          "rod_desc": "SB (K) - 1.500",
          "rod_count": "3",
          "rod_length": "25",
          "up_damp": "0.04242",
          "down_damp": "0.12292",
          "btn_edit": <Button variant="outlined" color="primary" >EDIT</Button>,
          "btn_del": <Button variant="outlined" color="secondary">DEL</Button>
        }
      ]
    }
    this.handleSelectDevice = this.handleSelectDevice.bind(this)
    this.handleClickSelectDevice = this.handleClickSelectDevice.bind(this)
  }
  async handleClickSelectDevice() {
    if (this.state.selectDevice !== "") {
      console.log(this.state.selectDevice)
      const res = await API().post('/statuspump/get', {
        "station_id": this.state.selectDevice
      })
      if (res.data.msg === undefined) {
        res.data.map((item, index) => {
          item.index = index + 1
          item.period_ms = (item.period_ms / 1000 / 60).toFixed(2)
          item.type = ((item.type === 'START') ? <spen style={{ width: "50px", textAlign: "center", display: "inline-block", color: "#ffffff", backgroundColor: "#1371b8", padding: "2px", borderRadius: "1px" }}>START</spen>

            : <spen style={{ width: "50px", textAlign: "center", display: "inline-block", color: "#ffffff", backgroundColor: "#c43d3dde", padding: "2px", borderRadius: "1px" }}>STOP</spen>)

          item.start_time = ((item.start_time === null) ? "" : moment(item.start_time).format('DD-MMMM-YYYY HH:mm:ss'))
          item.stop_time = ((item.stop_time === null) ? "" : moment(item.stop_time).format('DD-MMMM-YYYY HH:mm:ss'))
          return item
        })
      }
      console.log(res.data)
      this.setState({ PumpStatus: res.data })
    }
  }
  async componentDidMount() {
    const res = await API().get('/sensors/list')
    _.sortBy(res.data.device);
    res.data.device.sort()
    this.setState({ listDeivce: [...new Set(res.data.device)] });
  }
  componentWillUnmount() {

  }

  handleSelectDevice(e) {
    this.setState({ selectDevice: e.target.value })
  }
  render() {
    const { classes } = this.props;
    // const currentPath = this.props.currentPath;
    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <Grid container spacing={3}>
              <Grid item sm={10} className={classes.header}>
                Well Parameter Configulation
              </Grid>
              <Grid container spacing={3}>
                <Grid item md={2} >
                  <FormControl className={classes.formControl}>
                    <InputLabel shrink htmlFor="select-native-label-placeholder">
                      เลือกอุปกรณ์
                      </InputLabel>
                    <NativeSelect
                      inputProps={{
                        name: 'Select Device',
                        id: 'select-native-label-placeholder',
                      }}
                      value={this.state.selectDevice}
                      onChange={this.handleSelectDevice}
                    >
                      <option aria-label="None" value="" />
                      {
                        this.state.listDeivce.map((item, index) => {
                          return (<option key={index} value={item}>{item}</option>)
                        })
                      }
                    </NativeSelect>
                    <FormHelperText></FormHelperText>
                  </FormControl>
                </Grid>
                <Grid item sm={4}>
                  <div className={classes.buttonFloat}>
                    <Button
                      className={classes.buttonMargin}
                      variant="outlined"
                      color="default"
                      onClick={this.handleClickSelectDevice}
                    >
                      เลือกดูข้อมูล
                      </Button>
                  </div>
                </Grid>
              </Grid>

              {/* edit grid Left */}
              <Grid container spacing={3}>



                <Grid item md={4}>
                  {/* here */}
                  <Grid container spacing={0}
                    alignItems="center"
                    justify="center"
                  >



                    <FormGroup>
                      <img src={bp_icon} alt="bp_icon" ></img>
                      <Typography component="div">
                        <Grid component="label" container alignItems="center" spacing={1}>
                          <Grid item>Metre</Grid>
                          <Grid item>
                            <Switch checked={true} name="checkedC" />
                          </Grid>
                          <Grid item>Ft</Grid>
                        </Grid>
                      </Typography>
                    </FormGroup>
                  </Grid>
                </Grid>


                <Grid item md={8}>

                  {/* here is Paramconfig*/}
                  <fieldset className={classes.FIELDSET}>
                    <legend className={classes.LEGEND}>DownHole Parameter</legend>

                    <Grid container spacing={3}>
                      <Grid container spacing={3}> 
                        <Grid item sm={6} style={{paddingRight: "-10px"}}>
                          <fieldset className={classes.FIELDSET}>
                            <legend className={classes.LEGEND}>Depths</legend>
                            <div style={{display: "flex"}}>
                              <div style={{paddingLeft: "20px"}}>Top Preforation</div>
                              <div style={{paddingLeft: "72px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={666.3}></input></div>
                            </div>

                            <div style={{display: "flex",marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Bottom Preforation</div>
                              <div style={{paddingLeft: "44px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={6496.1}></input></div>
                            </div>


                            <div style={{display: "flex",marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Anchor Depth</div>
                              <div style={{paddingLeft: "82px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={6013.8}></input></div>
                            </div>


                            <div style={{display: "flex",marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Plug-Back TD</div>
                              <div style={{paddingLeft: "86px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={9478.3}></input></div>
                            </div>

                            <div style={{display: "flex",marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Pump Measured</div>
                              <div style={{paddingLeft: "66px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={5820.2}></input></div>
                            </div>


                            <div style={{display: "flex",marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Pump Vertical</div>
                              <div style={{paddingLeft: "82px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={4514.4}></input></div>
                            </div>

                            <div style={{display: "flex",marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Stork Length</div>
                              <div style={{paddingLeft: "91px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={4514.4}></input></div>
                            </div>

                          </fieldset>
                        </Grid>
                        <Grid item sm={6} style={{paddingRight: "-10px"}}>
                          <fieldset className={classes.FIELDSET}>
                            <legend className={classes.LEGEND}>Casing/Tubing</legend>
                            <div style={{display: "flex"}}>
                              <div style={{paddingLeft: "20px"}}>Pump ID</div>
                              <div style={{paddingLeft: "90px"}}>inch</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={1.5}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}><input type={'checkbox'}></input></div>
                              <div style={{paddingLeft: "20px"}}>Casing Valve Open?</div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Casing OD</div>
                              <div style={{paddingLeft: "76px"}}>inch</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={7}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Casing Weight</div>
                              <div style={{paddingLeft: "48px"}}>lb/ft</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={23}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Tubing OD</div>
                              <div style={{paddingLeft: "74px"}}>inch</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={2.875}></input></div>
                            </div>
                          </fieldset>
                        </Grid>
                      </Grid>
                    </Grid>

                  </fieldset>

                  <fieldset className={classes.FIELDSET}>
                    <legend className={classes.LEGEND}>Rod Configulation</legend>
                    <RodTable store={store} PumpStatus={this.state.PumpStatus} />
                  </fieldset>


                  <fieldset className={classes.FIELDSET}>
                    <legend className={classes.LEGEND}>Fluid Configulation</legend>
                      <Grid container spacing={3}> 
                        <Grid item sm={6} style={{paddingRight: "-10px"}}>
                          <fieldset className={classes.FIELDSET}>
                            <legend className={classes.LEGEND}>Rate</legend>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Oil</div>
                              <div style={{paddingLeft: "120px"}}>stb/d</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={18.7}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Water</div>
                              <div style={{paddingLeft: "95px"}}>stb/d</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={16.3}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Gas</div>
                              <div style={{paddingLeft: "102px"}}>mscf/d</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={5}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Last Test Date</div>
                              <div style={{paddingLeft: "48px"}}>inch</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={"01/07/2020"}></input></div>
                            </div>
                          </fieldset>
                          <fieldset className={classes.FIELDSET}>
                            <legend className={classes.LEGEND}>Gravity</legend>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Oil API</div>
                              <div style={{paddingLeft: "84px"}}>deg API</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={18.7}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Water</div>
                              <div style={{paddingLeft: "94px"}}>sp grav</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={16.3}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Gas</div>
                              <div style={{paddingLeft: "112px"}}>sp grav</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={5}></input></div>
                            </div>
                          </fieldset>
                        </Grid>
                        <Grid item sm={6} style={{paddingRight: "-10px"}}>
                          <fieldset className={classes.FIELDSET}>
                            <legend className={classes.LEGEND}>Tubing</legend>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Pressure</div>
                              <div style={{paddingLeft: "120px"}}>PSI</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={5}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Gradient</div>
                              <div style={{paddingLeft: "106px"}}>psi/ft</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={5}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Wellhead Temp.</div>
                              <div style={{paddingLeft: "55px"}}>DegF</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={5}></input></div>
                            </div>
                          </fieldset>
                          <fieldset className={classes.FIELDSET}>
                            <legend className={classes.LEGEND}>Casing</legend>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Pressure</div>
                              <div style={{paddingLeft: "140px"}}>PSI</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={90}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Oil Cut</div>
                              <div style={{paddingLeft: "86px"}}>fraction {"<="} 1</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={0.5}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Fluid Level From Surface</div>
                              <div style={{paddingLeft: "25px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={3444.9}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Fluid Above Pump</div>
                              <div style={{paddingLeft: "70px"}}>feet</div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={2375.9}></input></div>
                            </div>
                            <div style={{display: "flex", marginTop: "10px"}}>
                              <div style={{paddingLeft: "20px"}}>Fluid Shot Date</div>
                              <div style={{paddingLeft: "117px"}}></div>
                              <div style={{paddingLeft: "20px"}}><input type="text" defaultValue={'26/03/2020'}></input></div>
                            </div>
                          </fieldset>
                          <ColorButton_mock >Update</ColorButton_mock>
                        </Grid>
                      </Grid>
                  </fieldset>

                </Grid>
              </Grid>


            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamPumpStauts);