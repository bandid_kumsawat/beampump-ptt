import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {Tooltip,Typography} from '@material-ui/core';

import './css/Beam_Visualisation.css'

function Table(props) {
  const classes = useStyles();
  const { rod_config, index, ...other } = props;
  // const [rod_config] = React.useState(props.rod_config)
  const [colum] = React.useState([
    {
      "parameter": "taper_num",
      "label": "TaperNumber"
    },
    {
      "parameter": "rob_density",
      "label": "Rob Density"
    },
    {
      "parameter": "rod_elastic",
      "label": "Rob Elastic"
    },
    {
      "parameter": "rob_type",
      "label": "Rob Desscription"
    },
    {
      "parameter": "rob_diameter",
      "label": "Rob Diameter"
    },
    {
      "parameter": "rob_count",
      "label": "Number of Rob"
    },
    {
      "parameter": "rob_length",
      "label": "Rob Length"
    },
    {
      "parameter": "dampup_factor",
      "label": "Up Storke Damping"
    },
    {
      "parameter": "dampdown_factor",
      "label": "Down Storke Damping"
    },
  ])
  return (
    <div
      role="tabpanel"
      id={`table-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      <table id="paramValue">
        <thead></thead>
        <tbody>
          <tr>
            {
              colum.map((item, index) => {
                return (<th key={index}>{item.label}</th>)
              })
            }
          </tr>
          {
            rod_config.map((itemT, index) => {
              return <tr key={index}>
                <td className={classes.paramValue}>{itemT.taper_num}</td>
                <td className={classes.paramValue}>{itemT.rod_type}</td>
                <td className={classes.paramValue}>{itemT.rod_count}</td>
                <td className={classes.paramValue}>{itemT.rod_density}</td>
                <td className={classes.paramValue}>{itemT.rod_elastic}</td>
                <td className={classes.paramValue}>{itemT.rod_length}</td>
                <td className={classes.paramValue}>{itemT.rod_diameter}</td>
                <td className={classes.paramValue}>{itemT.dampup_factor}</td>
                <td className={classes.paramValue}>{itemT.dampdown_factor}</td>
              </tr>
            })
          }
        </tbody>
      </table>
    </div>
  );
}

Table.propTypes = {
  index: PropTypes.any.isRequired,
  rod_config: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  title:{
    fontSize: "16px",
    fontWeight: "700",
    // textDecoration: 'underline',
    marginBottom: "14px",
    marginTop: "14px"
  },
  paramValue: {
    color: "#000000",
    textAlign:'center',
    fontWeight: 400
  },
  paramValueheader: {
    textAlign:'center',
    fontWeight: 400
  },
  link: {
    fontSize: "16px",
    fontWeight: "400",
    marginBottom: "14px",
    marginTop: "14px",
    marginLeft: "16px"
  },
  link_sec: {
    fontSize: "16px",
    fontWeight: "400",
    marginBottom: "14px",
    marginTop: "14px",
    marginLeft: "5px"
  }
}));

WellInformation.propTypes = {
  index: PropTypes.any.isRequired,
  rod_config: PropTypes.any.isRequired,
};

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 1)',
    maxWidth: '100%',
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip);

export default function WellInformation(props) {
  const classes = useStyles();
  const { title, rod_config, index, /*...other*/ } = props;
  
  return (
    <div className={classes.root}>
      <div style={{display: "flex"}} >
        <div className={classes.title} hidden={title}>Well Information</div>
        <div className={classes.link} hidden={title}><span>[
          

          <HtmlTooltip
            title={
              <React.Fragment>
                <Typography color="inherit"><u>คลิก</u>เพื่อดูข้อมูล Rob Information</Typography>
                  {/* <em>{"And here's"}</em> <b>{'some'}</b> <u>{'amazing content'}</u>.{' '}
                  {"It's very engaging. Right?"} */}
              </React.Fragment>
            }
          >
            <a href={"/"}>Rob Information</a>
          </HtmlTooltip>

          ,</span></div>
        <div className={classes.link_sec} hidden={title}><span>
          
          <HtmlTooltip
            title={
              <React.Fragment>
                <Typography color="inherit"><u>คลิก</u>เพื่อดูข้อมูล D/H Parameter</Typography>
                {/* <em>{"And here's"}</em> <b>{'some'}</b> <u>{'amazing content'}</u>.{' '}
                {"It's very engaging. Right?"} */}
              </React.Fragment>
            }
          >
            <a href={"/"}>D/H Parameter</a>
          </HtmlTooltip>


        ]</span></div>
      </div>
      <Table rod_config={rod_config} index={index}/>
    </div>
  );
}