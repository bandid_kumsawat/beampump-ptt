import React, { Component } from 'react';
import withStyles from '@material-ui/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid } from '@material-ui/core';
import DynaChartResponsive_Current from './Chart/DynaChartResponsive_Current'
import DynaChartResponsive_Predict from './Chart/DynaChartResponsive_Predict'
import Bean_CurentGroup from './Bean_CurentGroup'
import moment from 'moment'
import { store } from './../../store'
import './Beam_Current.css'
import { setcurrentdevice, setprediction, setpredictioncurrent } from './../../actions';

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';

import _ from 'underscore'

import API from './../../api/server'

const styles = theme => ({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
  hieghtlightchart: {
    borderTop: "3px solid #ffffffff"
    // 0a88be
  },
  content: {
    fontSize: "16px"
  },
  media: {
    height: 0,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  formControl: {
    minWidth: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  buttonMargin: {
    marginTop: "12px",
    width: '100%'
  },
  buttonFloat: {
    display: 'block',
    float: 'left'
  },
  containerCenter:{

  },
  title: {
    width: "100%",
    textAlign: "center",
    margin: "20px",
    padding: "8px",
    borderBottom: '1px solid #0000007a',
    fontSize: '20px'
  },
  title2: {
    width: "100%",
    textAlign: "center",
    margin: "0px",
    padding: "8px",
    fontSize: '20px'
  },
  spacingPadding: {
    paddingRight: "2%",
    paddingLeft: "2%",
  }
});

class BeamViewSelect extends Component {
  constructor(props){
    super(props);
    this.store = this.props.store;
    this.state = {
      localS: localStorage.getItem('select_Dynachart'),
      listDeivce: [],
      selectDevice: '',
      listType: [],
      selectType: ''
    }
    this.handleViewDevice = this.handleViewDevice.bind(this);
    this.handleSelectDevice = this.handleSelectDevice.bind(this)
    this.handleType = this.handleType.bind(this)
  }
  async componentDidMount() {
    this.store.dispatch(setpredictioncurrent([undefined]))
    const res = await API().get('/sensors/list')
    _.sortBy(res.data.device);
    res.data.device.sort()
    this.setState({listDeivce: [...new Set(res.data.device)]});
    _.sortBy(res.data.type);
    res.data.type.sort()
    this.setState({listType: [...new Set(res.data.type)]});

    if (localStorage.getItem('device') === null 
      && localStorage.getItem('date') === null 
      && localStorage.getItem('offset') === null 
      && localStorage.getItem('type_current') === null
    ) {
      localStorage.setItem('device', 'LKU-R16T');
      localStorage.setItem('date', "2020-02-26");
      localStorage.setItem('offset', "10");
      localStorage.setItem('type_current', "FC")
    }

    if (localStorage.getItem('device') === null){
      this.setState({selectDevice: 'LKU-R16T'})
      localStorage.setItem('device', 'LKU-R16T' )
    } else {
      this.setState({selectDevice: localStorage.getItem('device')})
    }
    if (localStorage.getItem('type_current') === null){
      this.setState({selectType: "FC"})
    }else{
      this.setState({selectType: localStorage.getItem('type_current')})
    }

    const res2 = await API().post('/dynadata', {
      "WellName": this.state.selectDevice,
      "year": new Date().getFullYear(),
      "month": new Date().getMonth() + 1,
      "day": new Date().getDate(),
      "offset": 3
    })
    res2.data = res2.data.data
    // res2.data = res2.data.map(v => Object.assign({}, v, {DateFrom: new Date(v.DateTime)}))
    // น้อยไปมาก
    // .sort((a, b) => a.DateFrom - b.DateFrom) // ASC
    // มากไปน้อย
    // .sort((a, b) => b.DateFrom - a.DateFrom); // DESC

    if (res2.data.length > 0){
      var TempDT = {
        "DateTime": moment(res2.data[10].DateTime).add(1, 'days').format('DD-MMMM-YYYY HH:MM:SS'),
        "Loads": res2.data[10].Loads,
        "Positions": res2.data[10].Positions,
        "station_id": res2.data[10].station_id,
        "id": "00000ab741f5364e11445111",
        "pump_status": true,
        "PumpCardLoads": res2.data[10].PumpCardLoads,
        "PumpCardPositions": res2.data[10].PumpCardPositions,
        "TT": "99.58",
        "PT": "11548",
        "speed": "112.2",
        "Loads_min": 7795,
        "Loads_max": 18744,
        "Positions_min": 1144,
        "Positions_max": 2598,
        "rtu_status": false,
      }
      // res2.data.unshift(TempDT)

      res2.data = [TempDT].concat(res2.data) // [ 4, 3, 2, 1 ]
    }

    this.store.dispatch(setcurrentdevice([res2.data[1]]))
    this.store.dispatch(setprediction([res2.data[((res2.data.length >= 10) ? 0 : 10)]]))
    this.store.dispatch(setpredictioncurrent({data: res2.data}))
    // console.log("-------------- reverse ----------------")
    // console.log(res2.data)
  }
  handleSelectDevice(ev) {
    this.setState({selectDevice: ev.target.value});
    localStorage.setItem('device', ev.target.value);
  }
  handleType(ev){
    this.setState({selectType: ev.target.value});
    localStorage.setItem('type_current', ev.target.value)
  }
  async handleViewDevice(ev) {
    this.store.dispatch(setpredictioncurrent([undefined]))
    this.store.dispatch(setcurrentdevice([undefined]))
    this.store.dispatch(setprediction([undefined]))

    const res = await API().post('/dynadata', {
      "WellName": this.state.selectDevice,
      "year": new Date().getFullYear(),
      "month": new Date().getMonth() + 1,
      "day": new Date().getDate(),
      "offset": 3
    })
    res.data = res.data.data
    res.data = res.data.map(v => Object.assign({}, v, {DateFrom: new Date(v.DateTime)}))
    // น้อยไปมาก
    // .sort((a, b) => a.DateFrom - b.DateFrom) // ASC
    // มากไปน้อย
    .sort((a, b) => b.DateFrom - a.DateFrom); // DESC
    if (res.data.length > 0){
      var TempDT = {
        "DateTime": moment(res.data[10].DateTime).add(1, 'days').format('DD-MMMM-YYYY HH:MM:SS'),
        "Loads": res.data[10].Loads,
        "Positions": res.data[10].Positions,
        "station_id": res.data[10].station_id,
        "id": "00000ab741f5364e11445111",
        "pump_status": true,
        "PumpCardLoads": res.data[10].PumpCardLoads,
        "PumpCardPositions": res.data[10].PumpCardPositions,
        "TT": "99.58",
        "PT": "11548",
        "speed": "112.2",
        "Loads_min": 7795,
        "Loads_max": 18744,
        "Positions_min": 1144,
        "Positions_max": 2598,
        "rtu_status": false,
      }
      // res2.data.unshift(TempDT)
      res.data = [TempDT].concat(res.data) // [ 4, 3, 2, 1 ]
    }
    this.store.dispatch(setcurrentdevice([res.data[1]]))
    this.store.dispatch(setprediction([res.data[0]]))
    this.store.dispatch(setpredictioncurrent({data: res.data}))
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.hieghtlightchart}>
          <CardContent>
            <Grid container spacing={3}>
              <Grid item sm={4}>
                <FormControl className={classes.formControl}>
                  <InputLabel shrink htmlFor="select-native-label-placeholder">
                    เลือกอุปกรณ์
                  </InputLabel>
                  <NativeSelect
                    value={this.state.selectDevice}
                    onChange={this.handleSelectDevice }
                    inputProps={{
                      name: 'Select Device',
                      id: 'select-native-label-placeholder',
                    }}
                  >
                    <option aria-label="None" value="" />
                    {
                      this.state.listDeivce.map((item, index) => {
                        return (<option key={index} value={item}>{item}</option>)
                      })
                    }
                  </NativeSelect>
                  <FormHelperText></FormHelperText>
                </FormControl>
              </Grid>

              {/* <Grid item sm={4}>
                <FormControl className={classes.formControl}>
                  <InputLabel shrink htmlFor="select-native-label-placeholder-type">
                    เลือกชนิดอุปกรณ์
                  </InputLabel>
                  <NativeSelect
                    value={this.state.selectType}
                    onChange={this.handleType }
                    inputProps={{
                      name: 'Select Type',
                      id: 'select-native-label-placeholder-type',
                    }}
                  >
                    <option aria-label="None" value="" />
                    {
                      this.state.listType.map((item, index) => {
                        return (<option key={index} value={item}>{item}</option>)
                      })
                    }
                  </NativeSelect>
                  <FormHelperText></FormHelperText>
                </FormControl>
              </Grid> */}

              <Grid item sm={4}>
                <div className={classes.buttonFloat}>  
                  <Button 
                    className={classes.buttonMargin}
                    variant="outlined" 
                    color="default" 
                    disabled={( this.state.selectDevice === "" || this.state.selectDate === "" || this.state.selectOffset === 0)}
                    onClick={this.handleViewDevice}
                  >
                    เลือกดูข้อมูล
                  </Button>
                </div>
              </Grid>

            </Grid>

            <Grid container justify="center">
              <Grid item xs={12}>
                <Bean_CurentGroup store={store} />
              </Grid>
            </Grid>

            <Grid container spacing={3}>
            <Grid item md={6}>
                {/* <div className={classes.title}>PREDICTION</div> */}
                {
                  ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : this.store.getState().currentWellPrediction.map((data ,index) => {
                    return (<DynaChartResponsive_Predict
                      cid={data.id}
                      className={classes.DynaChart}
                      store={store}
                      color='#FF46A4'
                      fill="#FFF7FB"
                      key={index} 
                      index={index}
                      Loads={data.Loads} 
                      Positions={data.Positions}
                      width='800'
                      height='500'
                      // title={data.station_id}
                      // subTitle={moment(this.store.getState().currentWell[index].DateTime)/*.add(1, 'days').format('DD-MMMM-YYYY HH:MM:SS')*/}
                      // subTitle={''}
                      subTitle={data.DateTime}
                      hover={false}
                      border={false}
                      onclick={false}

                      detailline={true}
                      PumpCardLoads={data.PumpCardLoads}
                      PumpCardPositions={data.PumpCardPositions}
                      autoscale={false}
                      // detailline={false}
                      // autoscale={true}
                    />)
                  }))
                }
              </Grid>
              <Grid item md={6}>
                {/* <div className={classes.title}>CURRENT</div> */}
                {
                  ((this.store.getState().currentWell[0] === undefined) ? "" : this.store.getState().currentWell.map((data ,index) => {
                    return (<DynaChartResponsive_Current
                      cid={data.id}
                      className={classes.DynaChart}
                      store={store}
                      key={index} 
                      index={index}
                      Loads={data.Loads} 
                      Positions={data.Positions}
                      color='#29abe2'
                      fill='#E8F7FF'
                      width='800'
                      height='500'
                      // title={data.station_id}
                      subTitle={data.DateTime}
                      hover={false}
                      border={false}
                      onclick={false}

                      detailline={true}
                      PumpCardLoads={data.PumpCardLoads}
                      PumpCardPositions={data.PumpCardPositions}
                      autoscale={false}
                      // detailline={false}
                      // autoscale={true}
                    />)
                  }))
                }
              </Grid>
              
            </Grid>
            <hr></hr>
            <Grid container spacing={1} >
              <Grid item sm={6} className={classes.spacingPadding}>
                <div className={classes.title2}>Well Information</div>
                <table id="table_detail_current">
                  <tbody>
                    <tr>
                      <td>Well Name</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : this.store.getState().currentWellPrediction[0].station_id)
                        }
                      </td>
                    </tr>
                    <tr>
                      <td>Card Report Date</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : moment(this.store.getState().currentWellPrediction[0].DateTime).add(0, 'days').format('DD-MMMM-YYYY HH:MM:SS'))
                        }
                      </td>
                    </tr>
                    {/* <tr>
                      <td>Card Report Type</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : this.store.getState().currentWellPrediction[0].CardType)
                        }
                      </td>
                    </tr> */}
                  </tbody>
                </table>
              </Grid>
              <Grid item sm={6} className={classes.spacingPadding}>
                <div className={classes.title2}>Readings</div>
                <table id="table_detail_current">
                  <tbody>
                    <tr>
                      <td>Max Load (lbs)</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : 
                            Math.min.apply(Math, this.store.getState().currentWellPrediction[0].Loads.map(function(o) { 
                                return o;  
                            }))
                          )
                        }
                      </td>
                    </tr>
                    <tr>
                      <td>Min Load (lbs)</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : 
                            Math.max.apply(Math, this.store.getState().currentWellPrediction[0].Loads.map(function(o) { 
                              return o;  
                            }))
                          )
                        }
                      </td>
                    </tr>
                    <tr>
                      <td>Speed (rpm.)</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : 
                          this.store.getState().currentWellPrediction[0].speed
                          )
                        }
                      </td>
                    </tr>
                    <tr>
                      <td>Well State/ Shutdown Event</td>
                      <td>0</td>
                    </tr>
                    <tr>
                      <td>Temperature Transmitter (C°)</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : 
                            this.store.getState().currentWellPrediction[0].TT
                          )
                        }
                        </td>
                    </tr>
                    <tr>
                      <td>Pressure Transmitter (psi.)</td>
                      <td>
                        {
                          ((this.store.getState().currentWellPrediction[0] === undefined) ? "" : 
                            this.store.getState().currentWellPrediction[0].PT
                          )
                        }
                      </td>
                    </tr>
                    
                    {/* <tr>
                      <td>Gross Stroke (in)</td>
                      <td>0</td>
                    </tr>
                    <tr>
                      <td>Net Stroke (in)</td>
                      <td>0</td>
                    </tr>
                    <tr>
                      <td>Pump Fillage (%)</td>
                      <td>0</td>
                    </tr>
                    <tr>
                      <td>Fluid Load (%)</td>
                      <td>0</td>
                    </tr> */}
                  </tbody>
                </table>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(BeamViewSelect);