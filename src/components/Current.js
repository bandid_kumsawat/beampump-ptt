import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";

// Components
import BeamCurrent from './cards/Beam_Current'

import Topbar from "./Topbar";

// store
import { store } from './../store'

const backgroundShape = require("../images/shape.svg");

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.grey["A500"],
    overflow: "hidden",
    background: `url(${backgroundShape}) no-repeat`,
    backgroundSize: "cover",
    backgroundPosition: "0 400px",
    marginTop: 0,
    padding: 20,
    paddingBottom: 0
  },
  grid: {
    width: 1000
  }
});

class Cards extends Component {
  constructor (){
    super()
    this.state = {
      name: 'champ',
      age: 26
    }
  }
  componentDidMount() {
    // console.log(this.state)
  }
  render() {
    const { classes } = this.props;
    const currentPath = this.props.location.pathname;

    return (

      <React.Fragment>
      <CssBaseline />
      <Topbar currentPath={currentPath} store={store}/>
      <div className={classes.root}>
        <Grid container justify="center">
          <Grid item xs={12}>
            <BeamCurrent store={store}></BeamCurrent>
          </Grid>
        </Grid>
      </div>
    </React.Fragment>
    );
  }
}

export default withStyles(styles)(Cards);
