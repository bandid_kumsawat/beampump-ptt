import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";

// Components
// import BeamFrom from "./cards/Beam_From";
import BeamDataGroup from "./cards/Beam_DataGroup";
import BeamDetailAndPoint from "./cards/Beam_DetailAndPoint_mock"
import BeamTable from './cards/table/ParamTable'
import Gigmock from './cards/Beam_gigMock_page_2'
import Topbar from "./Topbar";

// store
import { store } from '../store'

const backgroundShape = require("../images/shape.svg");

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.grey["A500"],
    overflow: "hidden",
    background: `url(${backgroundShape}) no-repeat`,
    backgroundSize: "cover",
    backgroundPosition: "0 400px",
    marginTop: 0,
    // padding: 20
    paddingRight: 20,
    paddingLeft: 20
  },
  grid: {
    width: 1000
  }
});

class Cards extends Component {
  constructor (){
    super()
    this.state = {
    }
  }
  componentDidMount() {
    // console.log(this.state)
  }
  render() {
    const { classes } = this.props;
    const currentPath = this.props.location.pathname;

    return (
      <>
        <React.Fragment>
          <CssBaseline />
          <Topbar currentPath={currentPath} store={store}/>
          <div className={classes.root}>
            <Grid container justify="center">
              <Grid item md={3}></Grid>
              <Grid item md={6}>
              </Grid>
              <Grid item md={3}></Grid>
            </Grid>
            <br></br>
            <Grid container justify="center">
              <Grid item xs={12}>
                <BeamDataGroup store={store} />
              </Grid>
            </Grid>
            <br></br>
            <Grid container justify="center">
              <BeamDetailAndPoint store={store} />
              {/* <Gigmock /> */}
            </Grid>
            <br></br>
            {/* <BeamTable store={store}/> */}
          </div>
        </React.Fragment>
      </>
    );
  }
}

export default withStyles(styles)(Cards);
