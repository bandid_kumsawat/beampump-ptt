import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";

import Topbar from "./Topbar";

import BeamVisualisation from './cards/Beam_Visualisation'

// store
import { store } from './../store'

const backgroundShape = require("../images/shape.svg");

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.grey["A500"],
    overflow: "hidden",
    background: `url(${backgroundShape}) no-repeat`,
    backgroundSize: "cover",
    backgroundPosition: "0 400px",
    marginTop: 0,
    // padding: 20
    paddingRight: 20,
    paddingLeft: 20
  },
  grid: {
    width: 1000
  },
  hieghtlightchart: {
    borderTop: "3px solid #0a88be"
  },
});

class Visualisation extends Component {
  constructor (){
    super()
    this.state = {
    }
  }
  componentDidMount() {
    // console.log(this.state)
  }
  render() {
    const { classes } = this.props;
    const currentPath = this.props.location.pathname;

    return (
      <>
        <React.Fragment>
          <CssBaseline />
          <Topbar currentPath={currentPath} store={store}/>
          <div className={classes.root}>
            <Grid container justify="center">
              <Grid item md={12}>
                <BeamVisualisation store={store} currentPath={currentPath}/>
              </Grid>
            </Grid>
          </div>
        </React.Fragment>
      </>
    );
  }
}

export default withStyles(styles)(Visualisation);
