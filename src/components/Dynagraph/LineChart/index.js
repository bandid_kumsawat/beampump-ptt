import React from 'react';
import * as d3 from 'd3';
import './style.css'
import json_data_file from './data.json'
import DynaChart from './dynachart.json'

class LineChart extends React.Component {
    constructor(){
        super();
        this.myRef = React.createRef(); 
    }

    componentDidMount(){
        this.LineChartFunction();
    }
    LineChartFunction(){
        const print = (strt) => {
            console.log(strt)
        }
        var width = 600, height = 400

        var data = json_data_file
        var temp = []
        for (let i = 10; i !== 0;i--){
            temp.push({
                "date": 1000 + i,
                "wage": Math.random() * 120    
            })
        }

        data = []

        DynaChart[0].Loads.forEach((item, index, arr) => {
            var tttt = DynaChart[0].Positions[index]
            data.push({
                "date": Number(tttt),
                "wage": Number(item),
            })
        })

        var  max = () => { 
            var temp = Math.max.apply(Math, data.map(function(o) { 
                return o.wage;  
            })); 
            return temp
        }   
        var  min = () => { 
            var temp = Math.min.apply(Math, data.map(function(o) { 
                return o.wage;  
            })); 
            return temp
        } 

        var margin = {
            top: 10,
            right: 20,
            bottom: 50,
            left: 70
        }

        var padding_top = 30

        var div = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        var default_width = width - margin.left - margin.right;
        // 600 - 40 - 20 = 540
        var default_height = height - margin.top - margin.bottom;
        // 400 - 20 - 30 = 350 
        var default_ratio = default_width / default_height;
        // 540 / 350 = 1.542857142857143

        const set_size = () => {
            var w, h
            var current_width = window.innerWidth;
            print(current_width)
            var current_height = window.innerHeight;
            print(current_height)
            var current_ratio = current_width / current_height;

            // desktop
            if (current_ratio > default_ratio) {
                h = default_height;
                w = default_width;

            // mobile
            } else {
                margin.left = 40
                w = current_width - 40;
                h = w / default_ratio;
            }
            // Set new width and height based on graph dimensions
            width = w - 50 - margin.right;
            height = h - margin.top - margin.bottom;
        }

        set_size();
        print(width)
        print(height)
        //end responsive graph code

        // format the data
        // data.forEach(function (d) {
        //     var parseDate = d3.timeParse("%Y");
        //     d.date = +d.date;
        //     d.wage = +d.wage;
        // });
        //sort the data by date so the trend line makes sense
        // data.sort(function (a, b) {
        //     return a.date - b.date;
        // });

        // set the ranges
        var x = d3.scaleTime().range([0, width]);
        var y = d3.scaleLinear().range([height, 0]);

        // Scale the range of the data

        // x.domain([min(), max()]);
        // y.domain([min(), max()]);
        
        // OR

        x.domain([d3.min(data, function (d) {
            return d.date;
        }), d3.max(data, function (d) {
            return d.date;
        })]);
        y.domain([d3.min(data, function (d) {
            return d.wage;
        }), d3.max(data, function (d) {
            return d.wage;
        })]);

        // define the line
        var valueline = d3.line()
            .curve(d3.curveCatmullRom.alpha(1))
            .x(function (d) {
                return x(d.date);
            })
            .y(function (d) {
                return y(d.wage);
            });

        // append the svg object to the body of the page
        var svg = d3.select(this.myRef.current).append("svg")
            // .attr("style", "padding-top: "+padding_top+";")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom + padding_top)
            .attr("viewBox", "0 0 "+(width + margin.left + margin.right + 100)+" " + (height + margin.top + margin.bottom + padding_top))
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        

        // title line chart
        svg.append("text")
            .attr('y', -30)
            .attr("class", "title-line-chart")
            .attr('x', height / 2)
            .attr('fill', "#000000")
            .text("Dyna Graph")

        // sub title line chart
        svg.append("text")
            .attr('y', -10)
            .attr("class", "sub-title-line-chart")
            .attr('x', (height / 2) - 60)
            .attr('fill', "#000000")
            .text("LKU-A05T 01-26-2020  01-37-52")

        // Add the trendline
        print("Variable\t['data']")
        print(data)
        svg.append("path")
            .data([data])
            .attr("class", "line")
            .attr("d", valueline)
            .attr("stroke", "#D41243")
            .attr("stroke-width", 2)
            .attr("fill", "#FFFFFF")
            .on('mouseover', function (d, i) {
                d3.select(this).transition()
                    .duration('100')
                    .attr("r", 7);
                div.transition()
                    .duration(100)
                    .style("opacity", 1);
                div.html("*LKU-A05T")
                    .style("left", (d3.event.pageX + 10) + "px")
                    .style("top", (d3.event.pageY - 15) + "px");
            })
            .on('mouseout', function (d, i) {
                d3.select(this).transition()
                    .duration('200')
                    .attr("r", 5);
                div.transition()
                    .duration('200')
                    .style("opacity", 0);
            })

            // Add the data points
            // svg.selectAll("dot")
            //     .data(data)
            //     .enter().append("circle")
            //     .attr("r", 5)
            //     .attr("cx", function (d) {
            //         return x(d.date);
            //     })
            //     .attr("cy", function (d) {
            //         return y(d.wage);
            //     })
            //     .attr("stroke", "#D41243")
            //     .attr("stroke-width", 1.5)
            //     .attr("fill", "#DED2F2")

            // Add the axis
            // X
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x)
                    .ticks(3)
                    .tickFormat((d) => {
                        return d3.format("")(d)
                    }));
           
            var scale = d3.scaleLinear()
                .domain([min() , max()])
                .range([height, 0]);

            svg.append("text")
                .attr("text-anchor", "end")
                .attr("x", width / 2)
                .attr("y", height + margin.top + 20)
                .text("Positions");

            // Y
            svg.append("g")
                .call(d3.axisLeft(y).scale(scale)
                    .ticks(6)
                    .tickFormat(function (d) {
                        // return "$" + d3.format(".2f")(d)
                        return d3.format("")(d)
                    }));
            svg.append("text")
                .attr("text-anchor", "end")
                .attr("transform", "rotate(-90)")
                .attr("y", -(margin.left)+20)
                .attr("x", -(height / 2) + 20)
                .text("Loads")
    
    }
    render(){
        return (
            <div ref={this.myRef}></div>
        );
    }
 
}
export default LineChart;