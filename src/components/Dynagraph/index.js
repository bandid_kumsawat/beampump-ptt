import React from 'react'
import './connection.css'
// import Setting from './setting'
import ViewChart from './viewchart'

export default () => (
  <div className="connection pt-2">
    <ViewChart />
  </div>
)