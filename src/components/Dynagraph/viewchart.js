import React, { Component } from 'react';
import {
  Container, Row, Col, Card, Form, Spinner, Button
} from 'react-bootstrap';
import "./viewchart.css"
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import moment from 'moment'
import _ from "underscore"
import * as d3 from "d3";
import Dynagraph from "./dynagraph"
import Barchart from './barchart'
import Dyna_data from './../Dynagraph/BeamPumpLineChart/dynachart1.json'

import Linechart from './LineChart'
import BeamPumpLineChart from './BeamPumpLineChart'

// API Call data from Server 
import API from './../api/server'

import group1 from './../Dynagraph/BeamPumpLineChart/group1.json'
import group2 from './../Dynagraph/BeamPumpLineChart/group2.json'
import group3 from './../Dynagraph/BeamPumpLineChart/group3.json'

class setting extends Component {

  constructor() {
    super();
    this.myRef = React.createRef();
    this.state = {
      show_bg: '',
      device: [],
      device_select: "LKU-R16T",
      page_load: true,
      selectedDate: new Date("2020-02-18T21:11:54"),
      setSelectedDate: new Date("2020-02-18T21:11:54"),
      offset: 10,
      handleDateChange: (date) => {
        this.setState(state => ({
          setSelectedDate : date
        }))
        this.setState(state => ({
          selectedDate : date
        }))
      },
      dynadata: [],
      dynaimage: [],
      dynaparam: {
        name: '',
        date: '',
        index: '',
        data: [],
      },
      file_csv:''
    };
    this.onClickSubmit = this.onClickSubmit.bind(this);
    this.onChangeDevice = this.onChangeDevice.bind(this);
    this.onChangeInputOffset = this.onChangeInputOffset.bind(this);
  }

  drag_and_click_tag1 (){
    const slider = document.querySelector('.drag-function');
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener('mousedown', (e) => {
        isDown = true;
        // slider.classList.add('active');
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
        console.log(slider.scrollLeft)
    });
    slider.addEventListener('mouseleave', () => {
        isDown = false;
        // slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', () => {
        isDown = false;
        // slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', (e) => {
        if(!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
    });
  }


  // test 3d.js
  chart_test() {
    // variable 3d.js     start
    var height = 500
    var width = 700

    var margin = ({top: 20, right: 30, bottom: 30, left: 40})

    var yAxis = g => g
    .attr("transform", `translate(${margin.left},0)`)
    .call(d3.axisLeft(y))
    .call(g => g.select(".domain").remove())
    .call(g => g.select(".tick:last-of-type text").clone()
        .attr("x", 3)
        .attr("text-anchor", "start")
        .attr("font-weight", "bold")
        .text(data.y))

    var xAxis = g => g
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x).ticks(width / 80).tickSizeOuter(0))

    var data = Object.assign((d3.csvParse(this.state.file_csv, d3.autoType)).map(({date, close}) => ({date, value: close})), {y: "$ Close"})
    
    var y = d3.scaleLinear()
      .domain([0, d3.max(data, d => d.value)]).nice()
      .range([height - margin.bottom, margin.top])
      
    var x = d3.scaleUtc()
      .domain(d3.extent(data, d => d.date))
      .range([margin.left, width - margin.right])
    var line = d3.line()
      .defined(d => !isNaN(d.value))
      .x(d => x(d.date))
      .y(d => y(d.value))
    
    var chart = () => {
      const svg = d3.create("svg")
          .attr("viewBox", [0, 0, width, height]);
    
      svg.append("g")
          .call(xAxis);
    
      svg.append("g")
          .call(yAxis);
    
      svg.append("path")
          .datum(data)
          .attr("fill", "none")
          .attr("stroke", "steelblue")
          .attr("stroke-width", 1.5)
          .attr("stroke-linejoin", "round")
          .attr("stroke-linecap", "round")
          .attr("d", line);
    
      return svg.node();
    }
    var s = new XMLSerializer();
    var str = s.serializeToString(chart());
    return str
    // variable 3d.js     end
  }
  onshow_data(selection, data, name, that) {
    // new
    var el = document.getElementById(data[name][0].image);
    el.scrollIntoView(true);
    el.setAttribute("class", "change-color");

    // old
    console.log("that.state.show_bg")
    console.log(that.state.show_bg)
    if (that.state.show_bg !== ""){
      var el = document.getElementById(that.state.show_bg);
      el.setAttribute("class", "change-color-default");
    }
    
    console.log(data[name][0])
    that.setState({
      dynaparam: {
        image: data[name][0].image,
        name: data[name][0].WellName,
        date: data[name][0].date,
        index: 0,
        data: {
          Loads: [],
          Positions: []
        }
      }
    })
    that.setState({show_bg: data[name][0].image})
  }

  onClickDynaImage(item, index) {
    console.log(item)
    console.log(index)
    this.setState({
      dynaparam: {
        image: item.image,
        name: item.WellName,
        date: item.date,
        index: index,
        data: {
          Loads: item.data.Loads,
          Positions: item.data.Positions,
        }
      }
    })
    console.log(this.state.dynaparam)
  }

  // page mounted
  async componentDidMount() {


    const file_csv = (await API().get('/csv')).data
    this.setState({file_csv: file_csv})
    this.drag_and_click_tag1();
    const res = await API().get('/sensors/list')
    var temp = []
    res.data.device.forEach((item, index, arr) => {
      temp.push(item)
    })
    temp.sort()
    this.setState(state => ({
      device : temp
    }))
    this.setState(state => ({
      page_load : !state.page_load
    }))
  }
  /**
   * format name image
   * LKU-R16T 01-26-2020.png
   * LKU-R16T 01-26-20.png
   */
  async onClickSubmit(event) {
    var data = {
      "WellName": this.state.device_select,
      "year": Number(moment(this.state.selectedDate).format("YYYY")),
      "month": Number(moment(this.state.selectedDate).format("M")),
      "day": Number(moment(this.state.selectedDate).format("D")),
      "offset": Number(this.state.offset),
      "type": "FC"
    }
    
    // wait page
    this.setState(state => ({
      page_load : !state.page_load
    }))

    const res = await API().post('/dynadata', data)
    this.setState({dynadata: res.data.data})
    console.log(this.state.dynadata)
    var WellName = this.state.dynadata[0].WellName
    console.log(WellName)
    var dynaimage = []
    this.state.dynadata.forEach((item) => {
      dynaimage.push({
        image: item.PathImage,
        date: moment(item.CardDate + " " + item.CardTime).format("D MMMM YYYY"),
        data: item,
        WellName: WellName
      })
    })
    this.setState({dynaimage: dynaimage})
    console.log(this.state.dynaimage)
    window.localStorage.setItem('dynaimage', JSON.stringify(this.state.dynaimage));

    // wait page
    this.setState(state => ({
      page_load : !state.page_load
    }))
    this.LineChartFunction();
  }

  onChangeDevice = (event) => {
    console.log('this changed')
    this.setState({device_select: event.target.value});
  }

  onChangeInputOffset = (event) => {
    this.setState({offset: event.target.value});
  }

  addDefaultSrc(ev){
    ev.target.src = process.env.PUBLIC_URL + "/dyna_image/"+ "not found image.png"
  }
  LineChartFunction() {

    console.log(Dyna_data)
    var dyna_gen = {
        "8 February 2020": [
          {
              "image": "LKU-R16T/LKU-R16T 02-08-2020  10-14-08.png",
              "date": "8 February 2020",
              "data": {
                  "CardDate": "02-08-2020",
                  "CardDateTime": "2020-02-08T1010:14:08Z",
                  "CardTime": " 10:14:08",
                  "CardType": "PO",
                  "Loads": [
                      "-6656.0",
                      "-6336.0",
                      "-6048.0",
                      "-5696.0",
                      "-5184.0",
                      "-4864.0",
                      "-4608.0",
                      "-3648.0",
                      "-2688.0",
                      "-2048.0",
                      "-1344.0",
                      "-488.0",
                      "184.0",
                      "376.0",
                      "-648.0",
                      "-1984.0",
                      "-2848.0",
                      "-3680.0",
                      "-4512.0",
                      "-5088.0",
                      "-5216.0",
                      "-4576.0",
                      "-3456.0",
                      "-2624.0",
                      "-2016.0",
                      "-1440.0",
                      "-1088.0",
                      "-1192.0",
                      "-1760.0",
                      "-2784.0",
                      "-3648.0",
                      "-4352.0",
                      "-4960.0",
                      "-5280.0",
                      "-5184.0",
                      "-4800.0",
                      "-4096.0",
                      "-3328.0",
                      "-2784.0",
                      "-2336.0",
                      "-2112.0",
                      "-2336.0",
                      "-2784.0",
                      "-3488.0",
                      "-4288.0",
                      "-4832.0",
                      "-5280.0",
                      "-5536.0",
                      "-5120.0",
                      "-4576.0",
                      "-4064.0",
                      "-4096.0",
                      "-4576.0",
                      "-5216.0",
                      "-5856.0",
                      "-5984.0",
                      "-6144.0",
                      "-6400.0",
                      "-6688.0",
                      "-7104.0",
                      "-7456.0",
                      "-7832.0",
                      "-7744.0",
                      "-7200.0",
                      "-6688.0",
                      "-6304.0",
                      "-5984.0",
                      "-5696.0",
                      "-5504.0",
                      "-5696.0",
                      "-6208.0",
                      "-6784.0",
                      "-7104.0",
                      "-7328.0",
                      "-7520.0",
                      "-7520.0",
                      "-7296.0",
                      "-6880.0",
                      "-6464.0",
                      "-6272.0",
                      "-6240.0",
                      "-6272.0",
                      "-6560.0",
                      "-6976.0",
                      "-7608.0",
                      "-8288.0",
                      "-8832.0",
                      "-9112.0",
                      "-9560.0",
                      "-10008.0",
                      "-10456.0",
                      "-10168.0",
                      "-9400.0",
                      "-8664.0",
                      "-8088.0",
                      "-7832.0",
                      "-7296.0",
                      "-7104.0",
                      "-7104.0",
                      "-7456.0",
                      "-7960.0",
                      "-8544.0",
                      "-8608.0",
                      "-8960.0",
                      "-8984.0",
                      "-8984.0",
                      "-8128.0",
                      "-7392.0"
                  ],
                  "PathImage": "LKU-R16T/LKU-R16T 02-08-2020  10-14-08.png",
                  "Positions": [
                      "5.0",
                      "27.0",
                      "44.0",
                      "60.0",
                      "80.0",
                      "102.0",
                      "124.0",
                      "146.0",
                      "179.0",
                      "213.0",
                      "246.0",
                      "282.0",
                      "318.0",
                      "349.0",
                      "391.0",
                      "429.0",
                      "482.0",
                      "526.0",
                      "565.0",
                      "609.0",
                      "654.0",
                      "687.0",
                      "734.0",
                      "776.0",
                      "821.0",
                      "865.0",
                      "923.0",
                      "967.0",
                      "1009.0",
                      "1053.0",
                      "1101.0",
                      "1145.0",
                      "1186.0",
                      "1226.0",
                      "1267.0",
                      "1314.0",
                      "1356.0",
                      "1392.0",
                      "1425.0",
                      "1456.0",
                      "1486.0",
                      "1514.0",
                      "1536.0",
                      "1561.0",
                      "1580.0",
                      "1600.0",
                      "1617.0",
                      "1631.0",
                      "1639.0",
                      "1644.0",
                      "1647.0",
                      "1650.0",
                      "1650.0",
                      "1647.0",
                      "1644.0",
                      "1639.0",
                      "1631.0",
                      "1617.0",
                      "1602.0",
                      "1586.0",
                      "1570.0",
                      "1550.0",
                      "1533.0",
                      "1506.0",
                      "1481.0",
                      "1453.0",
                      "1425.0",
                      "1400.0",
                      "1367.0",
                      "1334.0",
                      "1286.0",
                      "1250.0",
                      "1214.0",
                      "1178.0",
                      "1137.0",
                      "1098.0",
                      "1053.0",
                      "1012.0",
                      "970.0",
                      "923.0",
                      "865.0",
                      "834.0",
                      "779.0",
                      "726.0",
                      "679.0",
                      "646.0",
                      "599.0",
                      "554.0",
                      "510.0",
                      "460.0",
                      "407.0",
                      "360.0",
                      "321.0",
                      "282.0",
                      "244.0",
                      "202.0",
                      "168.0",
                      "135.0",
                      "110.0",
                      "85.0",
                      "63.0",
                      "47.0",
                      "30.0",
                      "11.0",
                      "5.0",
                      "0.0",
                      "0.0",
                      "0.0"
                  ],
                  "WellName": "LKU-R16T"
              },
              "WellName": "LKU-R16T",
              "x": 0.2842063664075841,
              "y": 0.33458972450241058
          },
      ]
    }

    group1.forEach((item, index, arr) => {
      dyna_gen["8 February 2020 item_group2 " + index] = []
      dyna_gen["8 February 2020 item_group2 " + index].push({
        "image": "LKU-R16T/LKU-R16T 02-08-2020  10-14-08.png",
        "date": "8 February 2020",
        "data": {
            "CardDate": "02-08-2020",
            "CardDateTime": "2020-02-08T1010:14:08Z",
            "CardTime": " 10:14:08",
            "CardType": "PO",
            "Loads": [
                "-6656.0",
                "-6336.0",
                "-6048.0",
                "-5696.0",
                "-5184.0",
                "-4864.0",
                "-4608.0",
                "-3648.0",
                "-2688.0",
                "-2048.0",
                "-1344.0",
                "-488.0",
                "184.0",
                "376.0",
                "-648.0",
                "-1984.0",
                "-2848.0",
                "-3680.0",
                "-4512.0",
                "-5088.0",
                "-5216.0",
                "-4576.0",
                "-3456.0",
                "-2624.0",
                "-2016.0",
                "-1440.0",
                "-1088.0",
                "-1192.0",
                "-1760.0",
                "-2784.0",
                "-3648.0",
                "-4352.0",
                "-4960.0",
                "-5280.0",
                "-5184.0",
                "-4800.0",
                "-4096.0",
                "-3328.0",
                "-2784.0",
                "-2336.0",
                "-2112.0",
                "-2336.0",
                "-2784.0",
                "-3488.0",
                "-4288.0",
                "-4832.0",
                "-5280.0",
                "-5536.0",
                "-5120.0",
                "-4576.0",
                "-4064.0",
                "-4096.0",
                "-4576.0",
                "-5216.0",
                "-5856.0",
                "-5984.0",
                "-6144.0",
                "-6400.0",
                "-6688.0",
                "-7104.0",
                "-7456.0",
                "-7832.0",
                "-7744.0",
                "-7200.0",
                "-6688.0",
                "-6304.0",
                "-5984.0",
                "-5696.0",
                "-5504.0",
                "-5696.0",
                "-6208.0",
                "-6784.0",
                "-7104.0",
                "-7328.0",
                "-7520.0",
                "-7520.0",
                "-7296.0",
                "-6880.0",
                "-6464.0",
                "-6272.0",
                "-6240.0",
                "-6272.0",
                "-6560.0",
                "-6976.0",
                "-7608.0",
                "-8288.0",
                "-8832.0",
                "-9112.0",
                "-9560.0",
                "-10008.0",
                "-10456.0",
                "-10168.0",
                "-9400.0",
                "-8664.0",
                "-8088.0",
                "-7832.0",
                "-7296.0",
                "-7104.0",
                "-7104.0",
                "-7456.0",
                "-7960.0",
                "-8544.0",
                "-8608.0",
                "-8960.0",
                "-8984.0",
                "-8984.0",
                "-8128.0",
                "-7392.0"
            ],
            "PathImage": "LKU-R16T/LKU-R16T 02-08-2020  10-14-08.png",
            "Positions": [
                "5.0",
                "27.0",
                "44.0",
                "60.0",
                "80.0",
                "102.0",
                "124.0",
                "146.0",
                "179.0",
                "213.0",
                "246.0",
                "282.0",
                "318.0",
                "349.0",
                "391.0",
                "429.0",
                "482.0",
                "526.0",
                "565.0",
                "609.0",
                "654.0",
                "687.0",
                "734.0",
                "776.0",
                "821.0",
                "865.0",
                "923.0",
                "967.0",
                "1009.0",
                "1053.0",
                "1101.0",
                "1145.0",
                "1186.0",
                "1226.0",
                "1267.0",
                "1314.0",
                "1356.0",
                "1392.0",
                "1425.0",
                "1456.0",
                "1486.0",
                "1514.0",
                "1536.0",
                "1561.0",
                "1580.0",
                "1600.0",
                "1617.0",
                "1631.0",
                "1639.0",
                "1644.0",
                "1647.0",
                "1650.0",
                "1650.0",
                "1647.0",
                "1644.0",
                "1639.0",
                "1631.0",
                "1617.0",
                "1602.0",
                "1586.0",
                "1570.0",
                "1550.0",
                "1533.0",
                "1506.0",
                "1481.0",
                "1453.0",
                "1425.0",
                "1400.0",
                "1367.0",
                "1334.0",
                "1286.0",
                "1250.0",
                "1214.0",
                "1178.0",
                "1137.0",
                "1098.0",
                "1053.0",
                "1012.0",
                "970.0",
                "923.0",
                "865.0",
                "834.0",
                "779.0",
                "726.0",
                "679.0",
                "646.0",
                "599.0",
                "554.0",
                "510.0",
                "460.0",
                "407.0",
                "360.0",
                "321.0",
                "282.0",
                "244.0",
                "202.0",
                "168.0",
                "135.0",
                "110.0",
                "85.0",
                "63.0",
                "47.0",
                "30.0",
                "11.0",
                "5.0",
                "0.0",
                "0.0",
                "0.0"
            ],
            "WellName": "LKU-R16T"
        },
        "WellName": "LKU-R16T",
        "x": item.x,
        "y": item.y
      })
    })
    group2.forEach((item, index, arr) => {
      dyna_gen["8 February 2020 item_group1 " + index] = []
      dyna_gen["8 February 2020 item_group1 " + index].push({
        "image": "LKU-R16T/LKU-R16T 02-08-2020  10-14-08.png",
        "date": "8 February 2020",
        "data": {
            "CardDate": "02-08-2020",
            "CardDateTime": "2020-02-08T1010:14:08Z",
            "CardTime": " 10:14:08",
            "CardType": "PO",
            "Loads": [
                "-6656.0",
                "-6336.0",
                "-6048.0",
                "-5696.0",
                "-5184.0",
                "-4864.0",
                "-4608.0",
                "-3648.0",
                "-2688.0",
                "-2048.0",
                "-1344.0",
                "-488.0",
                "184.0",
                "376.0",
                "-648.0",
                "-1984.0",
                "-2848.0",
                "-3680.0",
                "-4512.0",
                "-5088.0",
                "-5216.0",
                "-4576.0",
                "-3456.0",
                "-2624.0",
                "-2016.0",
                "-1440.0",
                "-1088.0",
                "-1192.0",
                "-1760.0",
                "-2784.0",
                "-3648.0",
                "-4352.0",
                "-4960.0",
                "-5280.0",
                "-5184.0",
                "-4800.0",
                "-4096.0",
                "-3328.0",
                "-2784.0",
                "-2336.0",
                "-2112.0",
                "-2336.0",
                "-2784.0",
                "-3488.0",
                "-4288.0",
                "-4832.0",
                "-5280.0",
                "-5536.0",
                "-5120.0",
                "-4576.0",
                "-4064.0",
                "-4096.0",
                "-4576.0",
                "-5216.0",
                "-5856.0",
                "-5984.0",
                "-6144.0",
                "-6400.0",
                "-6688.0",
                "-7104.0",
                "-7456.0",
                "-7832.0",
                "-7744.0",
                "-7200.0",
                "-6688.0",
                "-6304.0",
                "-5984.0",
                "-5696.0",
                "-5504.0",
                "-5696.0",
                "-6208.0",
                "-6784.0",
                "-7104.0",
                "-7328.0",
                "-7520.0",
                "-7520.0",
                "-7296.0",
                "-6880.0",
                "-6464.0",
                "-6272.0",
                "-6240.0",
                "-6272.0",
                "-6560.0",
                "-6976.0",
                "-7608.0",
                "-8288.0",
                "-8832.0",
                "-9112.0",
                "-9560.0",
                "-10008.0",
                "-10456.0",
                "-10168.0",
                "-9400.0",
                "-8664.0",
                "-8088.0",
                "-7832.0",
                "-7296.0",
                "-7104.0",
                "-7104.0",
                "-7456.0",
                "-7960.0",
                "-8544.0",
                "-8608.0",
                "-8960.0",
                "-8984.0",
                "-8984.0",
                "-8128.0",
                "-7392.0"
            ],
            "PathImage": "LKU-R16T/LKU-R16T 02-08-2020  10-14-08.png",
            "Positions": [
                "5.0",
                "27.0",
                "44.0",
                "60.0",
                "80.0",
                "102.0",
                "124.0",
                "146.0",
                "179.0",
                "213.0",
                "246.0",
                "282.0",
                "318.0",
                "349.0",
                "391.0",
                "429.0",
                "482.0",
                "526.0",
                "565.0",
                "609.0",
                "654.0",
                "687.0",
                "734.0",
                "776.0",
                "821.0",
                "865.0",
                "923.0",
                "967.0",
                "1009.0",
                "1053.0",
                "1101.0",
                "1145.0",
                "1186.0",
                "1226.0",
                "1267.0",
                "1314.0",
                "1356.0",
                "1392.0",
                "1425.0",
                "1456.0",
                "1486.0",
                "1514.0",
                "1536.0",
                "1561.0",
                "1580.0",
                "1600.0",
                "1617.0",
                "1631.0",
                "1639.0",
                "1644.0",
                "1647.0",
                "1650.0",
                "1650.0",
                "1647.0",
                "1644.0",
                "1639.0",
                "1631.0",
                "1617.0",
                "1602.0",
                "1586.0",
                "1570.0",
                "1550.0",
                "1533.0",
                "1506.0",
                "1481.0",
                "1453.0",
                "1425.0",
                "1400.0",
                "1367.0",
                "1334.0",
                "1286.0",
                "1250.0",
                "1214.0",
                "1178.0",
                "1137.0",
                "1098.0",
                "1053.0",
                "1012.0",
                "970.0",
                "923.0",
                "865.0",
                "834.0",
                "779.0",
                "726.0",
                "679.0",
                "646.0",
                "599.0",
                "554.0",
                "510.0",
                "460.0",
                "407.0",
                "360.0",
                "321.0",
                "282.0",
                "244.0",
                "202.0",
                "168.0",
                "135.0",
                "110.0",
                "85.0",
                "63.0",
                "47.0",
                "30.0",
                "11.0",
                "5.0",
                "0.0",
                "0.0",
                "0.0"
            ],
            "WellName": "LKU-R16T"
        },
        "WellName": "LKU-R16T",
        "x": item.x,
        "y": item.y
      })
    })
    group3.forEach((item, index, arr) => {
      dyna_gen["8 February 2020 item_group3 " + index] = []
      dyna_gen["8 February 2020 item_group3 " + index].push({
        "image": "LKU-R16T/LKU-R16T 02-08-2020  10-14-08.png",
        "date": "8 February 2020",
        "data": {
            "CardDate": "02-08-2020",
            "CardDateTime": "2020-02-08T1010:14:08Z",
            "CardTime": " 10:14:08",
            "CardType": "PO",
            "Loads": [
                "-6656.0",
                "-6336.0",
                "-6048.0",
                "-5696.0",
                "-5184.0",
                "-4864.0",
                "-4608.0",
                "-3648.0",
                "-2688.0",
                "-2048.0",
                "-1344.0",
                "-488.0",
                "184.0",
                "376.0",
                "-648.0",
                "-1984.0",
                "-2848.0",
                "-3680.0",
                "-4512.0",
                "-5088.0",
                "-5216.0",
                "-4576.0",
                "-3456.0",
                "-2624.0",
                "-2016.0",
                "-1440.0",
                "-1088.0",
                "-1192.0",
                "-1760.0",
                "-2784.0",
                "-3648.0",
                "-4352.0",
                "-4960.0",
                "-5280.0",
                "-5184.0",
                "-4800.0",
                "-4096.0",
                "-3328.0",
                "-2784.0",
                "-2336.0",
                "-2112.0",
                "-2336.0",
                "-2784.0",
                "-3488.0",
                "-4288.0",
                "-4832.0",
                "-5280.0",
                "-5536.0",
                "-5120.0",
                "-4576.0",
                "-4064.0",
                "-4096.0",
                "-4576.0",
                "-5216.0",
                "-5856.0",
                "-5984.0",
                "-6144.0",
                "-6400.0",
                "-6688.0",
                "-7104.0",
                "-7456.0",
                "-7832.0",
                "-7744.0",
                "-7200.0",
                "-6688.0",
                "-6304.0",
                "-5984.0",
                "-5696.0",
                "-5504.0",
                "-5696.0",
                "-6208.0",
                "-6784.0",
                "-7104.0",
                "-7328.0",
                "-7520.0",
                "-7520.0",
                "-7296.0",
                "-6880.0",
                "-6464.0",
                "-6272.0",
                "-6240.0",
                "-6272.0",
                "-6560.0",
                "-6976.0",
                "-7608.0",
                "-8288.0",
                "-8832.0",
                "-9112.0",
                "-9560.0",
                "-10008.0",
                "-10456.0",
                "-10168.0",
                "-9400.0",
                "-8664.0",
                "-8088.0",
                "-7832.0",
                "-7296.0",
                "-7104.0",
                "-7104.0",
                "-7456.0",
                "-7960.0",
                "-8544.0",
                "-8608.0",
                "-8960.0",
                "-8984.0",
                "-8984.0",
                "-8128.0",
                "-7392.0"
            ],
            "PathImage": "LKU-R16T/LKU-R16T 02-08-2020  10-14-08.png",
            "Positions": [
                "5.0",
                "27.0",
                "44.0",
                "60.0",
                "80.0",
                "102.0",
                "124.0",
                "146.0",
                "179.0",
                "213.0",
                "246.0",
                "282.0",
                "318.0",
                "349.0",
                "391.0",
                "429.0",
                "482.0",
                "526.0",
                "565.0",
                "609.0",
                "654.0",
                "687.0",
                "734.0",
                "776.0",
                "821.0",
                "865.0",
                "923.0",
                "967.0",
                "1009.0",
                "1053.0",
                "1101.0",
                "1145.0",
                "1186.0",
                "1226.0",
                "1267.0",
                "1314.0",
                "1356.0",
                "1392.0",
                "1425.0",
                "1456.0",
                "1486.0",
                "1514.0",
                "1536.0",
                "1561.0",
                "1580.0",
                "1600.0",
                "1617.0",
                "1631.0",
                "1639.0",
                "1644.0",
                "1647.0",
                "1650.0",
                "1650.0",
                "1647.0",
                "1644.0",
                "1639.0",
                "1631.0",
                "1617.0",
                "1602.0",
                "1586.0",
                "1570.0",
                "1550.0",
                "1533.0",
                "1506.0",
                "1481.0",
                "1453.0",
                "1425.0",
                "1400.0",
                "1367.0",
                "1334.0",
                "1286.0",
                "1250.0",
                "1214.0",
                "1178.0",
                "1137.0",
                "1098.0",
                "1053.0",
                "1012.0",
                "970.0",
                "923.0",
                "865.0",
                "834.0",
                "779.0",
                "726.0",
                "679.0",
                "646.0",
                "599.0",
                "554.0",
                "510.0",
                "460.0",
                "407.0",
                "360.0",
                "321.0",
                "282.0",
                "244.0",
                "202.0",
                "168.0",
                "135.0",
                "110.0",
                "85.0",
                "63.0",
                "47.0",
                "30.0",
                "11.0",
                "5.0",
                "0.0",
                "0.0",
                "0.0"
            ],
            "WellName": "LKU-R16T"
        },
        "WellName": "LKU-R16T",
        "x": item.x,
        "y": item.y
      })
    })
    console.log(JSON.stringify(dyna_gen))
    // Dyna_data = dyna_gen

    var margin = {top: 10, right: 30, bottom: 30, left: 60},
        width = 460 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    var SVG = d3.select("#showchart")
        .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

    var div = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var x = d3.scaleLinear()
        .domain([0,1])
        .range([ 0, width ]);
    var xAxis = SVG.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    var y = d3.scaleLinear()
        .domain([0, 1])
        .range([ height, 0]);
    
    var yAxis = SVG.append("g")
        .call(d3.axisLeft(y));

    var clip = SVG.append("defs").append("SVG:clipPath")
        .attr("id", "clip")
        .append("SVG:rect")
        .attr("width", width )
        .attr("height", height )
        .attr("x", 0)
        .attr("y", 0);

    var scatter = []
    const color = "#0971bb"
    var i = 0;
    console.log(JSON.parse(window.localStorage.getItem('dynaimage'))) 
    var data = _.groupBy(JSON.parse(window.localStorage.getItem('dynaimage')), 'date');

    for (let name in data) {
        data[name][0].x = Math.random()
        data[name][0].y = Math.random()
    }

    console.log(JSON.stringify(data))
    var that = this
    for (let name in Dyna_data){

        scatter[i] = SVG.append('g')
            .attr("clip-path", "url(#clip)")
        scatter[i].selectAll("circle")
            .data(Dyna_data[name])
            .enter()
            .append("circle")
                .attr("cx", function (d) { return x(d.x); } )
                .attr("cy", function (d) { return y(d.y); } )
                .attr("r", 4)
                .style("fill", color)
                .style("opacity", 1)
                .on('mouseover', function (d, i) {
                    d3.select(this).transition()
                        .duration('100')
                        .attr("r", 6);
                    div.transition()
                        .duration(50)
                        .style("opacity", 1);
                    var str = d.WellName + "<br> [" + name + "]"
                    div.html(str)
                        .style("left", (d3.event.pageX + 10) + "px")
                        .style("top", (d3.event.pageY - 15) + "px");
                })
                .on('mouseout', function (d, i) {
                    d3.select(this).transition()
                        .duration('100')
                        .attr("r", 4);
                    div.transition()
                        .duration(50)
                        .attr("r", 4)
                        .style("opacity", 0);
                })
                .on('click', (d,i) => {
                  d3.selectAll(this).call(this.onshow_data, Dyna_data, name, that);
                })
                // this.onshow_data())
        i++
    }
                
    const updateChart = () => {
        var newX = d3.event.transform.rescaleX(x);
        var newY = d3.event.transform.rescaleY(y);
    
        xAxis.call(d3.axisBottom(newX))
        yAxis.call(d3.axisLeft(newY))
    
        for (let i = 0; i < scatter.length;i++){
            scatter[i]
                .selectAll("circle")
                .attr('cx', function(d) {return newX(d.x)})
                .attr('cy', function(d) {return newY(d.y)});
            scatter[i]
                .selectAll("circle")
                .attr('cx', function(d) {return newX(d.x)})
                .attr('cy', function(d) {return newY(d.y)});
        }
    }

    var zoom = d3.zoom()
        .scaleExtent([-100, 100])  
        .extent([[0, 0], [width, height]])
        .on("zoom", updateChart);

    // SVG.append("rect")
    //     .attr("width", width)
    //     .attr("height", height)
    //     .style("fill", "none")
    //     .style("pointer-events", "all")
    //     .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
    //     .call(zoom);

  }
  click_show_image(id){
    var el = document.getElementById(id);
    el.scrollIntoView(true);
  }
  show_card = () => {
    return (
      <>
        <div ref={this.myRef}>
          <Container fluid>
            {
                <Row>
                  <Col md={12} hidden={!this.state.page_load} >
                    <div className="mx-auto"  style={{ width: '200px', marginTop: '10%' }}>
                      <center>
                        <Spinner animation="border" role="status" >
                          <span className="sr-only">Loading...</span>
                        </Spinner> 
                        <span> Loading...</span>
                      </center> 
                    </div>
                  </Col>
                  <Col md={12} hidden={this.state.page_load}>
                    <Card style={{ width: '100%' }}>
                      {/* <Card.Header>test </Card.Header> */}

                      <Row>
                        <Col md={2}></Col>
                        <Col md={2} style={{ margin: '10px' }}>
                          <label>เลือกอุปกรณ์</label>
                          <Form.Control as="select" onChange={this.onChangeDevice}  style = {{marginTop: "10px"}}>
                            <option value=''>กรุณาเลือก Device</option> 
                            {
                              this.state.device.map((item, index) => (
                                <option key={(index)} value={item} >{item}</option>
                              ))
                            }
                          </Form.Control>
                        </Col>
                        <Col md={2} style = {{marginLeft:"10px"}}>
                          <label style = {{ marginTop: '10px'}}>เลือกวันที่</label><br></br>
                          <MuiPickersUtilsProvider utils={DateFnsUtils} >
                            <KeyboardDatePicker
                              disableToolbar
                              variant="inline"
                              format="dd-MM-yyyy"
                              margin="normal"
                              id="date-picker-inline"
                              label=""
                              value={this.state.selectedDate}
                              onChange={this.state.handleDateChange}
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}
                            />
                          </MuiPickersUtilsProvider>
                        </Col>
                        <Col md={2} style={{ margin: '10px' }}>
                            <label style = {{}}>กำหนดช่วงเวลา (+,-)</label>
                            <Form.Control type="number" value={this.state.offset} onChange={this.onChangeInputOffset} placeholder="Offset" style = {{marginTop: "10px"}} />
                        </Col>
                        <Col md={2} style={{margin: "10px"}}>
                          <Button disabled={(this.state.device_select === "" ? true : false)} variant="outline-primary" style = {{marginTop: "40px"}} onClick={this.onClickSubmit}>Submit</Button>
                        </Col>
                        <Col md={2}></Col>
                      </Row>
                    </Card>
                    <Row>
                      <Col md={12}>
                        <Card style = {{marginTop: "10px", height: "100%"}}>
                          <div className="d-flex flex-row mb-3 float-left drag-function" style={{overflowX: "scroll",width: "100%", height: "250px !important", flexDirection: "row", display: "flex", margin: "0 auto"}}>
                            <div className="not-found-device" hidden={this.state.dynaimage.length !== 0}>ไม่พบอุปกรณ์</div>
                            {
                              this.state.dynaimage.map((item, index) => {
                                return (<div id={item.image} key={index} className="p-2 dyna-hover" onClick={
                                  () => {
                                    this.onClickDynaImage(item, index)
                                  }
                                } style={{ marginLeft: "10px"}}><img width='380px' height='200px' src={
                                  process.env.PUBLIC_URL + "/dyna_image/"+ item.image
                                }  alt="dyna graph"></img><div style={{ width: "380px", height:'50px'}}><center>{ item.date }</center></div></div>)
                              }) 
                            }
                          </div>
                        </Card>
                      </Col>
                    </Row>
                    <Row style={{marginTop: "10px"}} hidden={this.state.dynaparam.name === ""}>
                      <Col md={5}>
                        {/* detail */}
                        <Card style = {{marginTop: "10px", height: "100%"}}>
                          <Container>
                          <Row style={{marginTop: "20px"}}>
                              <Col md={3}>
                                Wellname:
                              </Col>
                              <Col md={9}>
                                {
                                  this.state.dynaparam.name
                                }
                              </Col>
                            </Row>
                            <Row style={{marginTop: "20px"}}>
                              <Col md={3}>
                                Date:
                              </Col>
                              <Col md={9}>
                                {
                                  this.state.dynaparam.date
                                }
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <img width='100%' height='100%' 
                                  onError={
                                    this.addDefaultSrc
                                  }
                                  src={
                                    process.env.PUBLIC_URL + "/dyna_image/"+ this.state.dynaparam.image
                                  }  
                                  alt="dyna graph"></img>
                              </Col>
                            </Row>
                          </Container>
                        </Card>
                      </Col>
                      <Col md={7}>
                        {/* dyna graph */}
                        <Card style = {{marginTop: "10px", height: "100%"}}>
                          <Container>
                            <Row>
                                <Col md={12}>
                                    PTT
                                </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <div id='showchart'></div>
                                {/* <BeamPumpLineChart dynaimage={this.state.dynaimage}/> */}
                                {/* <Linechart csv_file={this.state.file_csv} dynachartdata={this.state.dynaparam}/> */}
                                {/* <div dangerouslySetInnerHTML={{ __html: this.chart_test() }} /> */}
                                {
                                  // this.chart_test()
                                  // this.chart_test().map((item, index) => {
                                  //   console.log(item)
                                  //   return (<div key={index}>{item}</div>)
                                  // })
                                  // this.state.dynaparam.data.map((item, index) => {
                                  //   console.log(item)
                                  //   return (
                                  //     <div key={index}>{item.Loads + " " + item.Positions}</div>
                                  //   )
                                  // })
                                }
                              </Col>
                            </Row>
                          </Container>
                        </Card>
                      </Col>
                    </Row>

                  </Col>
                </Row>
            }
          </Container>
        </div>
      </>
    )
  }

  render() {
    return (
      <div>
        {this.show_card()}
      </div>
    );
  }
  
}

export default setting;