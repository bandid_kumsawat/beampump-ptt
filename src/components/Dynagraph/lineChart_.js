import React from 'react';
import * as d3 from 'd3';
import API from './../api/server'
import json_data from './data.json'

class App extends React.Component {
 constructor(){
    super();
    this.myRef = React.createRef(); 
    this.dataset = [100, 200, 300, 400, 500]
 }

 async componentDidMount(){
    // var data = [{
    //     date: 2009,
    //     wage: 7.25
    // }, {
    //     date: 2008,
    //     wage: 6.55
    // }, {
    //     date: 2007,
    //     wage: 5.85
    // }, {
    //     date: 1997,
    //     wage: 5.15
    // }, {
    //     date: 1996,
    //     wage: 4.75
    // }, {
    //     date: 1991,
    //     wage: 4.25
    // }, {
    //     date: 1981,
    //     wage: 3.35
    // }, {
    //     date: 1980,
    //     wage: 3.10
    // }, {
    //     date: 1979,
    //     wage: 2.90
    // }, {
    //     date: 1978,
    //     wage: 2.65
    // }]
    var temp = json_data
    var data = []
    for (let i = 0;i < temp.Loads.length;i++){
        data.push({
            date: temp.Positions[i],
            wage: temp.Loads[i]
        })
    }
    console.log('show data line chart')
    var margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 40
    }
    console.log(this.props.dynachartdata)
    var div = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var default_width = 700 - margin.left - margin.right;
    var default_height = 500 - margin.top - margin.bottom;
    var default_ratio = default_width / default_height;

    var width = 800
    var height = 500

    const set_size = () => {
        var current_width = window.innerWidth;
        var current_height = window.innerHeight;
        var current_ratio = current_width / current_height;
        var h, w
        // desktop
        if (current_ratio > default_ratio) {
            h = default_height;
            w = default_width;
            // mobile
        } else {
            margin.left = 40
            w = current_width - 40;
            h = w / default_ratio;
        }
        // Set new width and height based on graph dimensions
        var width = w - 50 - margin.right;
        var height = h - margin.top - margin.bottom;
    };

    set_size();

    // data.forEach(function (d) {
    //     var parseDate = d3.timeParse("%Y");
    //     d.date = parseDate(d.date);
    //     d.wage = +d.wage;
    // });
    // data.sort(function (a, b) {
    //     return a.date - b.date;
    // });

    var x = d3.scaleTime().range([0, width]);
    var y = d3.scaleLinear().range([height, 0]);

    x.domain(d3.extent(data, function (d) {
        return d.date;
    }));
    y.domain([0, d3.max(data, function (d) {
        return d.wage;
    })]);

    var valueline = d3.line()
        .x(function (d) {
            return x(d.date);
        })
        .y(function (d) {
            return y(d.wage);
        });
    // append the svg object to the body of the page
    var svg = d3.select(this.myRef.current).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Add the trendline
    svg.append("path")
        .data([data])
        .attr("class", "line")
        .attr("d", valueline)
        .attr("stroke", "#32CD32")
        .attr("stroke-width", 2)
        .attr("fill", "#FFFFFF");

            var path = svg.selectAll("dot")
    .data(data)
    .enter().append("circle")
    .attr("r", 5)
    .attr("cx", function (d) {
        return x(d.date);
    })
    .attr("cy", function (d) {
        return y(d.wage);
    })
    .attr("stroke", "#32CD32")
    .attr("stroke-width", 1.5)
    .attr("fill", "#FFFFFF")
    .on('mouseover', function (d, i) {
        d3.select(this).transition()
            .duration('100')
            .attr("r", 7);
        div.transition()
            .duration(100)
            .style("opacity", 1);
        div.html("$" + d3.format(".2f")(d.wage))
            .style("left", (d3.event.pageX + 10) + "px")
            .style("top", (d3.event.pageY - 15) + "px");
    })
    .on('mouseout', function (d, i) {
        d3.select(this).transition()
            .duration('200')
            .attr("r", 5);
        div.transition()
            .duration('200')
            .style("opacity", 0);
    });

    var path = svg.selectAll("dot")
        .data(data)
        .enter().append("circle")
        .attr("r", 5)
        .attr("cx", function (d) {
            return x(d.date);
        })
        .attr("cy", function (d) {
            return y(d.wage);
        })
        .attr("stroke", "#32CD32")
        .attr("stroke-width", 1.5)
        .attr("fill", "#FFFFFF")
        .on('mouseover', function (d, i) {
            d3.select(this).transition()
                .duration('100')
                .attr("r", 7);
            div.transition()
                .duration(100)
                .style("opacity", 1);
            div.html("$" + d3.format(".2f")(d.wage))
                .style("left", (d3.event.pageX + 10) + "px")
                .style("top", (d3.event.pageY - 15) + "px");
        })
        .on('mouseout', function (d, i) {
            d3.select(this).transition()
                .duration('200')
                .attr("r", 5);
            div.transition()
                .duration('200')
                .style("opacity", 0);
        });

        if (width < 500) {
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x).ticks(5));
        } else {
            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));
        }
        
        svg.append("g")
            .call(d3.axisLeft(y).tickFormat(function (d) {
                return "$" + d3.format(".2f")(d)
            }));



    // ##########################################################################################################################
    // var res = await API().get('/csv2')
    // var data = Object.assign(d3.csvParse(res.data, d3.autoType).map(({date, close}) => ({date, value: close})), {y: "$ Close"})
    // console.log(data)
    // var margin = ({top: 20, right: 30, bottom: 30, left: 40})
    // var height = 500
    // var width = 500
    // const bisect = () => {
    //     const bisect = d3.bisector(d => d.date).left;
    //     return mx => {
    //         const date = x.invert(mx);
    //         const index = bisect(data, date, 1);
    //         const a = data[index - 1];
    //         const b = data[index];
    //         return b && (date - a.date > b.date - date) ? b : a;
    //     };
    // }
    // const formatDate = (date) => {
    //     return date.toLocaleString("en", {
    //         month: "short",
    //         day: "numeric",
    //         year: "numeric",
    //         timeZone: "UTC"
    //     });
    // }
    // const formatValue = (value) => {
    //     return value.toLocaleString("en", {
    //       style: "currency",
    //       currency: "USD"
    //     });
    // }
    // var line = d3.line()
    //     .curve(d3.curveStep)
    //     .defined(d => !isNaN(d.value))
    //     .x(d => x(d.date))
    //     .y(d => y(d.value))

    // var yAxis = g => g
    //     .attr("transform", `translate(${margin.left},0)`)
    //     .call(d3.axisLeft(y))
    //     .call(g => g.select(".domain").remove())
    //     .call(g => g.select(".tick:last-of-type text").clone()
    //         .attr("x", 3)
    //         .attr("text-anchor", "start")
    //         .attr("font-weight", "bold")
    //         .text(data.y))
    // var xAxis = g => g
    //     .attr("transform", `translate(0,${height - margin.bottom})`)
    //     .call(d3.axisBottom(x).ticks(width / 80).tickSizeOuter(0))
    // var y = d3.scaleLinear()
    //     .domain([0, d3.max(data, d => d.value)]).nice()
    //     .range([height - margin.bottom, margin.top])
    // var x = d3.scaleUtc()
    //     .domain(d3.extent(data, d => d.date))
    //     .range([margin.left, width - margin.right])
    // var callout = (g, value) => {
    //     if (!value) return g.style("display", "none");
      
    //     g.style("display", null)
    //         .style("pointer-events", "none")
    //         .style("font", "10px sans-serif");
      
    //     const path = g.selectAll("path")
    //       .data([null])
    //       .join("path")
    //         .attr("fill", "white")
    //         .attr("stroke", "black");
      
    //     const text = g.selectAll("text")
    //       .data([null])
    //       .join("text")
    //       .call(text => text
    //         .selectAll("tspan")
    //         .data((value + "").split(/\n/))
    //         .join("tspan")
    //           .attr("x", 0)
    //           .attr("y", (d, i) => `${i * 1.1}em`)
    //           .style("font-weight", (_, i) => i ? null : "bold")
    //           .text(d => d));
      
    //     const {x, y, width: w, height: h} = text.node().getBBox();
      
    //     text.attr("transform", `translate(${-w / 2},${15 - y})`);
    //     path.attr("d", `M${-w / 2 - 10},5H-5l5,-5l5,5H${w / 2 + 10}v${h + 20}h-${w + 20}z`);
    // }
    // var chart = () => {
        // const svg = d3.select(this.myRef.current).append("svg")
        //     .attr("width", width + margin.left + margin.right)
        //     .attr("height", height + margin.top + margin.bottom)
        //     .style("-webkit-tap-highlight-color", "transparent")
        //     .style("overflow", "visible");
      
    //     svg.append("g")
    //         .call(xAxis);
      
    //     svg.append("g")
    //         .call(yAxis);
        
    //     svg.append("path")
    //         .datum(data)
    //         .attr("fill", "none")
    //         .attr("stroke", "steelblue")
    //         .attr("stroke-width", 1.5)
    //         .attr("stroke-linejoin", "round")
    //         .attr("stroke-linecap", "round")
    //         .attr("d", line);
      
    //     const tooltip = svg.append("g");
      
    //     svg.on("touchmove mousemove", function() {
    //         const {date, value} = bisect(d3.mouse(this)[0]);
    //         tooltip.attr("transform", `translate(${x(date)},${y(value)})`)
    //         .call(callout, `${formatValue(value)}${formatDate(date)}`);
    //     });
      
    //     svg.on("touchend mouseleave", () => tooltip.call(callout, null));
        
    //     return svg.node();
    // }
    // chart()
 }
 render(){
  return (
    <div>
      <div ref={this.myRef}></div>
    </div>
  );
 }
 
}
export default App;